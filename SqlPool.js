/**
 * Created by 黄二杰 on 2016/3/11.
 */

var mysql = require('mysql');

var winston = require("winston");
var mpinfo = winston.info;
var mpdebug = winston.debug;
var mperror = winston.error;
var mpwarn = winston.warn;


//开发服
var config = {
    host: 'MysqlIP',
    // user: 'root',
    user: 'admin',
    password: '8836741E-00F0-4A83-92EE-8234D2425833',
    database: 'gamedb',
    port: 22068
};


var pool = mysql.createPool({
    connectionLimit: 30,
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database,
    port: config.port
});

//pool.on('connection', function (connection) {
//    //log("connection")
//});
//pool.on('enqueue', function () {
//    //log('Waiting for available connection slot');
//});


var x_query = function (sql, val, callback) {

    if (arguments.length == 2) {
        callback = val;
        pool.getConnection(function (err, connection) {
            // Use the connection
            if (err) {
                mperror(err);
                connection && connection.release();
                return;
            }
            connection.query(sql, function (err, rows, fildes) {

                if (err) {
                    mperror("--------------------------------------------------");
                    mperror("sql:\t" + sql);
                    mperror("val:\t" + val);
                    mperror(err);
                    mperror("--------------------------------------------------");
                }

                try {
                    //执行回调
                    callback(err, rows, fildes);
                }
                catch (e) {
                    mperror(e);
                }
                finally {
                    //关闭连接
                    connection.release();
                }


            });
        });
    }
    if (arguments.length == 3) {
        pool.getConnection(function (err, connection) {
            // Use the connection
            if (err) {
                mperror(err);
                connection && connection.release();
                return;
            }
            connection.query(sql, val, function (err, rows, fildes) {

                if (err) {
                    mperror("--------------------------------------------------");
                    mperror("sql:\t" + sql);
                    mperror("val:\t" + val);
                    mperror(err);
                    mperror("--------------------------------------------------");
                }

                try {
                    //执行回调
                    callback(err, rows, fildes);
                }
                catch (e) {
                    mperror(e);
                }
                finally {
                    //关闭连接
                    connection.release();
                }


            });
        });
    }
};

pool.x_query = x_query;


//对外开放接口
module.exports = pool;
