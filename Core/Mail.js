/**
 * Created by 黄二杰 on 2016/3/23.
 */

/**
 * 附件
 * @constructor
 */
var Attachment = function (data) {

    this.attachmentID = data.attachmentID;
    this.type = data.type;
    this.num = data.num;
    this.status = data.aStatus;
};


/**
 * 邮件
 * @constructor
 */
var Mail = function (data) {

    this.mailID = data.mailID;
    this.userID = data.userID;
    this.title = data.title;
    this.content = data.content;
    this.status = data.status;
    this.createTime = data.createTime;

    this.attachment = [];

    if (data.attachmentID != null) {
        this.attachment.push(new Attachment(data));
    }
};


var p = Mail.prototype;

p.pushAttachment = function (data) {
    if (data.attachmentID != null) {
        this.attachment.push(new Attachment(data));
    }
};


module.exports.Mail = Mail;




