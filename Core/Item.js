/**
 * Created by 黄二杰 on 2016/3/22.
 */


//道具类
var Item = function () {
    this.id = null;
    this.name = null;
    this.desc = null;               //道具说明
    this.payAcer = null;            //需要钻石
    this.giveScore = null;          //赠送金豆
    this.sortID = null;             //排序ID
};

module.exports.Item = Item;
