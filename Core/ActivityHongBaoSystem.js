/**
 * Created by Apple on 2017/7/14.
 */

var GD = require("../GD.js");

/**
 * 随机红包
 * @constructor
 */
function ActivityHongBao(id, money, num, type) {
    this.id = id;
    this.money = money;
    this.num = num;
    this.type = type;
    this.takeUserInfoArray = [];
    this.ts = new Date().toLocaleString();      //红包的时间

    this.tempMoneyArray = this.getSpread(money, num, type);

}

ActivityHongBao.TypeRandom = 1;           //随机红包
ActivityHongBao.TypeAverage = 2;           //平均红包

var rp = ActivityHongBao.prototype;

/**
 * 获取红包的分配
 * @param money
 * @param num
 * @param type
 * @returns {Array}
 */
rp.getSpread = function (money, num, type) {

    var tempMoneyArray = [];

    if (type == ActivityHongBao.TypeRandom) {
        var leftoverMoney = money - num;  //剩下的
        var weightArray = [];
        var sum = 0;
        for (var i = 0; i < this.num; ++i) {
            weightArray[i] = Math.random();
            sum += weightArray[i];
        }
        var tempSum = 0;
        for (var i = 0; i < this.num; ++i) {
            if (i == this.num - 1) {
                tempMoneyArray[i] = this.money - tempSum;
            }
            else {
                tempMoneyArray[i] = Math.floor(weightArray[i] / sum * leftoverMoney) + 1;
                tempSum += tempMoneyArray[i];
            }
        }
    }
    else {


        for (var i = 0; i < num - 1; ++i) {
            tempMoneyArray[i] = Math.floor(money / num);
        }
        tempMoneyArray[num - 1] = money - (tempMoneyArray[0] || 0) * (num - 1);
    }

    return tempMoneyArray;
};


/**
 * 拿红包， 返回是否拿到红包的金额
 * @param userID
 * @returns 如果领取过返回-money， 如果无红包可领取返回0， 其它返回领取到的金额
 */
rp.takeHongBao = function (userID) {

    //判断是否拿过红包
    for (var i = 0; i < this.takeUserInfoArray.length; ++i) {
        if (this.takeUserInfoArray[i].userID == userID) {
            return -this.takeUserInfoArray[i].money;
        }
    }

    if (this.takeUserInfoArray.length < this.num) {

        var money = this.tempMoneyArray[this.takeUserInfoArray.length];
        this.takeUserInfoArray.push({
            userID: userID,
            money: money,
            nickname: GD.cacheUserInfoMap[userID].nickname,
            ts: new Date().toLocaleTimeString(),            //直接用字符串了， 省得客户端还要去格式化
        });
        return money;
    }
    else {
        return 0;
    }

};

/**
 * 获取红包的领取信息
 * @returns {Array}
 */
rp.getHongBaoTakeInfo = function () {
    return this.takeUserInfoArray;
};


/**
 * 随机红包系统
 * @constructor
 */
var ActivityHongBaoSystem = function () {
    this.hongBaoQueue = {};
};

var p = ActivityHongBaoSystem.prototype;

/**
 * 创建一个红包
 * @param id
 * @param money
 * @param num
 * @param type
 */
p.createBaoHong = function (id, money, num, type) {
    this.hongBaoQueue[id] = new ActivityHongBao(id, money, num, type);
};


/**
 * 拿红包， 返回是否拿到红包的金额
 * @param hongBaoID
 * @param userID
 * @returns 如果领取过返回-money， 如果无红包可领取返回0， 其它返回领取到的金额
 */
p.takeHongBao = function (hongBaoID, userID) {

    if (this.hongBaoQueue[hongBaoID]) {
        return this.hongBaoQueue[hongBaoID].takeHongBao(userID);
    }
    return 0;
};

/**
 * 获取指定红包的领取信息
 * @param hongBaoID
 * @returns {*}
 */
p.getHongBaoTakeInfo = function (hongBaoID) {
    if (this.hongBaoQueue[hongBaoID]) {
        return this.hongBaoQueue[hongBaoID].getHongBaoTakeInfo();
    }
    return null;
};

module.exports = new ActivityHongBaoSystem();
