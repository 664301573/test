/**
 * Created by orange on 2016/6/13.
 */
function UserRank() {
    this.userRankArray = [];

    //每十分钟排序一次
    var self = this;
    setInterval(function () {
        self.sort();
    }, 10 * 60 * 1000);
}

var p = UserRank.prototype;

p.push = function (userInfo) {

    this.userRankArray.push(userInfo);
};

p.sort = function () {
    this.userRankArray.sort(this._sortfn);
};

p._sortfn = function (first, second) {

    if (second.score >= 1500000000 && first.score >= 1500000000) {
        if (first.isAndroid) {
            return 1;
        }
        else if (second.isAndroid) {
            return -1;
        }
        return Math.random() - 0.5;
    }
    else {
        return second.score - first.score;
    }


};

p.getTopN = function (n) {
    if (n > this.userRankArray.length) {
        n = this.userRankArray.length;
    }

    var wordFilterArray = "";
    // var wordFilterArray = "买卖冲充退收售";

    var sortUser = this.userRankArray;
    var rankInfoArray = [];
    for (var i = 0; i < n; i++) {
        var item = {};
        item.gameID = sortUser[i].gameID;
        item.nickname = sortUser[i].nickname;
        item.score = sortUser[i].score;
        item.faceID = sortUser[i].faceID;
        for (var j = 0; j < wordFilterArray.length; ++j) {
            item.nickname = item.nickname.split(wordFilterArray[j]).join("**");
        }

        rankInfoArray.push(item);
    }

    return rankInfoArray;
};

module.exports = UserRank;