/**
 * Created by 黄二杰 on 2016/3/10.
 */

var ttutil = require('../Util.js');
var SitdownResultCode = require("../Define.js").SitdownResultCode;
var UserStatusConst = require("../Define.js").UserStatusConst;
var TableLockStatus = require("../Define.js").TableLockStatus;

var winston = require("winston");
var mpinfo = winston.info;
var mpdebug = winston.debug;
var mperror = winston.error;
var mpwarn = winston.warn;

//游戏椅子
var Chair = function () {

    this.user = null;                   //正在玩的用户对象
    this.lockForUserID = null;          //如果为空表示没有锁定， 如果有锁定， 则存的是为谁而锁定
};


var cp = Chair.prototype;


//游戏桌子
var Table = function (room, tableID, chairCount) {
    this.room = room;

    this.tableID = tableID;
    this.chairCount = chairCount;
    //椅子数组
    this.chairArray = [];

    this.status = TableLockStatus.unlock;
    //房主
    this.landlord = null;
    //详细记录索引数据库改动
    this.detailIndex = 0;

    //桌子密码
    this.tablePwd = null;

    this.init();
};

var p = Table.prototype;

p.init = function () {

    for (var i = 0; i < this.chairCount; ++i) {
        this.chairArray[i] = new Chair();
    }
};

/**
 * 获取桌子信息
 * @returns {{}}
 */
p.getTableInfo = function () {
    var info = {};

    info.tableID = this.tableID;
    info.chairCount = this.chairCount;
    info.status = this.status;

    return info;
};
/**
 * 解锁
 * @param chairID
 * @param userID
 * @constructor
 */
p.lockChair = function (chairID, userID) {
    var chair = this.chairArray[chairID];
    if (!chair) return false;

    if (chair.lockForUserID != null) return false;
    chair.lockForUserID = userID;

    return true;
};

/**
 * 锁住椅子
 * @param chairID
 * @constructor
 */
p.unLockChair = function (chairID) {
    var chair = this.chairArray[chairID];
    if (!chair) return;
    chair.lockForUserID = null;
};

p.removeUserByChairID = function (user, chairID) {
    if (!user) return;
    var chair = this.chairArray[chairID];
    if (!chair) return;
    if (chair.user && chair.user.userID == user.userID) {
        //锁着的用户 一般都是椅子上的用户 所以也清空
        chair.user = null;
        chair.lockForUserID = null;
    }
};


/**
 * 删除传入的user, 旁观
 */
p.onUserLeave = function (user, isRoomClose) {
    //如果有在坐位上
    if (user.tableID == this.tableID && user.chairID != null) {

        user.sitStatus = null;
        user.status = UserStatusConst.US_FREE;
        var info = user.getUserInfo();
        //通知房间玩家,用户离开 旁观用户不通知
        if (!isRoomClose) {
            if (this.room.cheatMode) {
                this.room.sendRoomUserData("UsersGameStatus", info, user.userID);
            } else {
                this.room.sendRoomUserData("UsersGameStatus", info);
            }
        }

        do {
            if (this.chairArray[user.chairID] == null) {
                mpdebug("房间：" + this.room.roomID + " 用户： " + user.userID + " 椅子号：" + user.chairID + "离开异常");
                break;
            }
            //清空位置
            this.chairArray[user.chairID].user = null;
            //解锁
            if (this.chairArray[user.chairID].lockForUserID == user.userID) {
                this.chairArray[user.chairID].lockForUserID = null;
            }
        } while (0);

        mpdebug("userID-" + user.userID + "-离开桌子" + this.tableID);
    }

    user.tableID = null;
    user.chairID = null;

    //玩家离开的时候解锁房间
    if (this.getRealPlayerNum() == 0) {
        this.clearTable();
    }
};


/**
 * 当用户坐下， ， 就是可能是旁观， 可能是玩, 可能坐下失败
 * 返回 0表示坐下失败, 1表示 玩
 * @param user
 * @param enterPwd 如果桌子需要密码， 则这是用户提供的密码
 * @param setPwd 如果是设置秘密， 则这里是用户设置秘密
 */
p.onUserSitdown = function (user, chairID, agentID, enterPwd, userID, uuid) {

    var chair = this.chairArray[chairID];

    if (chair == null || (chair.user == null && (chair.lockForUserID != null) && chair.lockForUserID != user.userID)) {
        return SitdownResultCode.FAIL_SYS;
    }

    // 机器人屏蔽
    if (this.status == TableLockStatus.readyLock || this.status == TableLockStatus.lock) {
        if (user.isAndroid) {
            this.onUserLeave(user);
            return SitdownResultCode.FAIL_SYS;
        }
    }

    //邀请进入房间处理
    var isUuid = false;

    if (uuid) {
        // uuid正确跳过密码验证
        if (uuid == this.uuid)
            isUuid = true;
        else
            return SitdownResultCode.ROOMINVITATION_FAILL;

    }

    if (!isUuid && this.status == TableLockStatus.lock && typeof enterPwd == "undefined") {
        return SitdownResultCode.PWD_REQUIRED;
    }

    if (!isUuid && this.status == TableLockStatus.lock && this.tablePwd != enterPwd) {
        return SitdownResultCode.PWD_ERROR;
    }

    // if(this.status == TableLockStatus.unlock && setPwd){
    //     if(this.getPlayerNum() > 0){
    //         console.log("该位置已经有人了" + setPwd);
    //         return SitdownResultCode.LOCK_ERROR;
    //     }else {
    //         console.log("密码设置成功" + setPwd);
    //         this.status = TableLockStatus.lock;
    //         this.owner = user;
    //         this.tablePwd = setPwd;
    //     }
    // }

    if (chair.user == null) {
        chair.user = user;
        user.tableID = this.tableID;
        user.chairID = chairID;
        return SitdownResultCode.SUCCESS_PLAYING;
    }
    return SitdownResultCode.FAIL_SYS;

};

/**
 * 清空桌子信息
 */
p.clearTable = function () {
    this.landlord = null;
    this.uuid = null;
    this.tablePwd = null;
    this.status = TableLockStatus.unlock;
    this.room.onTableStatus(this.tableID);
};

/**
 * 得到玩家数量
 */
p.getPlayerNum = function () {

    var playerNum = 0;
    for (var i = 0; i < this.chairCount; ++i) {
        if (this.chairArray[i] != null && this.chairArray[i].user != null) {
            playerNum++;
        }
    }
    return playerNum;
};

/**
 * 得到真实玩家数
 */
p.getRealPlayerNum = function () {
    var playerNum = 0;
    for (var i = 0; i < this.chairCount; ++i) {
        if (this.chairArray[i] != null && this.chairArray[i].user != null && !this.chairArray[i].user.isAndroid) {
            playerNum++;
        }
    }
    return playerNum;
};


/**
 * 获取空闲椅子ID
 */
p.getFreeChairID = function () {
    var start = Math.floor(Math.random() * this.chairCount);
    for (var i = 0; i < this.chairCount; ++i) {
        var index = (i + start) % this.chairCount;
        var chair = this.chairArray[index];
        if (chair.user == null && chair.lockForUserID == null) {
            return index;
        }
    }

    return null;
};

/**
 * 获取椅子
 * @param index
 * @returns {*}
 */
p.getChair = function (index) {
    return this.chairArray[index];
};

p.getChairUser = function (index) {
    return this.chairArray[index].user;
};

module.exports = Table;