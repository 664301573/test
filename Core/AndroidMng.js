/**
 * Created by Administrator on 2016/3/11.
 * 机器人管理模块
 */

function AndroidMng() {
    this.freeAndroid = [];                      //空闲机器人
}

var p = AndroidMng.prototype;

/**
 * 回收空闲机器人
 * @param android
 */
p.pushFreeAndroid = function (android) {
    if (android == null) return;
    this.freeAndroid.push(android);
};

/**
 * 获取一个空闲机器人
 * @returns {*}
 */
p.getFreeAndroid = function () {
    if (this.freeAndroid.length == 0) return null;
    var index = Math.floor(Math.random() * this.freeAndroid.length);
    var android = this.freeAndroid[index];
    this.freeAndroid.splice(index, 1);
    return android;
};

/**
 * 获取剩余机器人的数目
 * @returns {number}
 */
p.getFreeCount = function () {
    return this.freeAndroid.length;
};

module.exports = AndroidMng;