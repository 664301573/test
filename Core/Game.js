/**
 * Created by 黄二杰 on 2016/3/10.
 */

var ttutil = require("../Util.js");
var winston = require('winston');
var log = winston.info;
/**
 * 子游戏
 * @constructor
 */
var Game = function () {


    this.moduleID = null;

    //房间列表
    this.roomMap = {};

    this.init();
};


var p = Game.prototype;

/**
 *
 * @param data 数据库读取到的数据
 */
p.init = function () {


    return this;
};

/**
 * data为数据库传递过来的属性
 * @param data
 */
p.updateAttr = function (data, moduleID) {
    this.moduleID = moduleID;
    ttutil.copyAttr(this, data);

};

/**
 * 增加一个房间
 */
p.addRoom = function (roomID, room) {
    if (roomID == null) return;
    //子游戏开房间
    this.roomMap[roomID] = room;
};

/**
 * 删除房间
 * @param roomID
 */
p.removeRoom = function (roomID) {
    this.roomMap[roomID] = null;
};


/**
 * 当用户离开调用
 * @param user
 */
p.onUserLeave = function (user) {

    if (user.moduleID == this.moduleID) {

        if (user.roomID != null) {
            this.roomMap[user.roomID] && this.roomMap[user.roomID].onUserLeave(user);
        }
    }

    user.moduleID = null;
    log("userID-" + user.userID + "-离开" + this.moduleID + "游戏");
};

/**
 * 得到房间列表信息
 */
p.getRoomArrayInfo = function () {
};

module.exports = Game;