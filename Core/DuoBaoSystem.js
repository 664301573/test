/**
 * Created by Apple on 2016/11/22.
 */


var GD = require('../GD');
var SqlPool = require('../SqlPool');
var winston = require("winston");
var ttutil = require('../Util.js');

//宝物模板
var PrizeTemplate = function (shapeID, worth, needMoney) {

    this.shapeID = shapeID;
    this.worth = worth;
    this.needMoney = needMoney || worth + worth * 0.05;
};

//宝物状态
var Prize = function (id, shapeID, worth, needMoney) {
    this.id = id;

    this.shapeID = shapeID;
    this.worth = worth;
    this.needMoney = needMoney;
    this.createTime = new Date();
    this.awardTime = null;
    this.winUserID = null;
    this.joinNum = 0;
    this.nowMoney = 0;

    //投宝数组, userID做key, money作value
    this.betMap = {};

    //是否锁住了， 不让投宝了
    this.lock = false;
    this.handleNum = 0; //正在处理的夺宝操作
};


var DuoBaoSystem = function () {

    //宝物种类
    this.prizeTemplateArray = {
        1: new PrizeTemplate(1, 200000, 0),
        2: new PrizeTemplate(2, 500000, 0),
        3: new PrizeTemplate(3, 1000000, 0),
        4: new PrizeTemplate(4, 2000000, 0),
        5: new PrizeTemplate(5, 5000000, 0),
        6: new PrizeTemplate(6, 10000000, 0),
        7: new PrizeTemplate(7, 20000000, 0),
        8: new PrizeTemplate(8, 50000000, 0),
        9: new PrizeTemplate(9, 100000000, 0),
        // 10: new PrizeTemplate(10, 200000, 0),
        // 11: new PrizeTemplate(11, 300000, 0),
        // 12: new PrizeTemplate(12, 400000, 0),
        // 13: new PrizeTemplate(13, 500000, 0),
        // 14: new PrizeTemplate(14, 600000, 0),
        // 15: new PrizeTemplate(15, 700000, 0),
        // 16: new PrizeTemplate(16, 800000, 0),
        // 17: new PrizeTemplate(17, 1000000, 0),
        // 18: new PrizeTemplate(18, 2000000, 0),
        // 19: new PrizeTemplate(19, 3000000, 0),
        // 20: new PrizeTemplate(20, 4000000, 0),
        // 21: new PrizeTemplate(21, 5000000, 0),
        // 22: new PrizeTemplate(22, 6000000, 0),
        // 23: new PrizeTemplate(23, 7000000, 0),
        // 24: new PrizeTemplate(24, 8000000, 0),
        // 25: new PrizeTemplate(25, 10000000, 0),
        // 26: new PrizeTemplate(26, 20000000, 0),
        // 27: new PrizeTemplate(27, 30000000, 0),
        // 28: new PrizeTemplate(28, 40000000, 0),
        // 29: new PrizeTemplate(29, 50000000, 0),
        // 30: new PrizeTemplate(30, 60000000, 0),
        // 31: new PrizeTemplate(31, 70000000, 0),
        // 32: new PrizeTemplate(32, 80000000, 0),
        // 33: new PrizeTemplate(33, 100000000, 0),
        // 34: new PrizeTemplate(34, 200000000, 0),
        // 35: new PrizeTemplate(35, 300000000, 0),
        // 36: new PrizeTemplate(36, 400000000, 0),
        // 37: new PrizeTemplate(37, 500000000, 0),
        // 38: new PrizeTemplate(38, 600000000, 0),
        // 39: new PrizeTemplate(39, 700000000, 0),
        // 40: new PrizeTemplate(40, 800000000, 0),
    };

    //当前宝物数组
    this.nowPrizeMap = {};

    //上盘宝物数组
    this.prePrizeMap = {};

    //prizeID映射Prize
    this.prizeID2Prize = {};

    //进入夺宝场景的玩家
    this.realUserArray = [];

    this.init();
};


var p = DuoBaoSystem.prototype;

p.removeUser = function (user) {
    ttutil.arrayRemove(this.realUserArray, user);
};

p.addUser = function (user) {
    this.removeUser(user);
    this.realUserArray.push(user);
};

//从数据读取 未被夺走的宝物信息
p.init = function () {

    SqlPool.x_query("select * from ps_duo_bao_prize where winUserID is null;", [], (err, res) => {
        if (err) {
            return;
        }

        for (var i = 0; i < res.length; ++i) {

            var prize = new Prize(res[i].id, res[i].shapeID, res[i].worth, res[i].needMoney);
            prize.createTime = res[i].createTime;
            prize.awardTime = res[i].awardTime;
            prize.winUserID = res[i].winUserID;
            prize.joinNum = res[i].joinNum;
            prize.nowMoney = res[i].nowMoney;
            this.nowPrizeMap[prize.shapeID] = prize;

            this.loadDuoBaoLog(res[i].id, prize.shapeID);
        }

        //没有宝物的生成
        for (var shapeID in this.prizeTemplateArray) {
            if (!this.nowPrizeMap[shapeID]) {
                this.generatePrize(shapeID);
            }
        }
    });

    for (var shapeID in this.prizeTemplateArray) {
        this.loadPreDuoBaoLog(shapeID);
    }


};


/**
 * 载入上一盘的数据
 * @param shapeID
 */
p.loadPreDuoBaoLog = function (shapeID) {

    SqlPool.x_query("call ps_duo_bao_pre_prize_log(?);", [shapeID], (err, res) => {
        if (err) {
            return;
        }
        if (res[0] && res[1]) {

            var prize = new Prize(res[0][0].id, res[0][0].shapeID, res[0][0].worth, res[0][0].needMoney);
            prize.createTime = res[0][0].createTime;
            prize.awardTime = res[0][0].awardTime;
            prize.winUserID = res[0][0].winUserID;
            prize.joinNum = res[0][0].joinNum;
            prize.nowMoney = res[0][0].nowMoney;
            this.prePrizeMap[prize.shapeID] = prize;


            for (var i = 0; i < res[1].length; ++i) {
                var userID = res[1][i].userID;
                prize.betMap[userID] = prize.betMap[userID] || 0;
                prize.betMap[userID] += res[1][i].money;
            }
        }

    });

};

/**
 * 载入单个宝物的 夺宝记录
 * @param prizeID
 */
p.loadDuoBaoLog = function (prizeID, shapeID) {
    SqlPool.x_query("select * from ps_duo_bao_log where prizeID = ?;", [prizeID], (err, res) => {
        if (err) {
            return;
        }
        var prizeInfo = this.nowPrizeMap[shapeID];

        for (var i = 0; i < res.length; ++i) {
            var userID = res[i].userID;
            prizeInfo.betMap[userID] = prizeInfo.betMap[userID] || 0;
            prizeInfo.betMap[userID] += res[i].money;
        }
    });

};

/**
 * 获取宝物 的记录只显示同型号的 最近10条
 * @param eventName
 * @param socket
 * @param data
 */
p.getPrizeLog = function (eventName, socket, data) {

    SqlPool.x_query("call ps_duo_bao_prize_log(?,?,?);", [data.shapeID, 0, 10], (err, res) => {
        if (err) {
            ttutil.psSendSocketData(socket, eventName, {errMsg: "系统出错"});
            return;
        }
        ttutil.psSendSocketData(socket, eventName, {prizeLog: res[0], shapeID: data.shapeID});
    });

};

/**
 * 获取宝物的押宝记录统计
 * @param eventName
 * @param socket
 * @param data
 */
p.getPrizeBetStat = function (eventName, socket, data) {

    if (!data || !data.prizeID || !data.shapeID) {
        ttutil.psSendSocketData(socket, eventName, {errMsg: "参数错误"});
        return;
    }

    //查前一期的统计
    if (data.prizeID == -1) {
        if (this.prePrizeMap[data.shapeID]) {

            var betArray = [];
            var betMap = this.prePrizeMap[data.shapeID].betMap;
            var cacheUserInfoMap = GD.cacheUserInfoMap;
            for (var userID  in betMap) {
                betArray.push({
                    money: betMap[userID],
                    nickname: cacheUserInfoMap[userID] && cacheUserInfoMap[userID].nickname
                });
            }
            betArray.sort(function (a, b) {
                return b.money - a.money;
            });

            var winUserID = this.prePrizeMap[data.shapeID].winUserID;
            ttutil.psSendSocketData(socket, eventName, {
                betArray: betArray,
                prizeID: this.prePrizeMap[data.shapeID].id,
                shapeID: data.shapeID,
                winNickname: cacheUserInfoMap[winUserID] && cacheUserInfoMap[winUserID].nickname
            });
        }
        else {
            ttutil.psSendSocketData(socket, eventName, {errMsg: "没有记录"});
            return;
        }

    }
    else {
        if (this.nowPrizeMap[data.shapeID] && this.nowPrizeMap[data.shapeID].id == data.prizeID) {

            var betArray = [];
            var betMap = this.nowPrizeMap[data.shapeID].betMap;
            var cacheUserInfoMap = GD.cacheUserInfoMap;
            for (var userID  in betMap) {
                betArray.push({
                    money: betMap[userID],
                    nickname: cacheUserInfoMap[userID] && cacheUserInfoMap[userID].nickname
                });
            }
            betArray.sort(function (a, b) {
                return b.money - a.money;
            });
            ttutil.psSendSocketData(socket, eventName, {
                betArray: betArray,
                prizeID: data.prizeID,
                shapeID: data.shapeID
            });
        }
        else {
            ttutil.psSendSocketData(socket, eventName, {errMsg: "所查信息过期"});
            return;
        }
    }


};


p.doDuoBao = function (eventName, socket, data) {

    if (!data || !data.prizeID || !data.shapeID || !data.money || data.money < 0) {
        ttutil.psSendSocketData(socket, eventName, {errMsg: "参数错误"});
        return;
    }

    if (!this.nowPrizeMap[data.shapeID] || data.prizeID != this.nowPrizeMap[data.shapeID].id) {
        ttutil.psSendSocketData(socket, eventName, {errMsg: "所选宝物被人夺走或有错"});
        return;
    }

    var userID = socket.userID;

    var prize = this.nowPrizeMap[data.shapeID];

    if (prize.lock) {
        ttutil.psSendSocketData(socket, eventName, {errMsg: "请稍候, 系统正在抽奖"});
        return;
    }

    if (GD.realUserMap[userID] && GD.realUserMap[userID].roomID != null) {
        ttutil.psSendSocketData(socket, eventName, {errMsg: "您正在游戏房间中，无权进行此操作"});
        return;
    }

    if (GD.realUserMap[userID] && GD.realUserMap[userID].score >= data.money) {

        data.money = Math.min(data.money, prize.needMoney - prize.nowMoney);
        //扣掉身上的分
        GD.realUserMap[userID].setDBInfo({score: GD.realUserMap[userID].score - data.money});
        prize.handleNum++;
        prize.nowMoney += data.money;
        if (prize.nowMoney >= prize.needMoney) {
            //先锁住
            prize.lock = true;
        }

        SqlPool.x_query("call ps_duo_bao(?,?,?);", [userID, data.prizeID, data.money], (err, result) => {

            prize.handleNum--;
            if (err || result[0][0].res == 1) {
                prize.nowMoney -= data.money;
                ttutil.psSendSocketData(socket, eventName, {errMsg: "系统出错!"});
                if (prize.lock) {
                    prize.lock = false;
                }
                return;
            }

            //更新信息用户
            if (GD.realUserMap[userID]) {
                ttutil.psSendSocketData(GD.realUserMap[userID].socket, "UserInfoUpdate", {score: GD.realUserMap[userID].score});
            }

            prize.betMap[userID] = prize.betMap[userID] || 0;
            prize.betMap[userID] += data.money;
            //夺宝操作成功
            ttutil.psSendSocketData(socket, eventName, {
                nowMoney: prize.nowMoney,
                meBetMoney: prize.betMap[userID] || 0,
                prizeID: prize.id,
                shapeID: prize.shapeID
            });

            ////////////////////////////////////////////////////////////////////////////////////////////
            //广播在夺宝界面的用户
            for (var i = 0; i < this.realUserArray.length; ++i) {
                if (this.realUserArray[i].userID != userID) {
                    ttutil.psSendSocketData(this.realUserArray[i].socket, "DuoBaoUpdateInfo", {
                        nowMoney: prize.nowMoney,
                        meBetMoney: prize.betMap[this.realUserArray[i].userID] || 0,
                        prizeID: prize.id,
                        shapeID: prize.shapeID,
                        type: 1,
                    });
                }
            }
            ////////////////////////////////////////////////////////////////////////////////////////////

            if (prize.lock && prize.handleNum == 0) {
                this.doPrizeDraw(prize);
            }

        });

    }
    else {
        ttutil.psSendSocketData(socket, eventName, {errMsg: "您的" + CURRENCY + "不足"});
    }


};


//抽奖
p.doPrizeDraw = function (prize) {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //抽奖
    var betArray = [];

    for (var userID in prize.betMap) {
        betArray.push({userID: userID, money: prize.betMap[userID]});
    }

    var betSumArray = [betArray[0].money];
    for (var i = 1; i < betArray.length; ++i) {
        betSumArray[i] = betSumArray[i - 1] + betArray[i].money;
    }

    var key = Math.random() * betSumArray[betSumArray.length - 1];

    var winIndex = 0;
    for (var i = 0; i < betSumArray.length; ++i) {
        if (key < betSumArray[i]) {
            winIndex = i;
            break;
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //赢家
    var winUserID = betArray[winIndex].userID;

    SqlPool.x_query('call ps_duo_bao_prize_draw(?, ?)', [winUserID, prize.id], (err, rows, fields) => {
        if (err) {
            return;
        }

        if (rows[0][0].res != 0) {
            winston.error("一元夺宝 开奖失败", rows[0][0].res, winUserID, prize.id);
            return;
        }
        //更新信息用户
        if (GD.realUserMap[winUserID]) {
            GD.realUserMap[winUserID].setDBInfo({bankScore: GD.realUserMap[winUserID].bankScore + prize.worth});
            ttutil.psSendSocketData(GD.realUserMap[winUserID].socket, "UserInfoUpdate", {bankScore: GD.realUserMap[winUserID].bankScore});
        }
        prize.winUserID = winUserID;

        this.prePrizeMap[prize.shapeID] = prize;

        this.writePrizeEmail(prize);

        this.generatePrize(prize.shapeID);
        // prize.lock = false;
        /////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        //广播在夺宝界面的用户
        var winNickname = GD.cacheUserInfoMap[prize.winUserID] && GD.cacheUserInfoMap[prize.winUserID].nickname;

        for (var i = 0; i < this.realUserArray.length; ++i) {

            ttutil.psSendSocketData(this.realUserArray[i].socket, "DuoBaoUpdateInfo", {
                winNickname: winNickname,
                prizeId: prize.id,
                worth: prize.worth,
                shapeID: prize.shapeID,
                type: 2
            });

        }
        //////////////////////////////////////////////////////////////
    });

};

//发邮件
p.writePrizeEmail = function (prize) {

    var winNickname = GD.cacheUserInfoMap[prize.winUserID] && GD.cacheUserInfoMap[prize.winUserID].nickname;

    this.writeEmail(prize.winUserID, "[活动]-恭喜您在一金币夺宝活动夺得编号" + prize.id + ",价值 " + prize.worth + " 的宝物, 请往保险柜查收");

    for (var userID in prize.betMap) {
        if (userID != prize.winUserID) {
            this.writeEmail(userID, "[活动]-您参与的一金币夺宝活动编号为" + prize.id + ",价值 " + prize.worth + " 的宝物, 被" + winNickname + "夺走了!");
        }

    }

};

//发邮件
p.writeEmail = function (userID, title) {

    GD.plazaServer.writeUserMail(userID, title, "如题");

};


/**
 * 获取当前宝物情况
 */
p.getPrizeInfo = function (eventName, socket, data) {

    var userID = socket.userID;
    //把当前的所有宝物发下去
    if (!data || !data.prizeID) {

        var prizeMap = {};
        for (var shapeID in this.nowPrizeMap) {
            if (this.nowPrizeMap[shapeID]) {
                prizeMap[shapeID] = {};
                prizeMap[shapeID].shapeID = this.nowPrizeMap[shapeID].shapeID;
                prizeMap[shapeID].worth = this.nowPrizeMap[shapeID].worth;
                prizeMap[shapeID].needMoney = this.nowPrizeMap[shapeID].needMoney;
                prizeMap[shapeID].createTime = this.nowPrizeMap[shapeID].createTime;
                prizeMap[shapeID].joinNum = this.nowPrizeMap[shapeID].joinNum;
                prizeMap[shapeID].nowMoney = this.nowPrizeMap[shapeID].nowMoney;
                prizeMap[shapeID].id = this.nowPrizeMap[shapeID].id;
                prizeMap[shapeID].meBetMoney = this.nowPrizeMap[shapeID].betMap[userID] || 0;
            }
        }

        ttutil.psSendSocketData(socket, eventName, prizeMap);
    }
    //发送指定宝物信息
    else {

        if (this.prizeID2Prize[data.prizeID]) {
            var prizeInfo = this.prizeID2Prize[data.prizeID];
            ttutil.psSendSocketData(socket, eventName, {
                id: prizeInfo.id,
                shapeID: prizeInfo.shapeID,
                worth: prizeInfo.worth,
                needMoney: prizeInfo.needMoney,
                createTime: prizeInfo.createTime.toLocaleString(),
                joinNum: prizeInfo.joinNum,
                nowMoney: prizeInfo.nowMoney,
                winNickname: GD.cacheUserInfoMap[prizeInfo.winUserID].nickname,
                awardTime: prizeInfo.awardTime.toLocaleString(),
                prizeID: data.prizeID,
            });
        }
        else {
            SqlPool.x_query("select * from ps_duo_bao_prize where id = ?;", [data.prizeID], (err, result) => {

                if (err) {
                    ttutil.psSendSocketData(socket, eventName, {errMsg: "系统出错!"});
                    return;
                }
                if (result && result[0]) {
                    //缓存
                    this.prizeID2Prize[result[0].id] = result[0];

                    var prizeInfo = result[0];

                    ttutil.psSendSocketData(socket, eventName, {
                        id: prizeInfo.id,
                        shapeID: prizeInfo.shapeID,
                        worth: prizeInfo.worth,
                        needMoney: prizeInfo.needMoney,
                        createTime: prizeInfo.createTime.toLocaleString(),
                        joinNum: prizeInfo.joinNum,
                        nowMoney: prizeInfo.nowMoney,
                        winNickname: GD.cacheUserInfoMap[prizeInfo.winUserID].nickname,
                        awardTime: prizeInfo.awardTime.toLocaleString(),
                        prizeID: data.prizeID,
                    });
                }
                else {
                    ttutil.psSendSocketData(socket, eventName, {errMsg: "所查记录不存在"});
                }
            });
        }
    }


};

/**
 * 生成宝物
 */
p.generatePrize = function (shapeID) {

    var prizeTemplate = this.prizeTemplateArray[shapeID];

    SqlPool.x_query("call ps_duo_bao_generate_prize(?,?,?)", [prizeTemplate.shapeID, prizeTemplate.needMoney, prizeTemplate.worth], (err, result) => {

        if (err) {
            return;
        }
        if (result && result[0] && result[0][0]) {
            this.nowPrizeMap[shapeID] = new Prize(result[0][0].prizeID, prizeTemplate.shapeID, prizeTemplate.worth, prizeTemplate.needMoney);


            //广播在夺宝界面的用户
            for (var i = 0; i < this.realUserArray.length; ++i) {

                //宝物刷新
                ttutil.psSendSocketData(this.realUserArray[i].socket, "DuoBaoUpdateInfo", {
                    shapeID: shapeID,
                    id: result[0][0].prizeID,
                    type: 3
                });

            }
        }
    });

};


module.exports = new DuoBaoSystem();