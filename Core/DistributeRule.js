/**
 * Created by Administrator on 2016/3/11.
 * 游戏机器人分配策略文件 委托模式 无继承 有点坑
 */


var UserStatusConst = require("../Define.js").UserStatusConst;
var RoomConst = require("../Define.js").RoomConst;
var winston = require("winston");

/**
 * 普通房间分配规则
 */

function GeneralRule(room) {
    this.room = room;

    this.standUpTimer = null;               //起立定时器
    this.thresholdTimer = null;             //阀值控制定时器
    this.attendTimer = null;                //陪打定时器

    this.tableWeightArray = [];                  //各个桌子权重
    this.totalWeight = null;                //重权重

    this.calcuteTableWeight();
}

var gp = GeneralRule.prototype;

/**
 * 计算权重 函数 f(tableID) = 2 * tableID  算面积
 */
gp.calcuteTableWeight = function () {
    var tableCount = this.room.tableCount;
    var perWeight = 4;                                          //调参数
    this.totalWeight = tableCount * tableCount * perWeight / 2; //总面积

    var crtWeight = 0;

    for (var i = 0; i < tableCount; i++) {
        var otherWeight = Math.pow(tableCount - i - 1, 2) * perWeight / 2;
        this.tableWeightArray[i] = this.totalWeight - otherWeight - crtWeight;
        crtWeight += this.tableWeightArray[i];
    }
};

/**
 * 根据权重获取桌子ID
 */
gp.getTableIDByWeight = function () {
    var crtWeight = 0;
    var randomWeight = Math.random() * this.totalWeight;

    for (var i = 0; i < this.tableWeightArray.length; i++) {
        crtWeight += this.tableWeightArray[i];
        if (randomWeight < crtWeight) {
            return i;
        }
    }

    return null;
};

/**
 * 开始分配
 */
gp.startDistribute = function () {
    //先添加一堆机器人到这个房间中
    var room = this.room;
    room.addAndroid(room.presetAndroidCount);

    this.standUpTimer = setInterval(this.androidStandUp.bind(this), 60000);
    this.thresholdTimer = setInterval(this.controlThreshold.bind(this), 20000);
    this.attendTimer = setInterval(this.androidAttend.bind(this), 8000);
};

/**
 * 停止分配
 */
gp.stopDistribute = function () {
    this.standUpTimer && clearInterval(this.standUpTimer);
    this.thresholdTimer && clearInterval(this.thresholdTimer);
    this.attendTimer && clearInterval(this.attendTimer);

    var len = this.room.androidUserArray.length;
    while (len > 0) {
        var android = this.room.androidUserArray[0];
        this.room.onUserLeave(android);
        len--;
    }
};

/**
 * 起立处理
 */
gp.androidStandUp = function () {
    var room = this.room;
    var randCount = Math.ceil(Math.random() * 3);
    var index = Math.floor(Math.random() * room.tableCount);

    var tableIndex = 0;
    for (var i = index; i < room.tableCount + index; ++i) {
        tableIndex = i % room.tableCount;
        var table = room.tableArray[tableIndex];
        if (table == null) continue;

        var totalPlayer = table.getPlayerNum();
        var realPlayer = table.getRealPlayerNum();

        if (totalPlayer == 0) continue;
        if (totalPlayer - realPlayer == 0) continue;
        //随机一下
        if (Math.random() > 1 / totalPlayer) continue;

        for (var j = 0; j < table.chairCount; j++) {
            var user = table.getChairUser(j);
            if (!user || !user.isAndroid || user.status != UserStatusConst.US_SIT && user.status != UserStatusConst.US_READY) continue;
            if (randCount <= 0) return;
            if (Math.random() > 0.5) continue;

            room.onUserLeave(user);
            randCount--;
        }
    }
};

/**
 * 控制阀值
 */
gp.controlThreshold = function () {
    var room = this.room;

    var totalPlayer = room.realUserArray.length + room.androidUserArray.length;

    if (totalPlayer < room.presetAndroidCount) {

        var count = room.presetAndroidCount - totalPlayer;
        room.addAndroid(count);

    } else if (totalPlayer > room.presetAndroidCount) {

        var count = totalPlayer - room.presetAndroidCount;
        var start = Math.floor(Math.random() * room.androidUserArray.length);

        //可能删除时 i会越步 但是由于定时检测所以可以越步
        for (var i = 0; i < room.androidUserArray.length; i++) {
            if (count == 0) break;
            var index = (i + start) % room.androidUserArray.length;
            var android = room.androidUserArray[index];
            if (!android) continue;
            if (android.status == UserStatusConst.US_FREE || android.status == UserStatusConst.US_READY || android.status == UserStatusConst.US_SIT) {
                room.onUserLeave(android);
                count--;
            }
        }
    }
};

/**
 * 陪打
 */
gp.androidAttend = function () {
    var room = this.room;
    //操作数
    var opCount = Math.ceil(Math.random() * 2);

    var aLen = room.androidUserArray.length;

    var startIndex = Math.floor(Math.random() * aLen);

    //分配
    for (var i = startIndex; i < aLen + startIndex; ++i) {
        var index = i % aLen;
        var android = room.androidUserArray[index];
        if (android == null || android.status != UserStatusConst.US_FREE) continue;

        var actionWeight = Math.ceil(Math.random() * 10);

        if (actionWeight > 8) {             //找真人

            var tStartIndex = Math.floor(Math.random() * room.tableCount);
            for (var j = 0 + tStartIndex; j < room.tableCount + tStartIndex; ++j) {

                var tableID = j % room.tableCount;

                //if (room.tableArray[tableID].lockPWD != null) continue;
                var realNum = room.tableArray[tableID].getRealPlayerNum();
                var playerNum = room.tableArray[tableID].getPlayerNum();

                if (realNum == 0 || playerNum == 0 || playerNum == room.chairCount) continue;

                //权重判断再一次 根据人数判断

                var chairID = room.tableArray[tableID].getFreeChairID();
                if (chairID == null) continue;


                room.onUserSitdown(android, tableID, chairID);
                winston.info("找真人");
                opCount--;
                break;
            }

        } else if (actionWeight > 4) {      //找机器人

            var preWeigth = this.tableWeightArray[0] * 2;
            for (var tableID = 0; tableID < room.tableArray.length; tableID++) {
                var weight = Math.random() * preWeigth;
                //不符合条件继续
                if (weight > this.tableWeightArray[0]) continue;

                var table = room.tableArray[tableID];
                // if (table.lockPWD != null) continue;

                var realNum = table.getRealPlayerNum();
                var playerNum = table.getPlayerNum();

                if (realNum != 0 || playerNum == 0 || playerNum == room.chairCount) continue;

                var chairWeight = Math.random() * (room.chairCount * 2);
                if (room.chairCount - playerNum < chairWeight) continue;

                var chairID = table.getFreeChairID();
                if (chairID == null) continue;
                room.onUserSitdown(android, tableID, chairID);
                opCount--;
                break;
            }
        } else if (actionWeight > 2) {      //随机坐下

            var preWeigth = this.tableWeightArray[0] * 2;
            for (var tableID = 0; tableID < room.tableArray.length; tableID++) {
                var weight = Math.random() * preWeigth;
                //不符合条件继续
                if (weight > this.tableWeightArray[0]) continue;

                var table = room.tableArray[tableID];
                //if (table.lockPWD != null) continue;

                var playerNum = table.getPlayerNum();

                if (playerNum == room.chairCount) continue;


                var chairWeight = Math.random() * (room.chairCount * 2);
                if (room.chairCount - playerNum < chairWeight) continue;

                var chairID = table.getFreeChairID();
                if (chairID == null) continue;


                room.onUserSitdown(android, tableID, chairID);
                opCount--;
                break;
            }
        }

        if (opCount <= 0) return;
    }
};


/**
 * 陪打
 */
gp.androidAttend4Preset = function () {
    var room = this.room;
    //操作数
    var opCount = Math.ceil(Math.random() * 3);

    var aLen = room.androidUserArray.length;

    var startIndex = Math.floor(Math.random() * aLen);

    //分配
    for (var i = startIndex; i < aLen + startIndex; ++i) {
        var index = i % aLen;
        var android = room.androidUserArray[index];
        if (android == null || android.status != UserStatusConst.US_FREE) continue;

        var actionWeight = Math.ceil(Math.random() * 10);

        if (actionWeight > 7) {             //找真人

            for (var tableID = 0; tableID < room.tableCount; ++tableID) {

                //if (room.tableArray[tableID].lockPWD != null) continue;
                var realNum = room.tableArray[tableID].getRealPlayerNum();
                var playerNum = room.tableArray[tableID].getPlayerNum();

                if (realNum == 0 || playerNum == 0 || playerNum == room.chairCount) continue;

                //权重判断再一次 根据人数判断

                var chairID = room.tableArray[tableID].getFreeChairID();
                if (chairID == null) continue;

                room.onUserSitdown(android, tableID, chairID);
                opCount--;
                break;
            }

        } else if (actionWeight > 4) {      //找机器人

            var tableID = this.getTableIDByWeight();
            if (tableID == null || room.tableArray[tableID] == null) continue;

            var table = room.tableArray[tableID];
            //if (table.lockPWD != null) continue;

            var realNum = table.getRealPlayerNum();
            var playerNum = table.getPlayerNum();

            if (realNum != 0 || playerNum == 0 || playerNum == room.chairCount) continue;

            var chairID = table.getFreeChairID();
            if (chairID == null) continue;

            room.onUserSitdown(android, tableID, chairID);
            opCount--;


        } else if (actionWeight > 2) {      //随机坐下

            var tableID = this.getTableIDByWeight();
            if (tableID == null || room.tableArray[tableID] == null) continue;

            var table = room.tableArray[tableID];
            //if (table.lockPWD != null) continue;

            var chairID = table.getFreeChairID();
            if (chairID == null) continue;


            room.onUserSitdown(android, tableID, chairID);
            opCount--;
        }

        if (opCount <= 0) return;
    }
};


/**
 * 防作弊房分配规则
 */
function AntiCheatingRule(room) {
    this.room = room;
    this.standUpTimer = null;           //起立定时器
    this.thresholdTimer = null;             //阀值控制定时器
    this.attendTimer = null;                //陪打定时器
}

var ap = AntiCheatingRule.prototype;

ap.startDistribute = function () {
    //先添加一堆机器人到这个房间中
    var room = this.room;
    room.addAndroid(room.presetAndroidCount);

    this.standUpTimer = setInterval(this.androidStandUp.bind(this), 60000);
    this.thresholdTimer = setInterval(this.controlThreshold.bind(this), 20000);
    this.attendTimer = setInterval(this.androidAttend.bind(this), 8000);
};

ap.stopDistribute = function () {
    this.standUpTimer && clearInterval(this.standUpTimer);
    this.thresholdTimer && clearInterval(this.thresholdTimer);
    this.attendTimer && clearInterval(this.attendTimer);

    var len = this.room.androidUserArray.length;
    while (len > 0) {
        var android = this.room.androidUserArray[0];
        this.room.onUserLeave(android);
        len--;
    }
};

/**
 * 清理没有真人的机器人
 */
ap.androidStandUp = function () {
    var room = this.room;

    for (var i = 0; i < room.tableCount; ++i) {
        var table = room.tableArray[i];
        if (table == null) continue;
        var realPlayer = table.getRealPlayerNum();
        if (realPlayer != 0) continue;
        var totalPlayer = table.getPlayerNum();
        if (totalPlayer == 0) continue;

        for (var chairIndex = 0; chairIndex < table.chairCount; chairIndex++) {
            var user = table.getChairUser(chairIndex);
            if (!user || !user.isAndroid || user.status == UserStatusConst.US_PLAYING) continue;
            room.onUserLeave(user);
        }
    }
};

ap.controlThreshold = function () {
    var room = this.room;

    var totalPlayer = room.realUserArray.length + room.androidUserArray.length;

    if (totalPlayer < room.presetAndroidCount) {

        var count = room.presetAndroidCount - totalPlayer;
        room.addAndroid(count);

    } else if (totalPlayer > room.presetAndroidCount) {

        var count = totalPlayer - room.presetAndroidCount;
        var start = Math.floor(Math.random() * room.androidUserArray.length);

        //可能删除时 i会越步 但是由于定时检测所以可以越步
        for (var i = 0; i < room.androidUserArray.length; i++) {
            if (count == 0) break;
            var index = (i + start) % room.androidUserArray.length;
            var android = room.androidUserArray[index];
            if (!android) continue;
            if (android.status == UserStatusConst.US_FREE) {
                room.onUserLeave(android);
                count--;
            }
        }
    }
};

ap.androidAttend = function () {
    var room = this.room;
    for (var i = 0; i < room.tableCount; ++i) {
        var table = room.tableArray[i];
        if (table == null) continue;
        var realPlayer = table.getRealPlayerNum();
        if (realPlayer == 0) continue;

        var totalPlayer = table.getPlayerNum();

        var attendCount = 0;
        //空闲数目
        var freeCount = table.chairCount - totalPlayer;

        if (freeCount == 0) continue;

        if (room.roomMode == RoomConst.MD_FULL_READY) {          //人满模式 斗地主等
            attendCount = freeCount;
        } else if (realPlayer == totalPlayer) {                  //只有真人的话, 考虑机器人 加一两个机器人
            attendCount = 1;
        } else {                                                //随机概率加进去
            var can = freeCount / table.chairCount * 0.5;
            if (Math.random() < can) continue;
            attendCount = 1;
        }

        if (attendCount == 0) continue;

        var index = Math.floor(Math.random() * room.androidUserArray.length);

        for (var j = index; j < room.androidUserArray.length + index; j++) {

            if (attendCount <= 0) break;

            var androidIndex = j % room.androidUserArray.length;
            var user = room.androidUserArray[androidIndex];

            if (!user) continue;
            if (user.status != UserStatusConst.US_FREE) continue;


            var chairID = table.getFreeChairID();
            if (chairID == null) break;

            room.onUserSitdown(user, i, chairID);
            attendCount--;
        }
    }
};


/**
 * 普通房间分配规则
 */

function HundredRule(room) {
    this.room = room;

    this.standUpTimer = null;               //起立定时器
    this.thresholdTimer = null;             //阀值控制定时器
    this.attendTimer = null;                //陪打定时器
}

var hp = HundredRule.prototype;

/**
 * 开始分配
 */
hp.startDistribute = function () {
    //先添加一堆机器人到这个房间中
    var room = this.room;
    room.addAndroid(room.presetAndroidCount);

    this.standUpTimer = setInterval(this.androidStandUp.bind(this), 180000);
    this.thresholdTimer = setInterval(this.controlThreshold.bind(this), 60000);
    this.attendTimer = setInterval(this.androidAttend.bind(this), 60000);
    this.firstSitdown();
};

/**
 * 停止分配
 */
hp.stopDistribute = function () {
    this.standUpTimer && clearInterval(this.standUpTimer);
    this.thresholdTimer && clearInterval(this.thresholdTimer);
    this.attendTimer && clearInterval(this.attendTimer);

    var len = this.room.androidUserArray.length;
    while (len > 0) {
        var android = this.room.androidUserArray[0];
        this.room.onUserLeave(android);
        len--;
    }
};

/**
 * 房间开启时坐下
 */
hp.firstSitdown = function () {
    var room = this.room;
    var aLen = Math.floor(room.androidUserArray.length / 2);
    for (var i = 0; i < aLen; ++i) {
        var android = room.androidUserArray[i];
        if (android == null || android.status != UserStatusConst.US_FREE) continue;

        for (var tableID = 0; tableID < room.tableArray.length; tableID++) {
            var table = room.tableArray[tableID];
            var chairID = table.getFreeChairID();
            if (chairID == null) continue;
            room.onUserSitdown(android, tableID, chairID);
            break;
        }
    }
};

/**
 * 起立处理
 */
hp.androidStandUp = function () {
    var room = this.room;
    var randCount = Math.ceil(Math.random() * 2);

    for (var i = 0; i < room.tableCount; ++i) {
        var table = room.tableArray[i];
        if (table == null) continue;

        var index = Math.floor(Math.random() * table.chairCount);

        for (var start = index; start < table.chairCount + index; start++) {

            var j = start % table.chairCount;

            var user = table.getChairUser(j);
            if (!user || !user.isAndroid || user.status != UserStatusConst.US_SIT && user.status != UserStatusConst.US_READY) continue;
            if (randCount <= 0) return;
            if (Math.random() > 0.5) continue;

            this.room.onUserLeave(user);
            randCount--;
        }
    }
};

/**
 * 控制阀值
 */
hp.controlThreshold = function () {
    var room = this.room;

    var totalPlayer = room.realUserArray.length + room.androidUserArray.length;

    if (totalPlayer < room.presetAndroidCount) {
        var count = room.presetAndroidCount - totalPlayer;
        room.addAndroid(count);
    } else if (totalPlayer > room.presetAndroidCount) {

        var count = totalPlayer - room.presetAndroidCount;
        var start = Math.floor(Math.random() * room.androidUserArray.length);
        //可能删除时 i会越步 但是由于定时检测所以可以越步
        for (var i = 0; i < room.androidUserArray.length; i++) {
            if (count == 0) break;
            var index = (i + start) % room.androidUserArray.length;
            var android = room.androidUserArray[index];
            if (!android) continue;
            if (android.status == UserStatusConst.US_FREE || android.status == UserStatusConst.US_READY || android.status == UserStatusConst.US_SIT) {
                room.onUserLeave(android);
                count--;
            }
        }
    }
};

/**
 * 陪打
 */
hp.androidAttend = function () {
    var room = this.room;
    //操作数
    var playerNum = Math.floor(room.androidUserArray.length / 2) - room.tableArray[0].getPlayerNum();

    if (playerNum <= 0)
        var opCount = Math.ceil(Math.random() * 2);
    else
        var opCount = playerNum;

    var aLen = room.androidUserArray.length;
    var startIndex = Math.floor(Math.random() * aLen);

    //分配
    for (var i = startIndex; i < aLen + startIndex; ++i) {
        var index = i % aLen;
        var android = room.androidUserArray[index];
        if (android == null || android.status != UserStatusConst.US_FREE) continue;


        for (var tableID = 0; tableID < room.tableArray.length; tableID++) {

            var table = room.tableArray[tableID];
            var chairID = table.getFreeChairID();
            if (chairID == null) continue;


            room.onUserSitdown(android, tableID, chairID);
            opCount--;
            break;
        }

        if (opCount <= 0) return;
    }
};


module.exports.GeneralRule = GeneralRule;
module.exports.AntiCheatingRule = AntiCheatingRule;
module.exports.HundredRule = HundredRule;
