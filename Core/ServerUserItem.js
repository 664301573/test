/**
 * Created by 黄二杰 on 2016/3/10.
 */

var ttutil = require("../Util.js");
var SqlPool = require("../SqlPool.js");
var UserStatusConst = require("../Define.js").UserStatusConst;
var LuckyMoneyConst = require("../Define.js").LuckyMoneyConst;
var FriendConst = require("../Define.js").FriendConst;
var verifyMng = require("../Verifycation/VerifycationManager");
var DuoBaoSystem = require("./DuoBaoSystem");
var GD = require("../GD.js");
var Config = require("../Config.js");

var winston = require("winston");
var mpinfo = winston.info;
var mpdebug = winston.debug;
var mperror = winston.error;
var mpwarn = winston.warn;

/**
 * 用户缓存字段类
 * @param userInfo
 * @constructor
 */
var UserInfo = function (userInfo) {

    this.account = userInfo["account"];
    this.userID = userInfo["userID"];
    this.gameID = userInfo["gameID"];
    this.nickname = userInfo["nickname"];
    this.phone = userInfo["phone"];
    this.gender = userInfo["gender"];
    this.faceID = userInfo["faceID"];
    this.score = userInfo["score"];
    this.memberOrder = userInfo["memberOrder"];
    this.totalRebate = userInfo["totalRebate"];
    this.unRecvRebate = userInfo["unRecvRebate"];
    this.password = userInfo["password"];
    this.twoPassword = userInfo["twoPassword"];
    this.twoPasswordType = userInfo["twoPasswordType"];
    this.enable = userInfo["enable"];
    this.isAndroid = userInfo["isAndroid"];
    this.Online = false;        //是否在线
};

/**
 * 类除了列出来的属性外， 还有一些数据库子段属性， 这些没有在声明中列出来， 但在运行时会按数据库成员子段赋值， 其中有一个易错点就是 该类有 gameID属性， 还有GameID属性， gameID属性表示该玩家在哪个游戏中， GameID则是数据库属性， 表示玩家的GameID, 跟数据库的含义是一致的
 * @constructor
 */
function ServerUserItem() {
    ////////////////////自定义成员////////////////////////

    this.socket = null;                 //socket
    this.moduleID = null;                 //游戏ID 该类有 gameID属性， 还有GameID属性， gameID属性表示该玩家在哪个游戏中， GameID则是数据库属性， 表示玩家的GameID, 跟数据库的含义是一致的
    this.roomID = null;                 //房间ID
    this.tableID = null;                //桌子号
    this.chairID = null;                //椅子号
    this.uuid = null;                   //唯一值

    this.status = UserStatusConst.US_FREE;                 //用户状态
    this.offlinePreStatus = null;                           //用户断线前的状态

    this.loginIP = null;				//登录IP
    this.loginISP = null;				//登录ISP
    this.loginTime = null;              //登录时间

    this.sitStatus = null;              //坐下辅助变量 过滤客户端坐下操作成功前客户端多次点击

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    this.channel = null;            //渠道
    this.version = null;            //版本号
    this.deviceID = null;           //设备号

    //断线定时器
    //
    this.offlineTimer = null;

    //坐下超时定时器
    this.sitdownTimer = null;

    //////////////////////////////////////////////////////////////////////////////////////
}

var p = ServerUserItem.prototype;


/**
 * 初始化
 */
p.init = function (dbData, socket, ip, isp, channel, version, deviceID) {

    this.setDBInfo(dbData);


    this.loginTime = new Date();
    this.loginIP = ip;
    this.loginISP = isp;

    this.channel = channel;
    this.version = version;
    this.deviceID = deviceID;

    this.originScore = dbData.score;
    this.originBankScore = dbData.bankScore;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    this.uuid = ttutil.getUUID();

};


/**
 * 当用户使用道具时.
 * @param data
 */
p.onUseItem = function (data) {
    var itemID = data.itemListID;

    // var self = this;
    // if (this.itemMap[itemID] > 0) {
    //
    //     //var hornNoticeMsg = {};
    //     data.nickname = self.nickname;
    //     //发小喇叭, 必须要在房间里
    //     if (itemID == PlazaItem.SHorn) {
    //         if (self.moduleID == null || self.roomID == null) {
    //             ttutil.psSendSocketData(self.socket, "LabaAnnouncement", {errMsg: "发小喇叭, 必须要选择房间"});
    //             return;
    //         }
    //     }
    //
    //     SqlPool.x_query('CALL GSP_System_RemoveItem(?,?);', [this.userID, data.itemListID], function (err, rows, fields) {
    //
    //         if (err) {
    //             return;
    //         }
    //
    //         if (rows && rows[0]) {
    //             if (itemID == PlazaItem.SHorn) {
    //                 if (self.moduleID != null && self.roomID != null) {
    //                     var room = GD.gameMap[self.moduleID].roomMap[self.roomID];
    //                     for (var i = 0, len = room.realUserArray.length; i < len; ++i) {
    //                         ttutil.psSendSocketData(room.realUserArray[i].socket, "HornNotice", data);
    //                     }
    //                     ttutil.psSendSocketData(room.socket, "ServiceNotice", {nickname: self.nickname, msg: data.message});
    //                     self.itemMap[itemID]--;
    //                     ttutil.psSendSocketData(self.socket, "LabaAnnouncement");
    //                 }
    //                 else {
    //                     ttutil.psSendSocketData(self.socket, "LabaAnnouncement", {errMsg: "发小喇叭, 必须要选择房间"});
    //                 }
    //             }
    //             else if (itemID == PlazaItem.BHorn) {
    //                 ttutil.psSendSocketData(GD.plazaServer.plazaSocket, "HornNotice", data);
    //                 ttutil.psSendSocketData(GD.gameServer.gameSocket, "ServiceNotice", {nickname: self.nickname, msg: data.message});
    //                 self.itemMap[itemID]--;
    //                 ttutil.psSendSocketData(self.socket, "LabaAnnouncement");
    //             }
    //             else {
    //                 ttutil.psSendSocketData(this.socket, "LabaAnnouncement", {errMsg: "道具类型错误"});
    //             }
    //         }
    //         else {
    //             ttutil.psSendSocketData(this.socket, "LabaAnnouncement", {errMsg: "道具不足"});
    //         }
    //     })
    //
    // }
    // else {
    //     ttutil.psSendSocketData(this.socket, "LabaAnnouncement", {errMsg: "道具不足"});
    // }
};


/**
 * 当用户的socket断开时
 */
p.onUserSocketDisconnect = function () {

    //先这样吧
    this.logout();
    return;


    var self = this;

    this.offlinePreStatus = this.status;
    //设置为断线状态
    this.status = UserStatusConst.US_OFFLINE;

    this.offlineTimer && clearTimeout(this.offlineTimer);

    this.offlineTimer = setTimeout(function () {
        self.logout();
    }, Config.OfflineReconnectionWaitTime);
    this.socket = null;
};

/**
 * 当被别人顶下
 * @param socket
 * @param ip
 * @param isp
 */
p.onKickOut = function (socket, ip, isp) {

    //离开所有的游戏， 以及房间
    this.leaveGame();

    //重连的socket 跟之前的不是同一条socket
    if (this.socket != socket) {

        mpinfo("玩家" + this.userID + "在【" + isp + "】ip【" + ip + "】登录");

        var msg = "您的账号已经在【" + isp + "】地点登录,ip【" + ip + "】,您已经被挤下线.";
        //重复登陆,强制之前登陆账号下线
        ttutil.psSendSocketData(this.socket, "KickOut", {msg: msg});

        //解除绑定
        this.socket.userID = null;
    }
    else {
        mpinfo("玩家" + this.userID + "同一条socket链接登录两次?");
    }


    if (this.loginID) {
        //用户登出记录注册
        SqlPool.x_query('CALL ps_user_logout(?);', [this.loginID], function (err, rows, fields) {
            if (err) {
                return;
            }
        });
    }


    this.socket = socket;
    this.socket.userID = this.userID;


    //把状态换成 断线前的 状态。。。（就是断线前是什么状态， 现在就是什么状态, )
    this.status = this.offlinePreStatus;
};

/**
 * 当用户socket重新连接时
 * @param socket
 */
p.onReconnection = function (socket, ip, isp) {
    this.offlineTimer && clearTimeout(this.offlineTimer);
    this.offlineTimer = null;

    if (this.socket) {
        mperror("此时 这socket应该为空");
        this.onKickOut(socket, ip, isp);
        return;
    }

    this.socket = socket;
    this.socket.userID = this.userID;


    //把状态换成 断线前的 状态。。。（就是断线前是什么状态， 现在就是什么状态, )
    this.status = this.offlinePreStatus;
};


/**
 * 校验二级密码
 * 如果二级密码对， 返回0
 * 如果自身没有设定级密码， -2
 * 其它返回-1
 * @param password
 * @param force
 * @returns {boolean}
 */
p.verifyTwoPassword = function (password) {


    if (!this.twoPassword) {
        return -2;
    }
    if (password == this.twoPassword) {
        return 0;
    }
    return -1;

};

/**
 * 被系统踢 并断开连接 强制踢了用户
 */
p.kickOutBySystem = function (msg) {
    //登出
    this.logout();
    //通知客户端踢人
    ttutil.psSendSocketData(this.socket, "KickOut", {msg: msg});
    //断开连接
    //this.socket.disconnect();
};

/**
 * 登出， 注销。真正的断开
 */
p.logout = function () {


    mpinfo("链接断开");

    if (this.loginID) {
        //用户登出记录注册
        SqlPool.x_query('CALL ps_user_logout(?);', [this.loginID], function (err, rows, fields) {
            if (err) {
                return;
            }
        });
    }


    this.leaveGame();
    DuoBaoSystem.removeUser(this);

    delete GD.realUserMap[this.userID];


    this.socket.userID = null;

    //清理这个用户相关的验证码
    verifyMng.clearVerifyItem(this.userID);

    //this.socket.activeDisConnect = true;
    //this.socket.disconnect();
    //断开socket-----------------
    mpinfo("userID-" + this.userID + "-离开整个游戏");


};

/**
 * 离开游戏处理
 */
p.leaveGame = function () {

    if (this.moduleID != null) {

        var game = GD.gameMap[this.moduleID];

        if (game) {
            game.onUserLeave && game.onUserLeave(this);
        }
        else {
            winston.error("-----------------------------------");
            winston.error(this.moduleID);
            winston.error("-----------------------------------");
        }

    }

    //清理定时器
    this.clearSitdownTimer();
};

/**
 * 发给子游戏用的 userInfo
 */
p.getUserInfoPrivate = function () {
    var info = {};
    info.faceID = this.faceID;
    info.gameID = this.gameID;
    info.gender = this.gender;
    info.isAndroid = this.isAndroid;
    info.memberOrder = this.memberOrder;
    info.nickname = this.nickname;
    info.score = this.score;
    info.userID = this.userID;
    info.userLevel = this.userLevel;
    info.weight = this.weight;
    info.vipTiren = this.vipTiren;
    info.tableID = this.tableID;
    info.chairID = this.chairID;
    info.status = this.status;
    info.privilege = this.privilege;                //用户特权

    return info;
};

/**
 * 得到用户信息
 * @returns {{}}
 */
p.getUserInfo = function () {

    var info = {};
    info.faceID = this.faceID;
    info.gameID = this.gameID;
    info.GamesRoomID = this.roomID;
    info.gender = this.gender;
    info.ip = this.loginIP;
    info.memberOrder = this.memberOrder;
    info.nickname = this.nickname;
    info.score = this.score;
    info.underWrite = this.underWrite;
    info.userID = this.userID;
    info.userLevel = this.userLevel;
    info.tableID = this.tableID;
    info.chairID = this.chairID;
    info.status = this.status;


    return info;
};

/**
 * 更新用户属性， 以保持跟数据库一致
 * @param info 需要更新的子段
 * @param noUpdateCache false表示要更新到缓存中去， true表示不更新
 */
p.setDBInfo = function (info, noUpdateCache) {
    ttutil.copyAttr(this, info);
    //更新到缓存中去
    !noUpdateCache && GD.mainServer.updateCacheUserInfoMap(this.userID, this);
};


/**
 * 获取用户ID， 先用大写UserID,
 * @returns {*}
 */
p.getUserID = function () {
    return this.userID;
};

/**
 * 清除坐下定时器
 */
p.clearSitdownTimer = function () {
    if (this.sitdownTimer) clearTimeout(this.sitdownTimer);
    this.sitdownTimer = null;
};

module.exports.ServerUserItem = ServerUserItem;
module.exports.UserInfo = UserInfo;