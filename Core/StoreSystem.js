/**
 * Created by grape on 2017/9/29.
 * 摆摊系统
 */

var GD = require('../GD');
var SqlPool = require('../SqlPool');
var winston = require("winston");

/**
 * 道具
 * @constructor
 */
function Goods() {
    this.id = null;
    this.goodsID = null;           //货物ID
    this.name = null;              //名字
    this.count = 0;                //数量
    this.payType = null;           //支付类型
    this.price = null;             //价格
}

/**
 * 摊位内容
 * @constructor
 */
function StoreSet(storeSystem, userID) {
    this.storeSystem = storeSystem;
    this.userID = userID;
    this.storeMap = {};
}

var gs = StoreSet.prototype;

/**
 * 查找道具，摊位ID
 * @param id
 */
gs.findGoods = function (id) {
    return this.storeMap[id];
};

/**
 * 添加物品
 * @param goodsID
 * @param count
 * @param price
 */
gs.addGoods = function (data) {
    var goodsConfig = GD.goodsMap[data.goodsID];

    var goods = new Goods();
    goods.id = data.id;
    goods.userID = data.userID;
    goods.goodsID = data.goodsID;
    goods.name = goodsConfig.name;
    goods.price = data.price;
    goods.count = data.count;
    goods.payType = data.payType;
    goods.nickname = data.nickname;
    goods.faceID = data.faceID;
    this.storeMap[data.id] = goods;

    this.storeSystem.getGoodsStoreSet(data.goodsID)[data.id] = goods;
};
/**
 * 下架物品
 * @param id
 */
gs.decGoods = function (id) {
    var storeInfo = this.storeMap[id];
    if (storeInfo == null)
        return;

    delete this.storeMap[id];

    delete this.storeSystem.getGoodsStoreSet(storeInfo.goodsID)[id];
};

gs.getStoreMap = function () {
    return this.storeMap;
};

/**
 * 摊位系统
 * @constructor
 */
function StoreSystem() {
    this.userStoreSet = {}; //用户摊位集合
    this.goodsStoreSet = {};//物品摊位集合
}

var p = StoreSystem.prototype;

/**
 * 需要放在加载玩道具配置表后加载
 */
p.loadUserStore = function () {
    SqlPool.x_query('call sa_ranking_store(?, ?)', [0, 50000], (err, results) => {
        if (err) {
            winston.error("加载用户摊位缓存错误" + err);
            return;
        }

        for (var k = 0; k < results.length - 1; ++k) {
            var result = results[k];

            if (result.length < 1)
                return;

            for (var i = 0; i < result.length; ++i) {
                var goodsSet = this.getUserStoreSet(result[i].userID);

                goodsSet.addGoods(result[i]);
            }
        }

        winston.info("用户摊位缓存成功");
    });
};
//
/**
 * 获取用户摊位 没有则创建
 */
p.getUserStoreSet = function (userID) {
    if (this.userStoreSet[userID] == null) {
        this.userStoreSet[userID] = new StoreSet(this, userID);
    }

    return this.userStoreSet[userID];
};

/**
 * 获取物品摊位
 */
p.getGoodsStoreSet = function (goodsID) {
    if (this.goodsStoreSet[goodsID] == null) {
        this.goodsStoreSet[goodsID] = {};
    }

    return this.goodsStoreSet[goodsID];
};

/**
 * 获取物品摊位数
 */
p.getGoods = function (userID) {

    var goods = [];

    for (var key in this.goodsStoreSet) {
        var data = this.goodsStoreSet[key];

        var count = 0;
        var userCount = 0;

        for (var i in data) {
            count++;

            if (data[i].userID == userID)
                userCount++;
        }

        goods.push({goodsID: key, count: count, userCount: userCount});
    }

    return goods;
};

module.exports = StoreSystem;