/**
 * Created by orange on 2016/10/9.
 * 背包道具系统
 */

var GD = require('../GD');
var SqlPool = require('../SqlPool');
var winston = require("winston");

/**
 * 道具
 * @constructor
 */
function Goods() {
    this.goodsID = null;
    this.name = null;
    this.count = 0;
}

/**
 * 道具集合 背包
 * @constructor
 */
function GoodsSet(userID) {
    this.userID = userID;
    this.goodsArray = [];
}

var gs = GoodsSet.prototype;

/**
 * 查找道具
 * @param goodsID
 */
gs.findGoods = function (goodsID) {
    if (goodsID == null) return null;
    for (var i = 0; i < this.goodsArray.length; ++i) {
        if (this.goodsArray[i] && this.goodsArray[i].goodsID == goodsID) {
            return this.goodsArray[i];
        }
    }

    return null;
};

/**
 * 使用道具
 * @param goodsID 道具ID
 * @param cb 回调通知函数
 */
gs.useGoods = function (goodsID, cb) {
    var goods = this.findGoods(goodsID);
    if (goods == null) {
        cb && cb("您没有此道具不能使用");
        return;
    }

    SqlPool.x_query("call ps_goods_use(?, ?);", [this.userID, goodsID], (err, result) => {
        if (err) {
            cb && cb("使用道具失败");
            return;
        }

        if (result[0] && result[0][0]) {

            if (result[0][0].msgID == 1) {
                goods.count -= 1;
                cb(null);
            } else {
                cb("您没有此道具不能使用");
            }

        } else {
            cb && cb("使用道具失败");
        }
    });
};

/**
 * 获取道具数
 * @param goodsID
 * @returns {*}
 */
gs.getGoodsCount = function (goodsID) {
    var goods = this.findGoods(goodsID);
    if (goods == null) return 0;
    return goods.count;
};

/**
 * 添加新道具 有则更新道具 无则添加新道具
 */
gs.updateGoods = function (goodsID, count) {
    var goodsConfig = GD.goodsMap[goodsID];
    var goods = this.findGoods(goodsID);
    if (goods == null) {
        goods = new Goods();
        goods.goodsID = goodsConfig.id;
        goods.name = goodsConfig.name;
        this.goodsArray.push(goods);
    }

    goods.count += count;
};

gs.getGoodsArray = function () {
    return this.goodsArray;
};

/**
 * 道具系统 背包
 * @constructor
 */
function GoodsSystem() {
    this.userGoodsSet = {}; //背包集合
}

var p = GoodsSystem.prototype;

/**
 * 需要放在加载玩道具配置表后加载
 */
p.loadUserGoods = function () {
    SqlPool.x_query('select * from ps_user_goods', (err, results) => {
        if (err) {
            winston.error("加载用户道具缓存错误" + err);
            return;
        }

        if (results[0]) {
            for (var i = 0; i < results.length; ++i) {
                var goodsSet = this.getUserGoodsSet(results[i].userID);
                goodsSet.updateGoods(results[i].goodsID, results[i].count);
            }
        }

        winston.info("用户道具缓存成功");
    });
};

/**
 * 获取用户背包 没有则创建
 */
p.getUserGoodsSet = function (userID) {
    if (this.userGoodsSet[userID] == null) {
        this.userGoodsSet[userID] = new GoodsSet(userID);
    }

    return this.userGoodsSet[userID];
};

/**
 * 更新用户道具
 * @param userID
 * @param goodsID
 * @param count
 */
p.updateUserGoods = function (userID, goodsID, count) {
    var goodsSet = this.getUserGoodsSet(userID);
    goodsSet.updateGoods(goodsID, count);
};

/**
 * 获取某一道具的数目
 * @param userID
 * @param goodsID
 */
p.getUserGoodsCount = function (userID, goodsID) {
    var goodsSet = this.getUserGoodsSet(userID);
    return goodsSet.getGoodsCount(goodsID);
};


module.exports = GoodsSystem;