/**
 * Created by 黄二杰 on 2016/3/10.
 */

var Table = require('./Table.js');
var ttutil = require('../Util.js');
var SqlPool = require('../SqlPool.js');

var UserStatusConst = require('../Define.js').UserStatusConst;
var RoomConst = require('../Define.js').RoomConst;
var SitdownResultCode = require("../Define.js").SitdownResultCode;
var TableLockStatus = require("../Define.js").TableLockStatus;
var DistributeRule = require("./DistributeRule.js");
var GD = require("../GD.js");
var GoodsConst = require('../Define').GoodsConst;

var winston = require("winston");

var mpinfo = winston.info;
var mpdebug = winston.debug;
var mperror = winston.error;
var mpwarn = winston.warn;

/*
 * 游戏房间
 * @constructor
 */
var Room = function (socket, androidMng) {

    this.moduleID = null;                                         //游戏id
    this.roomID = null;                                         //房间ID
    this.roomName = null;                                       //房间名称
    this.sortID = null;                                         //游戏房间排序ID
    this.enterScore = null;                                     //进入分数
    this.androidScoreBase = null;                              //机器人进入分数基准
    this.port = null;                                           //端口
    this.roomMode = null;                                       //房间模式

    this.socket = socket;                                         //房间socket
    this.tableCount = null;                                     //桌子数量
    this.chairCount = null;                                     //椅子数
    this.presetAndroidCount = null;                             //预定机器人数量
    this.cheatMode = null;                                     //是否是防作弊模式

    this.oldRoomInfo = null;                                    //老的数据结构

    //桌子列表
    this.tableArray = [];

    this.androidMng = androidMng;

    //真实玩家列表, 紧凑， 不允许空, 通过length可以获取该房间的真实玩家人数
    this.realUserArray = [];

    //机器人玩家列表, 紧凑， 不允许空, 通过length可以获取该房间的真实玩家人数
    this.androidUserArray = [];

    this.androidPolicy = null;

    //房卡
    this.roomCard = null;
};


var p = Room.prototype;


/**
 * 初始化
 * @param data
 */
p.init = function (data, oldData) {

    this.setInfo(data);
    this.oldRoomInfo = oldData;

    for (var i = 0; i < this.tableCount; ++i) {
        var table = new Table(this, i, this.chairCount);
        this.tableArray.push(table);
    }

    this.createAndroidPolicy();
};

/**
 * 关闭房间
 */
p.stopRoom = function () {
    this.androidPolicy && this.androidPolicy.stopDistribute();

    var len = this.realUserArray.length;
    while (len > 0) {
        var user = this.realUserArray[0];
        this.onUserLeave(user, true);
        len--;
    }
};

/**
 * 发送游戏服务器消息
 */
p.sendGameServerData = function (eventName, data) {
    mpdebug("sendGameServerData");
    ttutil.psSendSocketData(this.socket, eventName, data);
};

/**
 * 发送房间数据
 */
p.sendRoomUserData = function (eventName, data, userID) {
    mpdebug("sendRoomUserData");
    if (userID == null) {
        for (var i = 0; i < this.realUserArray.length; ++i) {
            if (this.realUserArray[i].socket == null) continue;
            ttutil.psSendSocketData(this.realUserArray[i].socket, eventName, data);
        }
    } else {
        var user = this.getRoomUser(userID);
        if (!user || !user.socket) return;
        ttutil.psSendSocketData(user.socket, eventName, data);
    }
};

/**
 * 进房间调整机器人分数
 * @param user
 */
p.adjustAndroidScore = function (user) {

    if (this.roomMode > RoomConst.MD_FULL_READY)
        user.score = this.androidScoreBase + Math.floor(Math.random() * 300 * (this.androidScoreBase ? this.androidScoreBase : 10000));
    else
        user.score = this.androidScoreBase + Math.floor(Math.random() * 30 * (this.androidScoreBase ? this.androidScoreBase : 10000));

    if (user.score < this.enterScore) {
        user.score = this.enterScore * Math.floor((Math.random() * 10) + 1);
    }

    user.setDBInfo({score: user.score});
};

/**
 *  创建机器人分配策略
 */
p.createAndroidPolicy = function () {

    if (this.roomMode == RoomConst.MD_HUNDRED) {
        this.androidPolicy = new DistributeRule.HundredRule(this);
    } else if (this.cheatMode) {
        this.androidPolicy = new DistributeRule.AntiCheatingRule(this);
    } else {
        this.androidPolicy = new DistributeRule.GeneralRule(this);
    }

    this.androidPolicy.startDistribute();
};


/**
 * 添加机器人人
 */
p.addAndroid = function (count) {
    for (var i = 0; i < count; ++i) {
        var android = this.androidMng.getFreeAndroid();
        if (android == null) continue;
        android.moduleID = this.moduleID;
        android.roomID = this.roomID;
        this.adjustAndroidScore(android);
        this.onUserEnter(android, android);
    }
};

/**
 * 获取用户
 * @param userID
 */
p.getRoomUser = function (userID) {
    for (var i = 0; i < this.realUserArray.length; ++i) {
        if (this.realUserArray[i] && this.realUserArray[i].userID == userID) {
            return this.realUserArray[i];
        }
    }

    for (var i = 0; i < this.androidUserArray.length; ++i) {
        if (this.androidUserArray[i].userID == userID) {
            return this.androidUserArray[i];
        }
    }

    return null;
};

/**
 * 获取房间用户数
 */
p.getRoomUserNumber = function () {
    return this.androidUserArray.length + this.realUserArray.length;
};

/**
 * 用户写分
 * @param data
 */
p.onWriteScore = function (data) {
    if (data == null) return;
    // console.log("用户写分 onWriteScore", data);

    var tableID = data.tableID == null ? -1 : data.tableID;
    var chairID = data.chairID == null ? -1 : data.chairID;
    var score = data.score == null ? 0 : data.score;
    var tax = data.tax == null ? 0 : data.tax;
    var gameDesc = data.gameDesc == null ? null : data.gameDesc;

    var userInfo = GD.cacheUserInfoMap[data.userID];

    // 非机器人， 写分写记录
    if (!userInfo.isAndroid) {

        //直接空掉吧
        if (!GD.isWriteGameDetailLog) {
            gameDesc = null;
        }

        SqlPool.x_query("call ps_user_write_score_and_game_log(?,?,?,?,?,?,?,?)", [data.userID, score, tax, this.moduleID, this.roomID, tableID, chairID, gameDesc], (err, rows) => {
            if (err) {
                return;
            }
            if (rows[0][0].res != 0) {
                //执行失败
                mperror("写分失败", this.roomID, data);
            }
        });
    }

    var user = GD.realUserMap[data.userID] || this.getRoomUser(data.userID);

    if (!user) {
        mpinfo("更新内存里的分数找不到用户, 可能已经退出游戏", "userID:", data.userID, "roomID:", this.roomID);
        return;
    }
    user.score += score;
    if (user.score < 0) {
        user.score = 0;
        mperror("写分分数为负 userID", data.userID, data);
    }
    user.setDBInfo({score: user.score, revenue: user.revenue + tax});

    this.sendGameServerData("WriteScore", {
        userID: user.userID,
        score: user.score
    });

    if (!this.cheatMode) {
        this.sendRoomUserData("RoomUserInfoUpdate", {score: user.score, userID: data.userID, roomID: this.roomID});
    }


    if (user.isAndroid) {
        var num = Math.ceil(Math.random() * 20);
        if (user.score <= this.enterScore || num <= 1) {
            this.sendGameServerData("UserLeave", {userID: user.userID});
            this.onUserLeave(user);
        }
    }
    else {
        ttutil.psSendSocketData(user.socket, "UserInfoUpdate", {score: user.score, revenue: user.revenue});
    }
};

/**
 * 多人游戏局数统计
 */
p.onRoundCount = function (data) {
    if (data == null) return;

    SqlPool.x_query("call ps_game_write_record(?,?,?,?)", [this.moduleID, this.roomID, data.tableID, data.gameDesc], (err) => {
        if (err) {
            mpinfo("更新多人游戏局数时失败", "moduleID:", this.moduleID);
            return;
        }
    });
};

/**
 * 用户状态改变
 * 确认逻辑有点奇葩
 * @param data
 */
p.onUserStatus = function (data) {
    var userID = data.userID;
    var user = this.getRoomUser(userID);

    if (user == null) return;

    switch (data.status) {
        case UserStatusConst.US_FREE:
            break;

        case UserStatusConst.US_SIT:
            user.status = UserStatusConst.US_SIT;
            user.clearSitdownTimer();
            var table = this.tableArray[user.tableID];
            if (table) table.unLockChair(user.chairID);
            user.sitStatus = UserStatusConst.SIT_SUCCESS;

            var info = user.getUserInfo();
            if (!this.cheatMode) this.sendRoomUserData("UsersGameStatus", info);
            else this.sendRoomUserData("UsersGameStatus", info, user.userID);
            break;

        case UserStatusConst.US_READY:
            user.status = UserStatusConst.US_READY;
            var info = user.getUserInfo();
            if (!this.cheatMode) this.sendRoomUserData("UsersGameStatus", info);
            else this.sendRoomUserData("UsersGameStatus", info, user.userID);
            break;

        case UserStatusConst.US_PLAYING:
            user.status = UserStatusConst.US_PLAYING;
            var info = user.getUserInfo();
            if (!this.cheatMode) this.sendRoomUserData("UsersGameStatus", info);
            else this.sendRoomUserData("UsersGameStatus", info, user.userID);
            break;

        case UserStatusConst.US_NULL:
            //如果是换桌起立
            this.onUserStandup(user, true);
            break;
    }
};

/**
 * 当用户离开调用
 * @param user
 * @param isRoomClose 是否是房间关闭
 * @param isGame2Plaza 是否是子游戏通知大厅的  是的话就不再向子游戏发送userLeave消息了
 */
p.onUserLeave = function (user, isRoomClose, isGame2Plaza) {
    if (user == null) return;
    if (user.tableID != null && user.chairID != null) {
        this.tableArray[user.tableID].onUserLeave(user, isRoomClose);

        //房间都已经关了， 就不需要再通知房间了， 也通知不到了, 或者这消息本来就是子游戏发过来的
        if (!isRoomClose && !isGame2Plaza) {
            this.sendGameServerData("UserLeave", {userID: user.userID});
        }

    }

    if (!isRoomClose && !this.cheatMode) {
        //通知房间里的玩家
        for (var i = 0, len = this.realUserArray.length; i < len; ++i) {
            //通知客户端玩家列表更新玩家离开
            if (this.realUserArray[i]) {
                ttutil.psSendSocketData(this.realUserArray[i].socket, "UserOnlineList", {
                    userID: user.userID,
                    Action: "Out"
                });
            }
        }
    }


    if (!user.isAndroid) {
        ttutil.arrayRemove(this.realUserArray, user);
    }
    else {
        ttutil.arrayRemove(this.androidUserArray, user);
        //回收机器人
        this.androidMng.pushFreeAndroid(user);
    }

    user.roomID = null;
    mpdebug("userID-" + user.userID + "-Name-" + user.nickname + "-离开游戏房间");
};

/**
 * 更新簿记索引
 */
p.onUpdateDetailIndex = function (tableID, index) {
    var table = this.tableArray[tableID];
    table && (table.detailIndex = index);
};

/**
 * 当用户进来时调用
 * data是暂时的， 等后面跟客户端  整理了参数再搞
 * @param user
 */
p.onUserEnter = function (user, data) {

    if (user.score < this.enterScore) {
        var errMsg = "进入此游戏房间需要 " + this.enterScore + CURRENCY + "！";
        ttutil.psSendSocketData(user.socket, "UserRoomIn", {errMsg: errMsg});

        //回收机器人
        if (user.isAndroid) {
            this.androidMng.pushFreeAndroid(user);
        }

        return;
    }

    //提前标志，
    user.roomID = this.roomID;
    if (user.isAndroid) {
        this.androidUserArray.push(user);
    }
    else {
        for (var i = 0; i < this.realUserArray.length; ++i) {
            if (!this.realUserArray[i] || this.realUserArray[i].userID == user.userID) {
                this.realUserArray.splice(i, 1);
                --i;
            }
        }

        this.realUserArray.push(user);
    }

    if (!user.isAndroid) {
        ttutil.psSendSocketData(user.socket, "UserRoomIn");
    }

    if (!this.cheatMode) {
        var info = user.getUserInfo();

        info.Action = "In";
        for (var i = 0; i < this.realUserArray.length; ++i) {
            if (this.realUserArray[i] && this.realUserArray[i].roomID == this.roomID) {
                ttutil.psSendSocketData(this.realUserArray[i].socket, "UserOnlineList", info);//通知客户端玩家列表更新玩家进入
            }
        }
    }


};


/**
 * 寻找可以快速加入的 桌子号跟椅子号
 *
 * {tableID:x, chairID:x}
 * 没有符合的条件， 返回null
 */
p.findQuicklyJoinTableAndChairID = function () {

    var canJoin = [];
    for (var i = 0; i < this.tableArray.length; ++i) {
        var table = this.tableArray[i];
        //和原来桌子一样那就继续咯
        if (table == null) continue;
        //有设置密码跳出 房卡功能
        if (table.status != TableLockStatus.unlock) continue;

        var chairArray = table.chairArray;
        for (var j = 0; j < chairArray.length; ++j) {
            var chair = chairArray[j];

            if (!chair || chair.lockForUserID != null || chair.user) continue;
            //加入候选名单
            canJoin.push({tableID: i, chairID: j});
            var num = Math.ceil(Math.random() * 20);
            //需求是  越前面的 概率越高
            if (num >= 18) {
                return {tableID: i, chairID: j};
            }
        }
    }

    if (canJoin.length > 0) {
        return canJoin[Math.floor(Math.random() * canJoin.length)];
    }
    //如果一个可加入的条件都没有
    return null;
};

/**
 * 获取用户信息
 * @param userID
 */
p.onUserInfo = function (userID) {
    var errCode = 0, info = null;
    var user = this.getRoomUser(userID);

    if (user == null) {
        errCode = 1;
    }
    else if (user.tableID == null) {
        errCode = 2;
    }
    else if (user.chairID == null) {
        errCode = 3;
    }
    else {
        info = user.getUserInfoPrivate();
    }

    return {info: info, errCode: errCode};
};

/**
 * 玩家坐下超时
 */
p.onUserSitdownTimeOut = function (user) {
    //只有等着游戏服务器状态才进行超时处理
    if (user.sitStatus != UserStatusConst.SIT_READY) return;
    user.sitdownTimer = null;
    user.sitStatus = null;


    this.onUserStandup(user);
    mpdebug("登录超时");
};


/**
 * 用户起立
 * @param user
 * @param isGame2Plaza 是否是子游戏通知大厅的  是的话就不再向子游戏发送userLeave消息了
 */
p.onUserStandup = function (user, isGame2Plaza) {
    if (user == null) return;
    //清理定时器
    user.clearSitdownTimer();

    //防作弊房或者机器人
    if (this.cheatMode || user.isAndroid) {
        this.onUserLeave(user, false, isGame2Plaza);         //防作弊房，直接离开房间
    } else if (user.tableID != null && user.chairID != null) {
        this.tableArray[user.tableID].onUserLeave(user, false, isGame2Plaza);
    }

    //防止其出错 这边再清理吧
    user.tableID = null;
    user.chairID = null;
};

// //获取桌子状态
// p.onTableLockStatus = function (user, tableID) {
//     var table = this.tableArray[tableID];
//
//     ttutil.psSendSocketData(user.socket, "TableLockStatus", {
//             tableID: table.tableID,
//             status: table.status
//         });
// };


// p.onLockTable = function (user, tableID, action) {
//
//     if (user == null || tableID == null || tableID >= this.tableCount || !this.tableArray[tableID]) {
//         ttutil.psSendSocketData(user.socket, "LockTable", {errMsg: "参数错误"});
//         return;
//     }
//
//     var table = this.tableArray[tableID];
//
//     switch (table.status){
//         case
// TableStatus.unlock:
//             table.status = TableStatus.readyLock;
//             ttutil.psSendSocketData(user.socket, "LockTable", {errMsg: "请稍候"});
//             break;
//         case TableStatus.lock:
//             ttutil.psSendSocketData(user.socket, "LockTable", {errMsg: "解锁成功"});
//             break;
//         case TableStatus.readyLock:
//             ttutil.psSendSocketData(user.socket, "LockTable", {errMsg: "加锁成功"});
//             break;
//         default:
//             ttutil.psSendSocketData(user.socket, "LockTable", {errMsg: "桌子出错！"});
//     }
//     this.sendRoomUserData("TableStatus", {
//             tableID: table.tableID,
//             status: table.status
//         });
// };

/**
 * 刷新桌子状态
 */
p.onTableStatus = function (tableID) {
    //非房卡不刷新
    if (!this.roomCard) return;
    var table = this.tableArray[tableID];

    this.sendRoomUserData("TableStatus", {
        tableID: table.tableID,
        status: table.status
    });
};

/**
 * 处理用户坐下，  有可能是坐下， 也有可能是换桌， 也有可能是旁观，先按着他原来逻辑写 后期再改动
 * @param user
 * @param tableID
 * @param chairID
 * @param agentID  中转服ID
 * @param enterPwd 如果桌子需要密码， 则这是用户提供的密码
 * @param setPwd 设置桌子的密码
 */
p.onUserSitdown = function (user, tableID, chairID, agentID, enterPwd, setPwd, userID, uuid) {

    var self = this;
    if (user == null || tableID == null || tableID >= this.tableCount || chairID == null || chairID >= this.chairCount || !this.tableArray[tableID]) {
        ttutil.psSendSocketData(user.socket, "UserSitdown", {errMsg: "参数错误"});
        return;
    }

    if (user.score < this.enterScore) {
        var errMsg = "此游戏需要 " + this.enterScore + CURRENCY + "！";
        ttutil.psSendSocketData(user.socket, "UserSitdown", {errMsg: errMsg});

        //机器人退出房间
        if (user.isAndroid) {
            this.onUserLeave(user);
        }

        return;
    }

    //判断是否正在准备接受游戏服务器状态中,是的话直接返回
    if (user.sitStatus == UserStatusConst.SIT_READY) {
        ttutil.psSendSocketData(user.socket, "UserSitdown", {errMsg: "请稍候"});
        return;
    }

    //判断椅子是否被锁定
    var chair = this.tableArray[tableID].getChair(chairID);
    if (!chair || chair.lockForUserID != null || chair.user) {
        ttutil.psSendSocketData(user.socket, "UserSitdown", {errMsg: "请稍候"});
        return;
    }


    if (user.tableID != null || user.chairID != null) {
        ttutil.psSendSocketData(user.socket, "UserSitdown", {errMsg: "请稍候,或重启游戏重试"});
        return;
    }

    //是玩游戏或者要旁观
    var table = this.tableArray[tableID];

    //坐下成功调用的方法
    var sitDownFn = (user, table) => {
        var msg = {};
        msg.serverIP = "subgame." + DOMAIN;
        msg.tableID = table.tableID;

        ttutil.psSendSocketData(user.socket, 'UserSitdown', msg);

        //设置等带游戏服务器状态
        user.sitStatus = UserStatusConst.SIT_READY;
        //锁住椅子
        // if (sitdownResultCode == SitdownResultCode.SUCCESS_PLAYING) {
        table.lockChair(chairID, user.userID);
        // }

        //有先清空
        if (user.sitdownTimer) {
            clearTimeout(user.sitdownTimer);
        }
        // 启动超时 踢出
        user.sitdownTimer = setTimeout(this.onUserSitdownTimeOut.bind(this, user), 30000);

        //是机器人的话直接发送
        if (user.isAndroid) {
            var info = user.getUserInfoPrivate();
            this.sendGameServerData("UserSitDown", info);
        }

        //非机器人的时候生成唯一标识
        if (!user.isAndroid && table.getRealPlayerNum() == 1) {
            table.uuid = ttutil.getUUID();
        }
    };

    //加密坐下
    if (this.roomCard && table.status == TableLockStatus.unlock && setPwd) {
        var goodsSet = GD.mainServer.goodsSystem.getUserGoodsSet(userID);
        var chair = table.chairArray[chairID];
        var count = goodsSet.getGoodsCount(GoodsConst.RoomCard);
        if (count <= 0) {
            this.onSitdownTableFail(user, SitdownResultCode.LOCK_ERROR, "道具不足无法加密!");
        }
        else if (table.getPlayerNum() > 0) {
            this.onSitdownTableFail(user, SitdownResultCode.LOCK_ERROR, "该房间已经有人!");
        } else {
            goodsSet.useGoods(GoodsConst.RoomCard, (err) => {
                if (err == null && chair.user == null) {
                    var count = goodsSet.getGoodsCount(GoodsConst.RoomCard);

                    ttutil.psSendSocketData(user.socket, "UpdateGoods", {goodsID: GoodsConst.RoomCard, count: count});
                    // 椅子和用户绑定
                    chair.user = user;
                    user.tableID = table.tableID;
                    user.chairID = chairID;
                    table.landlord = user;
                    //桌子加锁
                    table.tablePwd = setPwd;
                    table.status = TableLockStatus.lock;
                    sitDownFn(user, table);
                    //刷新桌子 状态
                    this.onTableStatus(tableID);
                    return;
                }
                this.onSitdownTableFail(user, SitdownResultCode.LOCK_ERROR, "道具使用错误!");
            })
        }
        return false;
    }

    //正常坐下
    var sitdownResultCode = table.onUserSitdown(user, chairID, agentID, enterPwd, userID, uuid);
    //返回 0表示坐下失败, 1表示 玩，
    if (sitdownResultCode == SitdownResultCode.SUCCESS_PLAYING) {
        sitDownFn(user, table);
        return true;
    }
    else if (sitdownResultCode == SitdownResultCode.PWD_REQUIRED) {
        var tableInfo = {
            roomID: this.roomID,
            tableID: tableID,
            chairID: chairID
        };
        ttutil.psSendSocketData(user.socket, 'UserSitdown', {
            failCode: sitdownResultCode,
            tableInfo: tableInfo,
            errMsg: "进入该房间需要密码！"
        });
    }
    else {
        return this.onSitdownTableFail(user, sitdownResultCode);
    }

    return false;
};


/**
 * 处理用户坐下桌子失败后的。
 * @param user
 * @param failCode
 */
p.onSitdownTableFail = function (user, failCode, errMsg) {
    if (failCode == SitdownResultCode.FAIL_SYS) {
        ttutil.psSendSocketData(user.socket, 'UserSitdown', {
            failCode: failCode,
            errMsg: errMsg ? errMsg : "与游戏房间连接超时,请稍候再试"
        });
    } else if (failCode == SitdownResultCode.PWD_ERROR) {
        ttutil.psSendSocketData(user.socket, 'UserSitdown', {failCode: failCode, errMsg: "密码错误！"});
    } else if (failCode == SitdownResultCode.LOCK_ERROR) {
        ttutil.psSendSocketData(user.socket, 'UserSitdown', {failCode: failCode, errMsg: errMsg ? errMsg : "加密错误！"});
    } else if (failCode == SitdownResultCode.ROOMINVITATION_FAILL) {
        ttutil.psSendSocketData(user.socket, 'UserSitdown', {failCode: failCode, errMsg: "邀请链接已经失效！"});
    }
    return false;
};

p.setInfo = function (data) {
    this.moduleID = data.moduleID;
    this.roomID = data.roomID;
    this.roomName = data.roomName;
    this.roomMode = data.roomMode;
    this.enterScore = data.enterScore;
    this.chairCount = data.chairCount;
    this.presetAndroidCount = data.presetAndroidCount;
    this.sortID = data.sortID;
    this.port = data.port;
    this.cheatMode = data.cheatMode;
    this.tableCount = data.tableCount;
    this.androidScoreBase = data.androidScoreBase;
    //防作弊模式关闭房卡
    this.roomCard = this.cheatMode == 1 ? 0 : data.roomCard;
};

module.exports = Room;

