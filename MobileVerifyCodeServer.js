/**
 * Created by 黄二杰 on 2016/5/6.
 * 手机验证码
 */



var http = require('http');
var querystring = require('querystring');
var parseString = require('xml2js').parseString;
var winston = require("winston");
var scheduler = require('node-schedule');


/**
 * 验证码结构体
 * @param code
 * @param mobileNum
 * @constructor
 */
var CodeObj = function (code, mobileNum) {
    this.code = code;
    this.mobileNum = mobileNum;
    this.count = 0;         //验证次数， 超过三次就销毁掉
    this.timestamp = Date.now();
};

/**
 * 手机验证码 发送请求
 * @constructor
 */
var MobileVerifyCodeServer = function () {

    this.mobileNum2LastTime = {};

    //socketID对应的验证码, 及手机号码， 及 对应的时间戳, 方便定时清空
    //{code:xxxx, mobileNum:xxx, timestamp:xxx}
    this.socketID2CodeObjMap = {};

    //5分钟清一次
    this.timerInterval = 1000 * 60 * 5;

    //定时器ID定时清理验证码， 每5分钟触发一次， 清理前5分钟的验证码。
    this.clearCodeMapTimer = null;
    /**
     * 开启定期清理操作
     */
    this._startIntervalClearCode();
};


var p = MobileVerifyCodeServer.prototype;

/**
 * 获取codeObj,
 * @param socketID
 * @returns {*}
 */
p.getCodeObj = function (socketID) {
    return this.socketID2CodeObjMap[socketID];
};

/**
 * 删除codeObj
 * @param socketID
 */
p.deleteCodeObj = function (socketID) {
    delete this.socketID2CodeObjMap[socketID];
};

p.sendCode = function (socketID, mobileNum, callback) {

    //加个防御， 同一个手机号， 30秒只能发一次
    ///////////////////////////////////////////////////////////////////////////////////////
    if (this.mobileNum2LastTime[mobileNum]) {
        if (Date.now() - this.mobileNum2LastTime[mobileNum] < 30000) {
            callback(false);
            return;
        }
        else {
            this.mobileNum2LastTime[mobileNum] = Date.now();
        }
    }
    else {
        this.mobileNum2LastTime[mobileNum] = Date.now();
    }
    ///////////////////////////////////////////////////////////////////////////////////////
    var code = this._generateCode();

    this._sendCode(mobileNum, code, function (err, res) {

        if (err) {
            winston.error(err);
            callback(false);
            return;
        }
        if (res && res.error_code == 0) {
            callback(true);
        }
        else {
            callback(false);
            winston.error(new Date().toLocaleString(), res);
        }
    });
    this.socketID2CodeObjMap[socketID] = new CodeObj(code, mobileNum);


};

//////////////////////////////////////////////////////////////////
//以下非公开

p._generateCode = function () {

    //生成   5位数字的验证码
    var code = Math.floor(Math.random() * 90000) + 10000;
    return String(code);//return the captcha text
};


/**
 * 定期清理超时验证码
 */
p._startIntervalClearCode = function () {

    var self = this;
    this.clearCodeMapTimer && clearInterval(this.clearCodeMapTimer);

    this.clearCodeMapTimer = setInterval(function () {

        var limitTimestamp = Date.now() - self.timerInterval;
        for (var id in self.socketID2CodeObjMap) {
            if (self.socketID2CodeObjMap[id].timestamp < limitTimestamp) {
                delete self.socketID2CodeObjMap[id];
            }
        }

        for (var mobileNum in self.mobileNum2LastTime) {
            if (self.mobileNum2LastTime[mobileNum] < limitTimestamp) {
                delete self.mobileNum2LastTime[mobileNum];
            }
        }

    }, this.timerInterval);

};

/**
 * 发送短信
 * @param mobile
 * @param content
 * @param callback  回调， 返回网站的返回码，已经解析成json '{"returnsms":{"returnstatus":["Success"],"message":["ok"],"remainpoint":["5474"],"taskID":["8605449"],"successCounts":["1"]}}'
 * @private
 */
p._sendCode = function (mobile, code, callback) {
    var data = {
        tpl_value: "#code#=" + code + "&" + "#minute#=" + 3,
        mobile: mobile,     //	接收短信的手机号码
        tpl_id: 47544,          //	短信模板ID，请参考个人中心短信模板设置
        dtype: "json",
        key: "91170e6862c0fb5371bcb81b1b4845e6"
    };
    //http://www.duanxin10086.com/sms.aspx?action=send&userid=12802&account=ydqpyxzx&password=123456&mobile=18050273924&content=123333

    data = querystring.stringify(data);

    winston.info(data);

    http.get('http://v.juhe.cn/sms/send?' + data, (res) => {
        // console.log(`Got response: ${res.statusCode}`);
        // consume response body
        res.setEncoding('utf8');
        var resData = "";
        res.on('data', (chunk) => {
            resData += chunk;
        });
        res.on('end', () => {
            callback(null, JSON.parse(resData));
        });
    }).on('error', (e) => {
        callback(e);
    });


};


//对外开放接口
module.exports = new MobileVerifyCodeServer();
//
// module.exports._sendCode('18050273924', "76438", function (data) {
//     console.log(data);
// });

