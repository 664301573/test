/**
 * Created by 黄二杰 on 2016/3/13.
 */


var SqlPool = require('./SqlPool.js');
var ttutil = require("./Util.js");
var ServerUserItem = require("./Core/ServerUserItem.js").ServerUserItem;
var FriendConst = require("./Define.js").FriendConst;
var UserStatusConst = require("./Define.js").UserStatusConst;
var Config = require("./Config.js");
var Mail = require("./Core/Mail.js").Mail;
var PlazaConst = require("./Define.js").PlazaConst;
var AlipayHelper = require("./HttpServer/Pay/zfb/AlipayHelper.js");
var WXPayHelper = require("./HttpServer/Pay/wx/WXPayHelper.js");
var WXLoginUtil = require("./HttpServer/Pay/wx/WXLoginUtil.js");
var WXPayConfig = require("./HttpServer/Pay/wx/WXPayConfig.js");
var GD = require("./GD.js");
var NameGenerator = require("./Generator/NameGenerator");
var winston = require("winston");
var LoginType = require('./Define').LoginType;
var GoodsConst = require('./Define').GoodsConst;
var GoodPayType = require('./Define').GoodPayType;
var GoodPayTypeDesc = require('./Define').GoodPayTypeDesc;
var DailyAttendanceType = require('./Define').DailyAttendanceType;
var verifyMng = require('./Verifycation/VerifycationManager');
var GameIDGenerator = require("./Generator/GameIDGenerator");
var IAPUtil = require("./HttpServer/Pay/iap/IAPUtil.js");
var Https = require("https");
var querystring = require('querystring');
var duoBaoSystem = require("./Core/DuoBaoSystem");
var ActivityHongBaoSystem = require("./Core/ActivityHongBaoSystem");
var Encrypt = require('./extend/Encrypt');
var EventMng = require("./EventMng");
var VerificationCodeHelper = require("./VerificationCodeHelper");
var JuheHelper = require("./JuheHelper");       //聚合助手

var mpinfo = winston.info;
var mpdebug = winston.debug;
var mperror = winston.error;
var mpwarn = winston.warn;
/**
 * 大厅的服务端
 * @constructor
 */
var PlazaServer = function () {



    //socketID 对应的socket
    this.socketID2socketMap = {};

    //监听玩家充值的定时器。定时从数据库查询
    this.listenUserPayTimer = null;

    //检测话费充值是否成功
    this.checkMobileFareTimer = null;

    this.init();

    //设定充值完成后的回调
    EventMng.mng.on(EventMng.event.PaySuccess, this.onPayFinish.bind(this));

    IAPUtil.setPayFinishCallback(this.onPayFinish.bind(this));
};


var p = PlazaServer.prototype;

/**
 * 当充值成功
 * @param args {        orderID: x,            businessID: x,        buyerAccount: x    }
 */
p.onPayFinish = function (args) {

    SqlPool.x_query('CALL ps_mark_pay_to_payed(?,?,?);', [args.orderID, args.businessID, args.buyerAccount], function (err, rows, fields) {
        if (err) {
            return;
        }
    });
};

//当真实用户登录成功
p.onRealUserLogon = function (dbData, socket, isp, ip, channel, version, deviceID) {


    var userID = dbData.userID;

    if (GD.realUserMap[userID] != null) {

        if (GD.realUserMap[userID].status == UserStatusConst.US_OFFLINE) {
            //当做重新连接了
            GD.realUserMap[userID].onReconnection(socket, ip, isp);
        }
        else {
            GD.realUserMap[userID].onKickOut(socket, ip, isp);
        }
    }
    else {
        GD.realUserMap[userID] = new ServerUserItem();
        GD.realUserMap[userID].socket = socket;
        GD.realUserMap[userID].channel = socket._036_channel;
        socket.userID = userID;
    }
    GD.userID2IPISPMap[userID] = {ip: ip, isp: isp};

    var user = GD.realUserMap[userID];
    user.init(dbData, socket, ip, isp, channel, version, deviceID);
    return user;
};

p.init = function () {

    this.initSocket();

    //监听玩家充值
    this.listenUserPay();

    //检测用户充值话费是否成功
    this.checkMobileFare();
};


/**
 * 监听玩家充值
 */
p.listenUserPay = function () {
    //监听玩家充值


    this.listenUserPayTimer && clearInterval(this.listenUserPayTimer);
    this.listenUserPayTimer = setInterval(() => {
        //查找充值成功， 但系统未给他金豆钻石的。
        SqlPool.x_query('CALL ps_read_pay_status(?);', [200], (err, rows, fields) => {
            if (err) {
                return;
            }
            if (rows && rows[0] && rows[0].length > 0) {

                for (var i = 0; i < rows[0].length; i++) {

                    var userID = rows[0][i].userID;
                    var payID = rows[0][i].id;
                    this.handleUserPay(userID, payID);
                }
            }
        });
    }, 5000);

};

//检测用户充值话费是否成功
p.checkMobileFare = function () {


    this.checkMobileFareTimer && clearInterval(this.checkMobileFareTimer);
    this.checkMobileFareTimer = setInterval(() => {
        //查找充话费状态为0的，
        SqlPool.x_query('select myOrderID, userID, ts,phone,num from ps_mobile_fare where `status` = ? or `status` = ?;', [0, -1], (err, rows, fields) => {
            if (err || !rows) {
                return;
            }

            for (let i = 0; i < rows.length; ++i) {
                mpinfo(`正在检测充值话费是否成功 myOrderID=${rows[i].myOrderID},userID=${rows[i].userID},ts=${new Date(rows[i].ts).toLocaleString()},phone=${rows[i].phone}, num=${rows[i].num}`);
                this.juheQueryOrder(rows[i]);
            }

        });


        //查找充话费状态为9的。， 即失败的， 要退还道具的
        SqlPool.x_query('select myOrderID, userID, goodsID,ts, num, phone from ps_mobile_fare where `status` = ?;', [9], (err, rows, fields) => {
            if (err || !rows) {
                return;
            }

            for (let i = 0; i < rows.length; ++i) {
                mpinfo(`正在检测充值话费失败的， 要退还道具的 myOrderID=${rows[i].myOrderID},userID=${rows[i].userID},goodsID=$${rows[i].goodsID},ts=${new Date(rows[i].ts).toLocaleString()}`);
                this.returnGoods(rows[i].myOrderID, rows[i].userID, rows[i].goodsID, rows[i].ts, rows[i].num, rows[i].phone);
            }

        });

    }, 60 * 1000);
};

//退还道具
p.returnGoods = function (myOrderID, userID, goodsID, ts, num, phone) {

    SqlPool.x_query('call ps_mobile_fare_fail_goods_return(?,?,?)', [userID, myOrderID, goodsID], (err, rows) => {

        if (err || !rows || !rows[0] || !rows[0][0]) {
            return;
        }

        if (rows[0][0].t_error == 0) {
            //更新道具表
            GD.goodsSystem.updateUserGoods(userID, goodsID, 1);

            if (GD.realUserMap[userID]) {
                var count = GD.goodsSystem.getUserGoodsCount(userID, goodsID);
                ttutil.psSendSocketData(GD.realUserMap[userID].socket, "UpdateGoods", {goodsID: goodsID, count: count});
            }


            this.writeUserMail(userID, `[话费充值]-您于[${new Date(ts).toLocaleString()}]充值[${num}]元话费到[${phone}]失败，道具现已退还`, "如题");
        }
        else {
            mperror(`退还道具失败myOrderID=${myOrderID}, userID=${userID}, goodsID=${goodsID}`);
        }

    });

};


//查询充值话费订单状态
p.juheQueryOrder = function (info) {
    let myOrderID = info.myOrderID;
    let userID = info.userID;
    let ts = info.ts;
    let phone = info.phone;
    let num = info.num;
    JuheHelper.checkOrder(myOrderID).then(obj => {
        let data = JSON.parse(obj.data);

        if (data.error_code == 0) {

            if (data.result && data.result.game_state != 0) {
                SqlPool.x_query('update ps_mobile_fare set `status` = ? where outOrderID = ? and (`status` = 0 or `status` =-1)', [data.result.game_state, data.result.sporder_id], (err, rows) => {

                    if (err) {
                        return;
                    }

                    if (data.result.game_state == 1) {
                        GD.plazaServer.writeUserMail(userID, `[话费充值]-您于[${new Date(ts).toLocaleString()}]充值[${num}]元话费到[${phone}]已经成功`, "如题");
                    }
                });
            }
        } else {
            //其它error_code 都当作充值失败， 退钱

            SqlPool.x_query('update ps_mobile_fare set `status` = ? where myOrderID = ? and (`status` = 0 or `status` =-1)', [9, myOrderID], (err, rows) => {

                if (err) {
                    return;
                }
            });
        }

    });
};


/**
 * 处理用户充值
 * @param userID
 * @param payID
 */
p.handleUserPay = function (userID, payID) {

    //操作数据库,
    SqlPool.x_query('CALL ps_user_pay(?);', [payID], (err, rows, fields) => {
        if (err) {
            return;
        }
        if (rows && rows[0]) {

            //如果事务有提交
            if (rows[0][0].t_error == 0) {

                var user = GD.realUserMap[userID];


                //充值购买道具的
                if (rows[1][0].t_goodsID) {
                    var goodsID = rows[1][0].t_goodsID;
                    var goodsNum = rows[1][0].t_goodsNum;


                    var userGoodsSet = GD.goodsSystem.getUserGoodsSet(userID);
                    userGoodsSet.updateGoods(goodsID, goodsNum);
                    if (user) {
                        ttutil.psSendSocketData(user.socket, "UpdateGoods", {
                            goodsID: goodsID,
                            count: userGoodsSet.getGoodsCount(goodsID)
                        });
                        user.setDBInfo({payGoodsRMB: user.payGoodsRMB + rows[1][0].t_rmb});

                        ttutil.psSendSocketData(user.socket, "GoodsPayStatus");
                    }

                    //写邮件
                    var title = `[购买]-您通过充值购买的${GD.goodsMap[goodsID].name},数量${goodsNum},已经到账了， 请在背包查收!`;
                    var content = "如题";
                    GD.plazaServer.writeUserMail(userID, title, content);
                }
                //普通充值的
                else {
                    //推荐人
                    var recommendGameID = rows[1][0].recommendGameID;           //当前玩家保险柜里总分数

                    if (user) {

                        // console.log(rows);
                        var data = {};

                        data.allAcer = rows[1][0].acer;                         //当前玩家身上的总钻石
                        data.allPaySum = rows[1][0].paySum;                     //当前玩家总充值数
                        data.memberOrder = rows[1][0].memberOrder;              //当前vip等级
                        data.allScore = rows[1][0].score;                       //当前玩家身上总分数
                        data.allBankScore = rows[1][0].bankScore;           //当前玩家保险柜里总分数


                        data.getAcer = rows[2][0].t_acer;                       //当前玩家获得的钻石
                        data.getPresentAcer = rows[2][0].t_presentAcer;                       //当前玩家获得的赠送钻石
                        data.getRMB = rows[2][0].t_rmb;                         //当前玩家支付的rmb
                        data.getScore = rows[2][0].t_score;                         //当前玩家获得的分数
                        data.getPresentScore = rows[2][0].t_presentScore;                         //当前玩家获得的赠送分数

                        data.orderID = rows[2][0].t_orderID;

                        this.sendSocketData(GD.realUserMap[userID].socket, "PayStatus", data);


                        user.setDBInfo({
                            acer: data.allAcer,
                            paySum: data.allPaySum,
                            memberOrder: data.memberOrder,
                            score: data.allScore,
                            bankScore: data.allBankScore
                        });
                    }

                    //写邮件
                    var title = `[充值] -您已经充值 ${(rows[2][0].t_acer + rows[2][0].t_presentAcer)}钻石赠送${(rows[2][0].t_score + rows[2][0].t_presentScore)}金币成功, 可在商店购买道具`;
                    var content = "如题";
                    GD.plazaServer.writeUserMail(userID, title, content);


                    //如果有填写推荐人
                    if (recommendGameID) {
                        var getRmb = rows[2][0].t_rmb;      //本次充值rmb

                        // var rewardMoney = getRmb * GD.goldRate * GD.acerRate * 0.05;     //本次奖励金额

                        //奖励改成 rmb
                        var rewardMoney = getRmb * 0.05;     //本次奖励rmb金额


                        //处理推荐奖励
                        this.handleRecommendReward(userID, recommendGameID, rewardMoney);
                    }
                }


            }
            else {
                mpwarn("ps_user_pay 事务执行出错 payID = " + payID + ", userID = " + userID);
            }
        }
        else {
            mpwarn("ps_user_pay, payID 不对？？payID = " + payID);
        }
    });
    // }
};

//处理推荐奖励
p.handleRecommendReward = function (userID, recommendGameID, rewardMoney) {

    // var doTast = (userID, rewardMoney, titleStr) => {
    //     SqlPool.x_query("call ps_recommend_reward_rmb(?,?)", [userID, rewardMoney], (err, rows) => {
    //         if (err) {
    //             return;
    //         }
    //         if (GD.realUserMap[userID]) {
    //
    //             GD.realUserMap[userID].setDBInfo({luckyRMB: GD.realUserMap[userID].luckyRMB + rewardMoney});
    //             ttutil.psSendSocketData(GD.realUserMap[userID].socket, "UserInfoUpdate", {luckyRMB: GD.realUserMap[userID].luckyRMB});
    //         }
    //         //////////////////////////////////////////////////////////////////////////////////////////////////////
    //         //写邮件
    //         var title = titleStr;
    //         var content = "如题";
    //         GD.plazaServer.writeUserMail(userID, title, content);
    //
    //         //////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //     });
    // };
    //
    // doTast(userID, rewardMoney, "[红包]-您在钜惠推荐活动中获取" + (rewardMoney.toFixed(2)) + "元红包成功, 请往保险柜验收");
    //
    // var recommendUserID = GD.gameID2UserID[recommendGameID];
    //
    // if (recommendUserID) {
    //     var beRecommendGameID = GD.cacheUserInfoMap[userID].gameID;
    //     doTast(recommendUserID, rewardMoney, "[红包]-您在钜惠推荐活动中通过[id:" + beRecommendGameID + "]获取" + (rewardMoney.toFixed(2)) + "元红包成功, 请往保险柜验收");
    // }
};

/**
 * 发送socket数据
 * @param socket
 * @param eventName
 * @param data
 */
p.sendSocketData = ttutil.psSendSocketData;


/**
 * 初始化socket
 */
p.initSocket = function () {

    GD.plazaSocket = GD.mainSocket.of("/Client");

    var EventType = {
        ReadActivity: "ReadActivity",
        ActivityRandomHongBao: "ActivityRandomHongBao", //后台随机赠送红包活动, 玩家点击获取
        SetRecommendGameID: "SetRecommendGameID",  //设置推荐码
        SetMobileVerifyLevel: "SetMobileVerifyLevel",       //设定手机验证级别， 0表示不验证， 1表示在陌生设备上登录需要验证， 2表示每次都验证
        GetLastSharePrizeDrawList: "GetLastSharePrizeDrawList",//获取上期获奖名单
        DuoBaoGetPrizeInfo: "DuoBaoGetPrizeInfo", //获取夺宝的信息
        DuoBaoEnterDuoBao: "DuoBaoEnterDuoBao",  //进入夺宝
        DuoBaoDoDuoBao: "DuoBaoDoDuoBao",  //执行夺宝操作
        DuoBaoOutDuoBao: "DuoBaoOutDuoBao",  //退出夺宝
        DuoBaoGetPrizeLog: "DuoBaoGetPrizeLog",  //夺宝获取宝物记录
        DuoBaoGetPrizeBetStat: "DuoBaoGetPrizeBetStat",  //夺宝获取押宝统计
        WXWebLoginAddr: "WXWebLoginAddr",  //获取微信登录网页地址
        CodeAddr: "CodeAddr",  //获取验证码地址
        TableLockStatus: "TableLockStatus",    //获取桌子状态
        UserSitdown: "UserSitdown",  //玩家坐下请求
        GameRoomUsers: "GameRoomUsers",  //监听请求房间所有玩家列表
        UserRoomIn: "UserRoomIn",  //玩家进入房间
        UserRoomOut: "UserRoomOut",  //玩家离开房间
        GenerateMobileCode: "GenerateMobileCode",  //生成验证码
        RequestPay: "RequestPay",  //
        RequestTakeCash: "RequestTakeCash",             //请求领取现金红包
        SystemConfig: "SystemConfig",  //监听请求服务器状态
        RegTime: "RegTime",  //用户获取服务器时间
        PhoneReg: "PhoneReg",  //手机玩家注册请求
        GetMailList: "GetMailList",  //读取邮件列表
        ReadSystemMail: "ReadSystemMail",  //读取系统邮件
        WriteSystemMail: "WriteSystemMail",  //修改玩家系统邮件状态
        GetMailListTip: "GetMailListTip",  //读取邮件是否有tip
        VerifyTwoPassword: "VerifyTwoPassword",  //验证二级密码
        VerifyQuestion: "VerifyQuestion",  //验证密保问题
        ForgotPassword: "ForgotPassword",  //忘记登录密码
        ForgotTwoPassword: "ForgotTwoPassword",  //忘记保险柜密码
        VerifyUser: "VerifyUser",  //玩家登陆请求验证用户身份
        GameKind: "GameKind",  //大厅读取游戏分类
        GameList: "GameList",  //大厅读取游戏列表
        GameRoomList: "GameRoomList",  //大厅读取游戏房间列表
        BankBusiness: "BankBusiness",  //保险柜业务 存取出 钻石兑换之类的
        QueryBusiness: "QueryBusiness",  //保险柜查询业务
        TransferMoney: "TransferMoney",  //打赏
        GetRebate: "GetRebate",  //获取返利
        ReadVipConfig: "ReadVipConfig",  //读取VIP信息
        ModifySetup: "ModifySetup",  //修改玩家个人信息
        LabaAnnouncement: "LabaAnnouncement",  //接受玩家喇叭道具喊话信息
        Logout: "Logout",  //当用户登出
        ReadAnnouncement: "ReadAnnouncement",  //游戏公告
        GetScoreRank: "GetScoreRank",  //获取排名
        GetRandomName: "GetRandomName",  //获取昵称
        BindAccount: "BindAccount",  //绑定账号
        ReadPayConfig: "ReadPayConfig",  //读取充值配置
        SystemNotice: "SystemNotice",  //系统公告
        EnterPlazaMain: "EnterPlazaMain",  //进入大厅
        GetGoodsConfig: "GetGoodsConfig",  //获取道具配置
        BuyGoods: "BuyGoods",  //购买道具
        GetGoods: "GetGoods",  //获取道具
        TakeActivityRechargeRebate: "TakeActivityRechargeRebate",       //获取充值回扣
        UseMobQuery: "UseMobQuery",  // Mob查询信息
        UseGoods: "UseGoods",  // 使用道具
        GameRoomTables: "GameRoomTables",          //监听请求房间所有桌子列表
        TableUuid: "TableUuid",                    //获取桌子uuid
        OpenDailyAttendance: "OpenDailyAttendance", //开启每日签到页面
        DailyAttendance: "DailyAttendance",             //日常签到
        Jqueryrotate: "Jqueryrotate",                   //转盘抽奖
        HistoricRecord: "HistoricRecord",               //查询历史游戏成绩

        StoreGoods: "StoreGoods", //获取物品摊位数
        StoreAdd: "StoreAdd", //上架物品
        StoreDec: "StoreDec", //下架物品
        StoreGet: "StoreGet", //获取物品goodsID的所有摊位信息
        StoreBuy: "StoreBuy", //摊位购买
        StoreBatchBuy: "StoreBatchBuy", //摊位批量购买
        UserStoreGet: "UserStoreGet", //获取用户userID的所有摊位信息

        TableUuid: "TableUuid",                       //获取桌子uuid,
        PayCash: "PayCash",
        TakeActivityWXFXJZ: "TakeActivityWXFXJZ",   //领取微信分享集赞活动
        TakeActivityRedPackRainLog: "TakeActivityRedPackRainLog",       //获取红包雨记录

        TakePrizeCodeReward: "TakePrizeCodeReward",                 //领取兑换码奖励
    };


    GD.plazaSocket.on("connection", (socket) => {
        socket.id = socket.id.substring(socket.id.indexOf('#') + 1);

        this.socketID2socketMap[socket.id] = socket;
        mpinfo("新连接成功", socket.id);


        //注册事件
        for (var key in EventType) {
            var event = EventType[key];
            socket.on(Encrypt.packetEvent(event, socket), this.onSocketEvent.bind(this, event, socket));
        }
        socket.on("disconnect", this.onSocketEvent.bind(this, "disconnect", socket));

    });
};
///////////////////////////////////////////////////////////////////////////////////////////////////////////

//后台发红包活动
p.onActivityRandomHongBao = function (socket, data) {

    if (GD.realUserMap[socket.userID]) {

        var userID = socket.userID;
        var hongBaoID = data.id;
        var money = ActivityHongBaoSystem.takeHongBao(hongBaoID, userID);
        if (money > 0) {
            SqlPool.x_query("call ps_activity_take_hong_bao(?,?,?);", [hongBaoID, userID, money], (err, rows) => {
                if (err) {
                    ttutil.psSendSocketData(socket, "ActivityRandomHongBao", {errMsg: "领取红包失败"});
                    return;
                }

                if (GD.realUserMap[userID]) {
                    var takeInfo = ActivityHongBaoSystem.getHongBaoTakeInfo(hongBaoID);
                    GD.realUserMap[userID].setDBInfo({bankScore: GD.realUserMap[userID].bankScore + money});
                    ttutil.psSendSocketData(GD.realUserMap[userID].socket, "ActivityRandomHongBao", {
                        id: hongBaoID,
                        money: money,
                        takeInfo: takeInfo,
                        msgType: 2,         //抢红包
                    });
                    ttutil.psSendSocketData(GD.realUserMap[userID].socket, "UserInfoUpdate", {bankScore: GD.realUserMap[userID].bankScore});
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////
                //写邮件
                var title = "[活动] -您获得红包" + (money) + "成功, 请往保险柜验收";
                var content = "如题";
                GD.plazaServer.writeUserMail(userID, title, content);
                //////////////////////////////////////////////////////////////////////////////////////////////////////

            });
        }
        else {
            var takeInfo = ActivityHongBaoSystem.getHongBaoTakeInfo(hongBaoID);
            ttutil.psSendSocketData(GD.realUserMap[userID].socket, "ActivityRandomHongBao", {
                id: hongBaoID,
                money: Math.abs(money),
                takeInfo: takeInfo,
                msgType: 2,         //抢红包
            });
        }
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////
p.onDuoBaoGetPrizeInfo = function (socket, data) {
    if (GD.realUserMap[socket.userID]) {
        duoBaoSystem.getPrizeInfo("DuoBaoGetPrizeInfo", socket, data);
    }

};

p.onDuoBaoDoDuoBao = function (socket, data) {
    if (GD.realUserMap[socket.userID]) {
        duoBaoSystem.doDuoBao("DuoBaoDoDuoBao", socket, data);
    }
};

p.onDuoBaoEnterDuoBao = function (socket, data) {
    if (GD.realUserMap[socket.userID]) {
        duoBaoSystem.addUser(GD.realUserMap[socket.userID]);
    }
};
//当退出夺宝
p.onDuoBaoOutDuoBao = function (socket, data) {

    if (GD.realUserMap[socket.userID]) {
        duoBaoSystem.removeUser(GD.realUserMap[socket.userID]);
    }
};

p.onDuoBaoGetPrizeLog = function (socket, data) {
    if (GD.realUserMap[socket.userID]) {
        duoBaoSystem.getPrizeLog("DuoBaoGetPrizeLog", socket, data);
    }
};

p.onDuoBaoGetPrizeBetStat = function (socket, data) {
    if (GD.realUserMap[socket.userID]) {
        duoBaoSystem.getPrizeBetStat("DuoBaoGetPrizeBetStat", socket, data);
    }
};

/**
 * 设定手机验证级别
 * @param socket
 * @param data
 */
p.onSetMobileVerifyLevel = function (socket, data) {
    if (GD.realUserMap[socket.userID]) {

        GD.realUserMap[socket.userID].setDBInfo({mobileVerifyLevel: Number(data.mobileVerifyLevel)});

        //保存到数据库
        SqlPool.x_query('update ps_account set mobileVerifyLevel = ? where userID = ?;', [Number(data.mobileVerifyLevel), socket.userID], (err) => {

            if (err) {
                ttutil.psSendSocketData(socket, "SetMobileVerifyLevel", {errMsg: "设置手机验证级别失败"});
                return;
            }
            ttutil.psSendSocketData(socket, "SetMobileVerifyLevel", {
                success: true,
                mobileVerifyLevel: Number(data.mobileVerifyLevel)
            });
            return;
        });

    }
};

//设置我的推荐人
p.onSetRecommendGameID = function (socket, data) {
    var user = GD.realUserMap[socket.userID];
    if (user) {
        if (user.recommendGameID) {
            ttutil.psSendSocketData(socket, "SetRecommendGameID", {errMsg: "您已有推荐人了，推荐人【" + user.recommendGameID + "】"});
            return;
        }
        if (!data.recommendGameID) {
            ttutil.psSendSocketData(socket, "SetRecommendGameID", {errMsg: "参数错误"});
            return;
        }

        if (data.recommendGameID == user.gameID) {
            ttutil.psSendSocketData(socket, "SetRecommendGameID", {errMsg: "推荐码不能是自己的"});
            return;
        }

        SqlPool.x_query("call ps_set_recommend_game_id(?,?)", [user.userID, data.recommendGameID], (err, rows) => {
            if (err) {
                ttutil.psSendSocketData(socket, "SetRecommendGameID", {errMsg: "系统错误"});
                return;
            }
            if (rows[0][0].errMsg) {
                ttutil.psSendSocketData(socket, "SetRecommendGameID", {errMsg: rows[0][0].errMsg});
                return;
            }
            else {
                ttutil.psSendSocketData(socket, "SetRecommendGameID", {success: true});
                if (GD.realUserMap[socket.userID]) {
                    GD.realUserMap[socket.userID].setDBInfo({recommendGameID: Number(data.recommendGameID)});
                }
                return;
            }
        });
    }
};

/**
 * 当socket事件来临时会被汇总到该函数
 * @param eventName
 * @param socket
 * @param data
 */
p.onSocketEvent = function (eventName, socket, data) {

    try {

        mpdebug({eventName: eventName, data: data});

        if (eventName != "disconnect") {
            var encryptData = data;

            data = Encrypt.decryptData(encryptData, socket);

            if (data == null) {
                //写数据库报备
                SqlPool.x_query('CALL ps_record_bad_plaza_net_message(?,?,?,?);', [socket.userID, encryptData, eventName, 1], (err) => {
                });
                //暂时不返回， 过段时间再返回，
                return;
            }
        }


        var functionName = "on" + eventName[0].toUpperCase() + eventName.substr(1);

        //存在， 且为函数
        if (this[functionName] != null && typeof this[functionName] == "function") {
            this[functionName](socket, data);
        }
    } catch (err) {
        mperror("---------------------------------------------------------------------------------");
        mperror({eventName: eventName, data: data});
        mperror(err);

        if (socket.userID != null && GD.realUserMap[socket.userID]) {
            GD.realUserMap[socket.userID].logout();
        }
        socket.activeDisConnect = true;
        socket.disconnect();

        mperror("---------------------------------------------------------------------------------");
    }
};


/**
 * 创建一充值订单
 * @param money
 * @param payName
 * @param userID
 * @param channel
 * @param goodsID
 * @param goodsNum
 * @param callback
 */
p.generatePayOrder = function (money, payName, userID, channel, goodsID, goodsNum, callback) {

    var orderID = ttutil.generateOrderID();
    var retData;

    if (goodsID) {
        retData = {acer: 0, score: 0};
    }
    else {
        retData = ttutil.getPayInfoByMoney(GD.payRatioMap, money);
        if (!retData) {
            return;
        }
    }

    SqlPool.x_query('CALL ps_insert_pay_record(?,?,?,?,?,?,?,?,?,?,?);', [userID, orderID, payName, money, retData.acer, 0, retData.score, 0, channel, goodsID, goodsNum], function (err, rows, fields) {
        callback && callback(err, userID, orderID, payName, money, retData.acer, 0, retData.score, 0, goodsID, goodsNum);
    });

};


/**
 * 读取活动
 * @param socket
 * @param data
 */
p.onReadActivity = function (socket, data) {


    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {

        SqlPool.x_query('CALL ps_read_activity();', (err, rows, fields) => {
            if (err) {
                return;
            }

            ttutil.psSendSocketData(socket, "ReadActivity", rows[0]);
        });
    }
};


/**
 * 获取上期的获奖名单
 * @param socket
 * @param data
 */
p.onGetLastSharePrizeDrawList = function (socket, data) {

    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {

        SqlPool.x_query('CALL ps_share_prize_draw_name_list(?,?);', [1, data.type], (err, rows, fields) => {
            if (err) {
                ttutil.psSendSocketData(socket, "GetLastSharePrizeDrawList", {errMsg: "系统出错"});
                return;
            }
            if (rows && rows[0] && rows[0].length > 0) {

                var title = "第" + rows[0][0].groupID + "期" + (data.type == 1 ? "亿元大礼" : "分享再送大礼") + "获奖名单";
                ttutil.psSendSocketData(socket, "GetLastSharePrizeDrawList", {list: rows[0], title: title});
            }
            else {
                ttutil.psSendSocketData(socket, "GetLastSharePrizeDrawList", {errMsg: "没有记录"});
            }

        });
    }
};

/**
 *
 * @param socket
 * @param data
 */
p.onCodeAddr = function (socket, data) {
    var isLimit = this.onCodeLimit(socket, data);

    if (isLimit) {
        ttutil.psSendSocketData(socket, "CodeAddr", {errMsg: "验证码请求太过频繁！"});
        return;
    }
    this.onCodeAddrAuto(socket, data);
};

/**
 *
 * @param socket
 * @param data
 */
p.onCodeAddrAuto = function (socket, data) {
    // CodeAddr 为历史遗留名称，原先是发送验证码地址，已改为发送验证码图片base64数据

    var channel = data.channel;
    var os = data.os;

    var last5min = Date.now() - 60 * 60 * 1000;
    while (GD.last5minRegInfoArray[0] < last5min) {
        GD.last5minRegInfoArray.shift();
    }


    var funcName;
    switch (os) {

        case "Windows":
            funcName = "randomCode";
            break;
        default:
            funcName = "colorNum";
            break;
    }


    VerificationCodeHelper[funcName]().then(obj => {
        //count为尝试次数
        GD.socketID2CodeMap[ttutil.md5(socket.id)] = {code: obj.code, timestamp: Date.now(), count: 0};


        ttutil.psSendSocketData(socket, "CodeAddr", {base64: obj.buffer.toString('base64')});
    }).catch(e => {

        ttutil.psSendSocketData(socket, "CodeAddr", {base64: ""});
    });


};

/**
 * 验证码请求限制
 * @param soclet
 * @param data
 */
p.onCodeLimit = function (socket, data) {
    var id = ttutil.md5(socket.id);
    var result = typeof(GD.socketID2CodeTimeMap[id]) === "undefined" ? false : Date.now() - GD.socketID2CodeTimeMap[id].timestamp < 1000;
    if (!result) {
        GD.socketID2CodeTimeMap[id] = {timestamp: Date.now()};
    }
    return result
};
/**
 * 清除注册码
 * @param socket
 * @param data
 */
p.onCodeDel = function (socket, data) {
    var id = ttutil.md5(socket.id);

    delete GD.socketID2CodeMap[id];
};
/**
 * 微信登录地址
 * @param socket
 * @param data
 */
p.onWXWebLoginAddr = function (socket, data) {


    var socketIDMD5 = ttutil.md5(socket.id);


    if (Config.IsDebug) {
        socketIDMD5 = "Debug" + socketIDMD5;
    }

    var urlData = {
        appid: WXPayConfig.webAPPID,
        redirect_uri: Config.WXRedirectUri,
        response_type: "code",
        scope: "snsapi_login",
        state: socketIDMD5
    };


    var args = querystring.stringify(urlData);
    ttutil.psSendSocketData(socket, "WXWebLoginAddr", {url: "https://open.weixin.qq.com/connect/qrconnect?" + args + "#wechat_redirect"});

};

/**
 * 当微信web版本登录后， 腾讯回调过来
 * @param socketIDMD5
 * @param code
 */
p.onWXWebLoginCallback = function (socketIDMD5, code) {

    for (var socketID in this.socketID2socketMap) {
        var md5 = ttutil.md5(socketID);

        if (md5 == socketIDMD5) {
            ttutil.psSendSocketData(this.socketID2socketMap[socketID], "WXLoginCode", {code: code, isWeb: true});
            return;
        }
    }
};


/**
 * 请求支付
 * @param socket
 * @param data
 */
p.onRequestPay = function (socket, data) {


    var self = this;
    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {

        var payType2Name = {
            1: "支付宝支付",
            2: "微信支付",
            3: "苹果内购"
        };
        // 苹果审核需要内购,暂时开启
        // if (data.type == 3) {
        //     self.sendSocketData(socket, "RequestPay", {errMsg: "苹果内购暂时关闭， 请先用 微信或支付宝充值"});
        //     return;
        // }
        //检查参数
        if (!data || (!data.goodsID && (!data.money || !payType2Name[data.type] || !GD.payRatioMap[data.money]))) {
            self.sendSocketData(socket, "RequestPay", {errMsg: "参数错误"});
            return;
        }

        if (data.goodsID && (data.goodsNum <= 0 || !GD.goodsMap[data.goodsID])) {
            self.sendSocketData(socket, "RequestPay", {errMsg: "参数错误"});
            return;
        }
        if (data.goodsID) {
            var goodsConfig = GD.goodsMap[data.goodsID];

            //如果不是用推广红包购买的方式则不行
            if (goodsConfig.type != 10) {
                self.sendSocketData(socket, "RequestPay", {errMsg: "参数错误,该道具不能用充值方式购买"});
                return;
            }
            data.money = goodsConfig.price * data.goodsNum;

        }

        //生成支付订单
        this.generatePayOrder(data.money, payType2Name[data.type], userID, socket._036_channel, data.goodsID, data.goodsNum, function (err, userID, orderID, payName, money, acer, presentAcer, score, presentScore, goodsID, goodsNum) {

            if (err) {
                self.sendSocketData(socket, "RequestPay", {errMsg: "系统错误"});
                return;
            }

            var goodsConfig = GD.goodsMap[goodsID];


            var finishAcer = acer + presentAcer;
            var finishScore = score + presentScore;

            var desc;

            if (goodsConfig) {
                desc = `${money}元购买${goodsNum}个${goodsConfig.name}道具`;
            }
            else {
                desc = `${money}元购买${finishAcer}钻石,赠送${finishScore}金币`;
            }


            var qr = require("qr-image");

            //支付宝
            if (data.type == 1) {

                if (data.channel == "web") {
                    var requestREQ = AlipayHelper.pay(orderID, money, desc, "page");
                    self.sendSocketData(socket, "RequestPay", {
                        req: requestREQ,
                        type: data.type,
                        channel: data.channel,
                        orderID: orderID,
                        money: money,
                    });
                }
                else if (data.channel == "qrcode") {
                    AlipayHelper.qrCode(orderID, money, desc).then(result => {

                        var buffer = result.qr_code && qr.imageSync(result.qr_code, {type: 'png', size: 10, margin: 2});

                        self.sendSocketData(socket, "RequestPay", {
                            base64: buffer && buffer.toString('base64'),
                            desc: desc,
                            type: data.type,
                            channel: data.channel,
                            orderID: orderID,
                            money: money,
                        });
                    }).catch(err => {
                        self.sendSocketData(socket, "errMsg", "支付错误");
                    });


                }
                else if (data.channel == "wap") {
                    var requestREQ = AlipayHelper.pay(orderID, money, desc, "wap");
                    self.sendSocketData(socket, "RequestPay", {
                        req: requestREQ,
                        type: data.type,
                        channel: data.channel,
                        orderID: orderID,
                        money: money,
                    });
                }
                else {
                    var requestREQ = AlipayHelper.pay(orderID, money, desc, "app");
                    self.sendSocketData(socket, "RequestPay", {req: requestREQ, type: data.type});
                }


            }
            else if (data.type == 2) {

                if (data.channel == "web") {

                    WXPayHelper.unifiedOrder(orderID, money, desc, "NATIVE").then(function (object) {
                        if (!object) {
                            self.sendSocketData(socket, "errMsg", "微信支付错误, 请联系客服");
                        }
                        else {

                            var codeUrl = object.code_url;
                            codeUrl = PROTOCOL + "www." + DOMAIN + "/index/wxpay?desc=" + desc + "&url=" + codeUrl;
                            self.sendSocketData(socket, "RequestPay", {
                                req: codeUrl,
                                type: data.type,
                                channel: data.channel,
                                orderID: orderID,
                                money: money,
                            });
                        }
                    });
                }
                else if (data.channel == "qrcode") {


                    WXPayHelper.unifiedOrder(orderID, money, desc, "NATIVE").then(function (object) {
                        if (!object) {
                            self.sendSocketData(socket, "errMsg", "微信支付错误, 请联系客服");
                        }
                        else {

                            var codeUrl = object.code_url;
                            var buffer = qr.imageSync(codeUrl, {type: 'png', size: 10});

                            self.sendSocketData(socket, "RequestPay", {
                                base64: buffer.toString('base64'),
                                desc: desc,
                                type: data.type,
                                channel: data.channel,
                                orderID: orderID,
                                money: money,
                            });
                        }
                    });


                }
                else {

                    WXPayHelper.appPay(orderID, money, desc).then(function (object) {
                        if (!object || !object.prepayid) {
                            self.sendSocketData(socket, "errMsg", "微信支付错误, 请联系客服");
                        }
                        else {

                            self.sendSocketData(socket, "RequestPay", {
                                req: JSON.stringify(object),
                                type: data.type,
                                channel: data.channel,
                                orderID: orderID,
                                money: money,
                            });
                        }
                    });

                }

            }
            // 苹果内购买
            else if (data.type == 3) {
                var verifyUrl = "https://" + Config.payHost + ":" + Config.httpsServerPort + "/verifyIAPReceipt?orderID=" + orderID + "&";
                var reqData = {
                    productId: data.productId,
                    verifyUrl: verifyUrl
                };

                self.sendSocketData(socket, "RequestPay", {
                    req: reqData, type: data.type,
                    orderID: orderID,
                    money: money,
                });
            }
        });
    }
};

/**
 * 请求领取现金红包
 * @param socket
 * @param data
 */
p.onRequestTakeCash = function (socket, data) {


    var self = this;
    var userID = socket.userID;

    if (userID == null)
        return;

    var userInfo = GD.realUserMap[userID];

    SqlPool.x_query('call ps_take_cash(?)', [userInfo.wxUnionID], (err, results) => {
        if (err) {
            ttutil.psSendSocketData(socket, "RequestTakeCash", {errMsg: "领取现金红包失败，请稍后重试！"});
            return;
        }

        var result = results[0][0];
        if (result.errMsg) {
            ttutil.psSendSocketData(socket, "RequestTakeCash", {errID: result.errID, errMsg: result.errMsg});
            return;
        }

        userInfo.luckyRMB += result.luckyRMB;

        ttutil.psSendSocketData(socket, "UserInfoUpdate", {
            acer: userInfo.acer,
            score: userInfo.score,
            bankScore: userInfo.bankScore,
            luckyRMB: userInfo.luckyRMB
        });

        //写邮件
        var content = "如题";
        var title = `[活动]-您领取现金红包成功，请在保险柜查收`;
        GD.plazaServer.writeUserMail(userID, title, content);

        ttutil.psSendSocketData(socket, "RequestTakeCash", {successMsg: title});
    });
};

/**
 * 当用户登出
 * @param socket
 * @param data
 */
p.onLogout = function (socket, data) {

    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {
        GD.realUserMap[userID].logout();
    }

};


/**
 * 游戏公告
 * @param socket
 * @param data
 */
p.onReadAnnouncement = function (socket, data) {
    var self = this;
    SqlPool.x_query("Call ps_announcement_read()", function (err, rows, fields) {
        if (err) return;

        if (rows && rows[0]) {

            var annouceArray = [];
            for (var i = 0; i < rows[0].length; i++) {
                var tmp = {};
                tmp.announcement = rows[0][i].announcement;
                tmp.link = rows[0][i].link;
                tmp.createTime = rows[0][i].createTime.getTime();
                annouceArray.push(tmp);
            }

            self.sendSocketData(socket, "ReadAnnouncement", {announcement: annouceArray})
        }
    });
};

/**
 * 验证码
 * @param socket
 * @param data
 */
p.onGenerateMobileCode = function (socket, data) {

    var self = this;
    if (!data || !data.mobileNum) {
        self.sendSocketData(socket, "GenerateMobileCode", {errMsg: "参数错误"});
        return;
    }


    //绑定是要验证或解绑手机号码的
    if (data.mobileNum == "***********") {
        var userID = socket.userID;

        //已经登录的
        if (userID) {
            var userInfo = GD.realUserMap[userID];
            if (!userInfo) {
                self.sendSocketData(socket, "GenerateMobileCode", {errMsg: "请登录"});
                return;
            }
            data.mobileNum = userInfo.phone;
        }
        //未登录的
        else {
            data.mobileNum = socket.mobileNum;
        }

    }

    if (!data.mobileNum || data.mobileNum.length != 11 || !ttutil.isMobileNum(data.mobileNum)) {
        self.sendSocketData(socket, "GenerateMobileCode", {errMsg: "参数错误"});
        return;
    }


    var socketID = ttutil.md5(socket.id);
    if (data.code == null || GD.socketID2CodeMap[socketID] == null) {
        self.sendSocketData(socket, "GenerateMobileCode", {errMsg: "图形验证码错误"});
        return;
    }
    if (GD.socketID2CodeMap[socketID].code != data.code.toUpperCase()) {
        GD.socketID2CodeMap[socketID].count++;
        self.sendSocketData(socket, "GenerateMobileCode", {errMsg: "图形验证码错误"});

        if (GD.socketID2CodeMap[socketID].count >= 3) {
            delete GD.socketID2CodeMap[socketID];
            //刷新验证码 add by canaddy
            self.onCodeAddr(socket);
        }
        return;
    }

    var mobileNum = data.mobileNum;
    GD.mobileVerifyCodeServer.sendCode(socketID, mobileNum, function (res) {

        if (res) {
            self.sendSocketData(socket, "GenerateMobileCode", {success: true});
        }
        else {
            self.sendSocketData(socket, "GenerateMobileCode", {errMsg: "获取验证码失败,请不要太频繁"});
        }
    });

};


//用户获取服务器时间
p.onRegTime = function (socket, data) {
    var ip = socket.handshake.address.substr(7);

    var self = this;
    SqlPool.x_query('CALL ps_read_filter(?,?);', [ip, 1], function (err, rows, fields) {
        if (err) {
            return;
        }

        var myDate = new Date();
        if ((myDate.getMonth() + 1) < 10) var y = "0" + (myDate.getMonth() + 1);
        else var y = (myDate.getMonth() + 1);
        if (myDate.getDate() < 10) var r = "0" + myDate.getDate();
        else var r = myDate.getDate();
        var sj = myDate.getFullYear() + "" + y + "" + r;

        var limit = true;

        if (rows && rows[0]) {
            if (rows[0][0].num > 0) {
                limit = false;
            }
        }

        self.sendSocketData(socket, "regTime", {Time: Number(sj), Limit: limit});

    });
};

/**
 * 当收到qq的openID
 * @param socketID
 * @param args [nickname, gender, openid]
 */
p.onQQOpenID = function (socketID, args) {

    var self = this;

    var socket = null;
    for (var id in this.socketID2socketMap) {
        if (id == socketID) {
            socket = this.socketID2socketMap[id];
            break;
        }
    }
    if (!socket) {
        return;
    }

    self.sendSocketData(socket, "qqlogon", {nickname: args.nickname, gender: args.gender, openid: args.openid});

};

//获取自己桌子的UU
p.onTableUuid = function (socket, data) {
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {
        var user = GD.realUserMap[userID];
        var moduleID = user.moduleID;
        var roomID = user.roomID;
        var tableID = user.tableID;
        var room = GD.gameMap[moduleID].roomMap[roomID];

        if (roomID == null || moduleID == null || tableID == null || !GD.gameMap[moduleID] || !GD.gameMap[moduleID].roomMap[roomID]) {
            //返回错误房间连接断开信息
            this.sendSocketData(socket, 'TableUuid', {errMsg: "玩家位置出错"});
            return;
        }
        var room = GD.gameMap[moduleID].roomMap[roomID];

        if (tableID == null || tableID >= room.tableCount || !room.tableArray[tableID]) {
            this.sendSocketData(socket, "TableUuid", {errMsg: "玩家位置出错"});
            return;
        }
        var uuid = room.tableArray[tableID].uuid;


        this.sendSocketData(socket, 'TableUuid', {uuid: uuid});
    }
};

//玩家坐下请求
p.onUserSitdown = function (socket, data) {


    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {

        data = data || {};
        var user = GD.realUserMap[userID];
        var moduleID = user.moduleID;
        var roomID = user.roomID;

        var tableID = data.TableID;
        var chairID = data.ChairID;
        var agentID = data.agentID;

        var enterPwd = data.enterPwd;
        var setPwd = data.setPwd;
        var uuid = data.uuid;

        if (roomID == null || moduleID == null || !GD.gameMap[moduleID] || !GD.gameMap[moduleID].roomMap[roomID]) {
            //返回错误房间连接断开信息
            this.sendSocketData(socket, 'UserSitdown', {errMsg: "玩家房间错误"});
            return;
        }
        var room = GD.gameMap[moduleID].roomMap[roomID];
        //快速加入 或者百人桌子
        if (data.action == 1 || (data.TableID == null && data.action == null)) {
            //寻找可供快速加入的桌子椅子
            var position = room.findQuicklyJoinTableAndChairID();
            if (position != null) {
                room.onUserSitdown(user, position.tableID, position.chairID, agentID);
                return;
            }
            this.sendSocketData(socket, 'UserSitdown', {errMsg: "没有可坐的位置。"});

            // try {
            //     winston.error("-没有可坐的位置---------------------------------");
            //     var tempArray = [];
            //     for (var i = 0; i < room.tableArray.length; ++i) {
            //         var table = room.tableArray[i];
            //         for (var j = 0; j < table.chairCount; ++j) {
            //             var chair = table.chairArray[j];
            //
            //             chair.user && tempArray.push(chair.user.getUserInfoPrivate());
            //         }
            //     }
            //     winston.error("room.tableArray:", tempArray);
            //     winston.error("-没有可坐的位置---------------------------------");
            //
            // }
            // catch (e) {
            //     winston.error("-没有可坐的位置---------------------------------", e);
            // }

            return;
        }
        else {
            room.onUserSitdown(user, tableID, chairID, agentID, enterPwd, setPwd, userID, uuid);
            return;
        }

    }

};

//监听请求房间所有玩家列表
p.onGameRoomUsers = function (socket, data) {
    var self = this;
    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {


        if (data.GamesRoomID == null) {
            this.sendSocketData(socket, "GameRoomUsers", {errMsg: "参数错误"});
            return;
        }

        var user = GD.realUserMap[userID];
        var roomID = data.GamesRoomID;
        var moduleID = user.moduleID;

        if (!GD.gameMap[moduleID]) {
            self.sendSocketData(socket, "GameRoomUsers", {errMsg: `找不到该游戏${moduleID}`});
            return;
        }

        if (GD.gameMap[moduleID].roomMap[roomID]) {

            var room = GD.gameMap[moduleID].roomMap[roomID];
            var roomUserInfoArray = [];
            for (var i = 0; i < room.realUserArray.length; ++i) {

                var userInfo = room.realUserArray[i].getUserInfo();

                roomUserInfoArray.push(userInfo);
            }
            for (var i = 0; i < room.androidUserArray.length; ++i) {

                var userInfo = room.androidUserArray[i].getUserInfo();

                roomUserInfoArray.push(userInfo);
            }
            self.sendSocketData(socket, "GameRoomUsers", roomUserInfoArray);
        }


    }
};

//获取房间桌子列表
p.onGameRoomTables = function (socket, data) {
    var self = this;
    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {


        if (data.GamesRoomID == null) {
            this.sendSocketData(socket, "GameRoomUsers", {errMsg: "参数错误"});
            return;
        }

        var user = GD.realUserMap[userID];
        var roomID = data.GamesRoomID;
        var moduleID = user.moduleID;


        if (GD.gameMap[moduleID].roomMap[roomID]) {

            var room = GD.gameMap[moduleID].roomMap[roomID];
            var roomTableInfoArray = [];
            for (var i = 0; i < room.tableArray.length; ++i) {

                var tableInfo = room.tableArray[i].getTableInfo();

                roomTableInfoArray.push(tableInfo);
            }
            self.sendSocketData(socket, "GameRoomTables", roomTableInfoArray);
        }

    }

};

//玩家进入房间
p.onUserRoomIn = function (socket, data) {

    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {

        var user = GD.realUserMap[userID];
        var moduleID = user.moduleID;               //所在的游戏
        var roomID = data.GamesRoomID;          //请求进入的 游戏房间

        //ip都有服务端获取
        data.Ip = GD.realUserMap[userID].loginIP;

        if (roomID == null || moduleID == null || GD.gameMap[moduleID] == null || GD.gameMap[moduleID].roomMap[roomID] == null) {
            return;
        }

        var room = GD.gameMap[moduleID].roomMap[roomID];
        room.onUserEnter(user, data);

    }
};

//玩家离开房间
p.onUserRoomOut = function (socket, data) {

    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {
        var user = GD.realUserMap[userID];

        var moduleID = user.moduleID;
        var roomID = user.roomID;

        if (moduleID != null && roomID != null && GD.gameMap[moduleID] && GD.gameMap[moduleID].roomMap[roomID]) {
            var room = GD.gameMap[moduleID].roomMap[roomID];
            room.onUserLeave(user);
        }
    }
};


//监听请求服务器状态
p.onSystemConfig = function (socket, data) {

    var self = this;
    SqlPool.x_query("select * from ps_system_config", function (err, rows, fields) {
        if (err) {
            return;
        }
        if (rows) {
            var systemConfig = rows[0];

            if (systemConfig) {
                self.sendSocketData(socket, "SystemConfig", systemConfig);
            }
            //
            // //
            // if (systemConfig.maintenance != 0) {
            //     self.sendSocketData(socket, "SystemConfig", systemConfig.maintenanceMsg);
            // }


        }
    });
};


p.onPhoneReg = function (socket, data) {
    if (data == null) return;

    if (!GD.mainServer.isLegitimateNickName(data.nickname)) {
        ttutil.psSendSocketData(socket, "PhoneReg", {errMsg: "不是合法昵称"});
        return;
    }

    var self = this;
    self.doRegUser(socket, data, "PhoneReg");

};


/**
 * 参数什么的都要验证合法了才调用这个函数
 * @param socket
 * @param data
 */
p.doRegUser = function (socket, data, eventName) {
    var self = this;
    var ip = data.ip || socket.handshake.address.substr(7);
    var sql = 'CALL ps_user_reg(?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?);';

    data.gender = data.gender == null ? 0 : data.gender;
    data.faceID = data.faceID == null ? 0 : data.faceID;

    var id = ttutil.md5(socket.id);

    if (data.code == null || GD.socketID2CodeMap[id] == null) {
        self.sendSocketData(socket, eventName, {errMsg: "验证码错误"});
        return;
    }
    if (GD.socketID2CodeMap[id].code != data.code.toUpperCase()) {
        GD.socketID2CodeMap[id].count++;
        self.sendSocketData(socket, eventName, {errMsg: "验证码错误"});

        if (GD.socketID2CodeMap[id].count >= 3) {
            delete GD.socketID2CodeMap[id];
            //刷新验证码 add by canaddy
            self.onCodeAddrAuto(socket, data);
        }
        return;
    }


    //成功后就删除
    delete GD.socketID2CodeMap[id];
    eventName = eventName ? eventName : "reg";

    var gameID = GameIDGenerator.generatorGameID();
    if (gameID == null) {
        self.sendSocketData(socket, eventName, {errMsg: "生成游戏ID失败"});
        //刷新验证码 add by canaddy
        self.onCodeAddrAuto(socket, data);
        return;
    }

    var val = [gameID, data.account, data.nickname, data.password, data.gender, data.faceID, ip, null, null, LoginType.AccountType, data.deviceID, data.os, data.channel, data.version];
    //开始注册
    SqlPool.x_query(sql, val, function (err, rows, fields) {
        if (err) {
            return;
        }
        if (rows && rows[0]) {
            if (rows[0][0].msgid == 1) {
                mpinfo("玩家新账号注册成功");
                self.sendSocketData(socket, eventName, {success: true});

                GD.last5minRegInfoArray.push(Date.now());
            } else {
                self.sendSocketData(socket, eventName, rows[0][0]);
                //刷新验证码 add by canaddy
                self.onCodeAddrAuto(socket, data);
            }
        } else {
            self.sendSocketData(socket, eventName, {errMsg: "注册失败"});
            //刷新验证码 add by canaddy
            self.onCodeAddrAuto(socket, data);
        }
    });
};

p.onForgotPassword = function (socket, data) {

    if (!data) {
        this.sendSocketData(socket, "ForgotPassword", {errMsg: "参数错误"});
        return;
    }

    if (!data.account && !socket.forgotPasswordAccount) {
        this.sendSocketData(socket, "ForgotPassword", {errMsg: "输入账号不能为空"});
        return;
    }

    var accountInfo = GD.cacheUserAccountMap[data.account || socket.forgotPasswordAccount];
    if (!accountInfo) {
        this.sendSocketData(socket, "ForgotPassword", {errMsg: "没有该账号信息"});
        return;
    }

    if (!accountInfo.phone) {
        this.sendSocketData(socket, "ForgotPassword", {errMsg: "您没有绑定手机, 无法找回密码！"});
        return;
    }

    if (data.account) {
        socket.forgotPasswordAccount = data.account;
        socket.mobileNum = accountInfo.phone;

        var phone = accountInfo.phone;
        phone = phone[0] + phone[1] + phone[2] + "****" + phone[7] + phone[8] + phone[9] + phone[10];

        this.sendSocketData(socket, "ForgotPassword", {
            success: true,
            phone: phone
        });
    }
    else {
        var socketID = ttutil.md5(socket.id);
        var codeObj = GD.mobileVerifyCodeServer.getCodeObj(socketID);

        if (!codeObj) {
            this.sendSocketData(socket, "ForgotPassword", {errMsg: "手机验证码错误"});
            return;
        }

        if (codeObj.mobileNum != socket.mobileNum) {
            this.sendSocketData(socket, "ForgotPassword", {errMsg: "手机号码不正确"});
            return;
        }

        if (codeObj.code != data.mobileCode) {

            this.sendSocketData(socket, "ForgotPassword", {errMsg: "手机验证码错误"});
            codeObj.count++;
            if (codeObj.count > 3) {
                GD.mobileVerifyCodeServer.deleteCodeObj(socketID);
            }
            return;
        }

        var randomPassword = "";
        for (var i = 0; i < 6; i++)
            randomPassword += Math.floor(Math.random() * 10);

        var password = Encrypt.SHA1(randomPassword).toUpperCase();

        var account = socket.forgotPasswordAccount;
        var userID = GD.cacheUserAccountMap[account] ? GD.cacheUserAccountMap[account].userID : null;

        socket.forgotPasswordAccount = null;
        GD.mobileVerifyCodeServer.deleteCodeObj(socketID);

        //  1 修改密码   2 找回密码 直接设定新密码
        SqlPool.x_query('CALL ps_user_modify_password(?,?,?,?);', [userID, null, password, 2], (err, rows, fields) => {
            if (err) {
                this.sendSocketData(socket, "ForgotPassword", {errMsg: "修改失败"});
                return;
            }
            if (rows && rows[0]) {
                if (rows[0][0].msgID == 1) {
                    this.sendSocketData(socket, "ForgotPassword", {success: true, password: randomPassword});

                    if (GD.cacheUserInfoMap[userID])
                        GD.cacheUserInfoMap[userID].password = password;
                } else if (rows[0][0].msgID == 2) {
                    this.sendSocketData(socket, "ForgotPassword", {
                        errMsg: rows[0][0].errMsg
                    });
                }
            }
        });
    }
};

p.onForgotTwoPassword = function (socket, data) {

    if (!data) {
        this.sendSocketData(socket, "ForgotTwoPassword", {errMsg: "参数错误"});
        return;
    }

    var userID = socket.userID;
    var userInfo = GD.realUserMap[userID];

    if (!userInfo.phone) {
        this.sendSocketData(socket, "ForgotTwoPassword", {errMsg: "您没有绑定手机, 无法找回密码！"});
        return;
    }

    var socketID = ttutil.md5(socket.id);
    var codeObj = GD.mobileVerifyCodeServer.getCodeObj(socketID);

    if (!codeObj) {
        this.sendSocketData(socket, "ForgotTwoPassword", {errMsg: "手机验证码错误"});
        return;
    }

    if (codeObj.mobileNum != userInfo.phone) {
        this.sendSocketData(socket, "ForgotTwoPassword", {errMsg: "手机号码不正确"});
        return;
    }

    if (codeObj.code != data.mobileCode) {

        this.sendSocketData(socket, "ForgotTwoPassword", {errMsg: "手机验证码错误"});
        codeObj.count++;
        if (codeObj.count > 3) {
            GD.mobileVerifyCodeServer.deleteCodeObj(socketID);
        }
        return;
    }

    this.sendSocketData(socket, "ForgotTwoPassword", {success: true});
    GD.mobileVerifyCodeServer.deleteCodeObj(socketID);
};

/**
 * 玩家请求身份验证，包括游客注册咯
 * @param socket
 * @param data
 * {
 *      isp:x,
 *      moorMachine:x ,     机器码
 *      //互斥 账号密码
 *      account:x,
 *      password:x,
 *      //qq登陆
 *      qqID:x
 *      //游客登陆
 *      guestID:x,  如果为null，就注册一个
 *      //
 *      //wxCode:wxCode, 通过这个去获取wx的openID
 *      //
 *      //类型
 *      loginType:x,  1 是账号密码， 2 游客登陆，3 QQ， 4 微信
 * }
 *
 *
 */
p.onVerifyUser = function (socket, data) {
    var self = this;

    if (!data.isp) data.isp = "";

    //因为用了代理  原始ip不能直接通过socket获取, so  直接让客户端传吧
    data.ip = data.ip || socket.handshake.address.substr(7);

    //todo
    SqlPool.x_query("select * from ps_system_config", function (err3, rows3, fields3) {
        if (err3) {
            return;
        }
        if (rows3) {
            var systemConfig = rows3[0];

            // console.log(systemConfig)
            //
            if (systemConfig.maintenance != 0) {

                if (data.loginType == LoginType.AccountType) {

                    SqlPool.x_query("select * from ps_user_privilege where userID = (select userID from ps_account where account = ?)", [data.account], function (err, rows) {

                        if (rows[0] != null) {
                            self.login(socket, data, null);
                        }
                        else {

                            self.sendSocketData(socket, "VerifyUser", {errMsg: systemConfig.maintenanceMsg});
                        }


                    });


                }
                else {
                    self.sendSocketData(socket, "VerifyUser", {errMsg: systemConfig.maintenanceMsg});
                }
            }
            else {
                self.doVerifyUser(socket, data);
            }

        }
    });


};


p.doVerifyUser = function (socket, data) {

    var self = this;
    if (data.loginType == LoginType.AccountType) {
        self.login(socket, data, null);
    } else if (data.loginType == LoginType.GuestType) {
        //进行注册
        if (!data.guestID) {

            var id = ttutil.md5(socket.id);

            if (data.code == null || GD.socketID2CodeMap[id] == null) {
                self.sendSocketData(socket, "VerifyUser", {errMsg: "验证码错误"});
                return;
            }
            if (GD.socketID2CodeMap[id].code != data.code.toUpperCase()) {
                GD.socketID2CodeMap[id].count++;
                self.sendSocketData(socket, "VerifyUser", {errMsg: "验证码错误"});

                if (GD.socketID2CodeMap[id].count >= 3) {
                    delete GD.socketID2CodeMap[id];
                    //刷新验证码 add by canaddy
                    self.onCodeAddrAuto(socket, data);
                }
                return;
            }


            //成功后就删除
            delete GD.socketID2CodeMap[id];

            var guestID = ttutil.getUUID();
            data.guestID = guestID;

            var sex = Math.random() < 0.5 ? 0 : 1;
            var faceID = Math.floor(Math.random() * 10);

            var gameID = GameIDGenerator.generatorGameID();
            if (gameID == null) {
                self.sendSocketData(socket, "VerifyUser", {errMsg: "生成游戏ID失败"});
                //刷新验证码 add by canaddy
                self.onCodeAddr(socket);
                return;
            }


            var val = [gameID, null, null, null, sex, faceID, data.ip, guestID, null, LoginType.GuestType, data.deviceID, data.os, data.channel, data.version];


            SqlPool.x_query("CALL ps_user_reg(?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?);", val, function (err, rows, fields) {
                if (err) {
                    ttutil.psSendSocketData(socket, "VerifyUser", {errCode: 3, errMsg: "游客登陆失败,请重新登陆"});
                    //刷新验证码 add by canaddy
                    self.onCodeAddrAuto(socket, data);
                    return;
                }

                if (rows && rows[0] && rows[0][0]) {
                    var retSet = rows[0][0];
                    if (retSet.msgid == 1) {
                        self.login(socket, data, data.guestID);
                        GD.last5minRegInfoArray.push(Date.now());
                    } else {
                        mpdebug("游客注册失败，原因" + retSet.msg);
                        ttutil.psSendSocketData(socket, "VerifyUser", {errCode: 3, errMsg: "游客登陆失败,请重新登陆"});
                        //刷新验证码 add by canaddy
                        self.onCodeAddrAuto(socket, data);
                    }
                }

            });

            return;
        } else {
            self.login(socket, data, data.guestID);
        }
    } else if (data.loginType == LoginType.QQType) {
        self.sendSocketData(socket, "VerifyUser", {errCode: 3, errMsg: "暂时不支持QQ登录"});
    }
    //微信登录
    else if (data.loginType == LoginType.WXType) {

        var code = data.wxCode;

        if (!code) {
            ttutil.psSendSocketData(socket, "VerifyUser", {errMsg: "获取微信code失败"});
            return;
        }

        //通过code去获取 access_token
        WXLoginUtil.onWXLogin(data.plaza, code, data.isWeb, function (errmsg, openID, unionID) {


            if (errmsg) {
                ttutil.psSendSocketData(socket, "VerifyUser", {errMsg: errmsg});

            }
            else {

                //判断是不是可以微信转账用的openID
                self.login(socket, data, openID, unionID, !data.isWeb ? openID : "");

            }
        })
    }
    else if (data.loginType == LoginType.AccountScanType) {

        var userID = data.userID;
        var selectSocketID = data.socketID;

        var isOK = false;

        for (var socketID in this.socketID2socketMap) {
            if (selectSocketID == socketID) {
                if (userID == this.socketID2socketMap[socketID].userID) {
                    isOK = true;
                    break;
                }
            }
        }

        if (isOK) {
            self.login(socket, data, data.userID, null);
        }
        else {
            ttutil.psSendSocketData(socket, "VerifyUser", {errMsg: "校验错误, 登录失败"});
        }


    }
    else {
        ttutil.psSendSocketData(socket, "VerifyUser", {errCode: 3, errMsg: "没用此登录方式"});
    }
};

//注册微信用户
p.regWXUser = function (socket, data, wxid, unionID) {

    var self = this;

    var sex = Math.random() < 0.5 ? 0 : 1;
    var faceID = Math.floor(Math.random() * 10);

    var gameID = GameIDGenerator.generatorGameID();
    if (gameID == null) {
        self.sendSocketData(socket, "VerifyUser", {errMsg: "生成游戏ID失败"});
        return;
    }


    var val = [gameID, null, null, null, sex, faceID, data.ip, wxid, unionID, LoginType.WXType, data.deviceID, data.os, data.channel, data.version];


    SqlPool.x_query("CALL ps_user_reg(?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?,?,?,?);", val, function (err, rows, fields) {
        if (err) {
            ttutil.psSendSocketData(socket, "VerifyUser", {errCode: 3, errMsg: "微信登陆失败,请重新登陆"});
            return;
        }

        if (rows && rows[0] && rows[0][0]) {
            var retSet = rows[0][0];
            if (retSet.msgid == 1) {
                mpinfo("微信注册后  重新登录");
                self.login(socket, data, wxid, unionID);
            } else {
                mpinfo("微信注册失败，原因" + retSet.msg);
                ttutil.psSendSocketData(socket, "VerifyUser", {errCode: 3, errMsg: "微信登陆失败,请重新登陆"})
            }
        }

    });
};

p.login = function (socket, data, otherID, unionID, openID) {
    //开始获取玩家信息
    var self = this;

    socket._036_channel = data.channel;      //渠道直接保存在socket里面

    //限制一下isp的长度
    if (data.isp && data.isp.length > 100) {
        data.isp = data.isp.substr(0, 100);
    }

    var ignoreMoorMachine = 1;  //是否忽略机器码
    if (data.loginType == LoginType.AccountType) {
        //账号登录不能忽略机器码
        if (GD.account2LastTryLoginTimeMap[data.account] != null) {

            //两次尝试时间 小于5秒， 做限制
            if (Date.now() - GD.account2LastTryLoginTimeMap[data.account] < 5000) {
                self.sendSocketData(socket, "VerifyUser", {
                    errMsg: "两次尝试登录时间小于5秒, 请稍候",
                    loginType: data.loginType
                });
                return;
            }
        }
        GD.account2LastTryLoginTimeMap[data.account] = Date.now();
        ignoreMoorMachine = 0;
        //有手机验证码., 判断下手机验证码是不是正确
        if (data.mobileCode) {
            var code = data.mobileCode;
            var socketID = ttutil.md5(socket.id);
            var codeObj = GD.mobileVerifyCodeServer.getCodeObj(socketID);

            if (!codeObj || codeObj.code != code) {
                self.sendSocketData(socket, "VerifyUser", {errMsg: "手机验证码错误"});

                if (codeObj) {
                    codeObj.count++;
                    if (codeObj.count > 3) {
                        GD.mobileVerifyCodeServer.deleteCodeObj(socketID);
                    }
                }

                return;
            }
            GD.mobileVerifyCodeServer.deleteCodeObj(socketID);
            //验证过了
            ignoreMoorMachine = 1;
        }
    }


    SqlPool.x_query('CALL ps_user_logon(?,?,?,?,?,?,?,?,?,?,?,?,?,?);', [data.account, data.password, data.moorMachine, data.isp, data.ip, otherID, unionID, data.loginType, data.os, ignoreMoorMachine, socket._036_channel, data.deviceID, data.version, openID], function (err, rows, fields) {
        if (err) {
            return;
        }

        if (rows) {
            if (rows[0] && rows[0][0] && rows[0][0].errCode) {
                var errCode = rows[0][0].errCode;
                //#1表示账号被机器锁定
                //#2表示账号密码错误

                if (errCode == 2 && data.loginType == LoginType.WXType) {
                    winston.info("微信注册");
                    self.regWXUser(socket, data, otherID, unionID);
                    return;
                }

                var phone = rows[0][0].phone;

                if (phone) {
                    phone = phone[0] + phone[1] + phone[2] + "****" + phone[7] + phone[8] + phone[9] + phone[10];
                }
                self.sendSocketData(socket, "VerifyUser", {
                    errCode: errCode,
                    errMsg: rows[0][0].errMsg,
                    loginType: data.loginType,
                    phone: phone,
                });
                //绑定socket信息
                socket.mobileNum = rows[0][0].phone;
                return;
            }

            //初始化踢人次数
            if (rows[1] && rows[1][0] && rows[0][0].memberOrder > 0) {
                //已经使用的踢人次数
                var tirenTimes = rows[1][0].tiren || 0;

                var vipConfig = GD.mainServer.getVipConfig(rows[0][0].memberOrder);
                var leftTirenTimes = vipConfig.tiren - tirenTimes;
                rows[0][0].vipTiren = leftTirenTimes;
            }
            else if (rows[0][0].memberOrder > 0) {      //表里没有记录,但vip不为0 就是原来次数
                var vipConfig = GD.mainServer.getVipConfig(rows[0][0].memberOrder);
                rows[0][0].vipTiren = vipConfig.tiren;
            } else {
                rows[0][0].vipTiren = 0;
            }

            var userInfo = rows[0][0];

            var user = self.onRealUserLogon(userInfo, socket, data.isp, data.ip, data.channel, data.version, data.deviceID);
            user.os = data.os;
            userInfo.uuid = user.uuid;
            userInfo.loginTime = user.loginTime.format("yyyy-MM-dd");

            userInfo.hasAccount = userInfo.account ? true : false;
            userInfo.hasWeiXin = userInfo.weixinID ? true : false;
            userInfo.hasQQ = userInfo.qqID ? true : false;
            userInfo.hasMobile = userInfo.phone ? true : false;
            userInfo.hasOpenID = userInfo.openID ? true : false;

            if (userInfo.hasMobile) {
                userInfo.phone = userInfo.phone[0] + userInfo.phone[1] + userInfo.phone[2] + "****" + userInfo.phone[7] + userInfo.phone[8] + userInfo.phone[9] + userInfo.phone[10];
            }


            delete userInfo.loginID;
            delete userInfo.account;
            delete userInfo.password;
            delete userInfo.isAndroid;
            delete userInfo.answer1;
            delete userInfo.answer2;
            delete userInfo.privilege;
            delete userInfo.weight;
            delete userInfo.enable;
            delete userInfo.friendOK;
            delete userInfo.realName;
            delete userInfo.idnum;
            delete userInfo.qqID;
            delete userInfo.weixinID;
            delete userInfo.openID;

            if (userInfo.twoPassword == null || userInfo.twoPassword.length < 40) {
                userInfo.twoPassword = false;
            }
            else {
                userInfo.twoPassword = true;
            }
            //把ip传下去
            // rows[0][0].ip = socket.handshake.address.substr(7);


            try {
                if (data.deviceID && data.deviceID != "00000000-0000-0000-0000-000000000000" && data.deviceID != "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF") {

                    if (GD.deviceID2SocketMap[data.deviceID] && GD.deviceID2SocketMap[data.deviceID] != socket) {

                        var msg = "您的设备已经登录其它账号,您已经被挤下线.";
                        //重复登陆,强制之前登陆账号下线
                        ttutil.psSendSocketData(GD.deviceID2SocketMap[data.deviceID], "KickOut", {msg: msg});

                        if (GD.realUserMap[GD.deviceID2SocketMap[data.deviceID].userID]) {
                            GD.realUserMap[GD.deviceID2SocketMap[data.deviceID].userID].logout();
                        }

                        GD.deviceID2SocketMap[data.deviceID].activeDisConnect = true;
                        GD.deviceID2SocketMap[data.deviceID].disconnect();


                    }
                    //绑定deviceID
                    socket.deviceID = data.deviceID;
                    GD.deviceID2SocketMap[data.deviceID] = socket;
                }
            }
            catch (e) {
                console.error(e);
            }

            self.sendSocketData(socket, "VerifyUser", userInfo);

            // 用户登录时验证其是否有已充值，但没验证过的凭证
            self.verifyReceipt();
        }
    });
};

// 用户登录时验证其是否有已充值，但没验证过的凭证
p.verifyReceipt = function () {
    SqlPool.x_query('CALL ps_read_pay_status(?)', [250], function (err, rows, fields) {
        if (err) {
            return;
        }

        if (rows && rows[0]) {
            for (var i = 0; i < rows[0].length; ++i) {
                (function (data) {
                    var orderID = data.payOrder;
                    var receipt = data.receipt;
                    var delay = 500 * i;

                    setTimeout(function () {
                        IAPUtil.verifyReceipt(orderID, receipt, 5000);
                    }, delay);
                })(rows[0][i]);
            }
        }
    });
};

//大厅读取游戏分类
p.onGameKind = function (socket, data) {
    var self = this;

    //操作数据库
    SqlPool.x_query('CALL ps_system_read_kind();', function (err, rows, fields) {
        if (err) {
            return;
        }
        if (rows && rows[0]) {
            self.sendSocketData(socket, "GameKind", rows[0]);
        } else {
            self.sendSocketData(socket, "GameKind", {errMsg: "没有游戏分类信息"});
        }
    });
};
//大厅读取游戏列表
p.onGameList = function (socket, data) {
    var self = this;

    var userID = socket.userID;
    if (userID == null || GD.realUserMap[userID] == null) return;

    //操作数据库
    SqlPool.x_query('CALL ps_system_read_module();', function (err, rows, fields) {
        if (err) {
            return;
        }

        if (rows && rows[0]) {
            var gameList = rows[0];
            GD.gameArray = gameList;
            GD.mainServer.updateGameList();
            self.sendSocketData(socket, "GameList", GD.gameArray || 0);
        }
        else {
            mperror("数据库游戏列表为空！");
        }
    });
};

//大厅读取游戏房间列表
p.onGameRoomList = function (socket, data) {
    var userID = socket.userID;
    var moduleID = data.GamesListID;

    var version = null;
    var isMobile = false;

    if (data.hasOwnProperty("mbVersion")) {
        version = data.mbVersion;
        isMobile = true;
    } else {
        version = data.pcVersion;
    }

    if (userID == null || GD.realUserMap[userID] == null) {
        mperror("用户不再服务器中");
        return;
    }

    var game = GD.gameMap[moduleID];
    GD.realUserMap[userID].moduleID = moduleID;

    if (game) {
        var roomMap = game.roomMap;
        var roomArray = [];

        for (var roomID in roomMap) {

            if (!roomMap[roomID]) continue;

            //先暂时这样
            var t = ttutil.deepCopy(roomMap[roomID].oldRoomInfo);

            t.userCount = roomMap[roomID].getRoomUserNumber();
            delete t.androidScoreBase;
            delete t.moduleID;
            delete t.presetPlayer;
            roomArray.push(t);
        }


        this.sendSocketData(socket, "GameRoomList", {room: roomArray});
    }
};


p.onGetMailListTip = function (socket, data) {
    var self = this;
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {
        SqlPool.x_query("call ps_user_isexist_unread_mail(?)", [userID], function (err, rows, fields) {
            if (err) {
                self.sendSocketData(socket, "GetMailListTip", {msgCount: 0});
                return;
            }

            if (rows && rows[0]) {
                self.sendSocketData(socket, "GetMailListTip", rows[0][0]);
            } else {
                self.sendSocketData(socket, "GetMailListTip", {msgCount: 0});
            }

        });
    }
};

/**
 * 读取邮件列表
 * @param socket
 * @param data
 */
p.onGetMailList = function (socket, data) {
    var self = this;
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {

        var start = (data && data.start) || 0;
        var size = (data && data.size) || 10000;
        //操作数据库
        SqlPool.x_query('CALL ps_user_read_system_mail(?,?,?,?);', [1, userID, start, size], function (err, rows, fields) {
            if (err) {
                return;
            }
            if (rows) {
                if (!rows[0]) {
                    self.sendSocketData(socket, "GetMailList", 0);
                    return;
                }
                //查询所有邮件
                self.sendSocketData(socket, "GetMailList", rows[0]);
            }
        });
    }
};
//读取系统邮件
p.onReadSystemMail = function (socket, data) {

    var self = this;
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {

        var mailID = data.mailID;

        if (mailID == null) {
            self.sendSocketData(socket, "ReadSystemMail", 0);
            return;
        }
        //操作数据库
        SqlPool.x_query('CALL ps_user_read_system_mail(?,?,?,?);', [2, mailID, null, null], function (err, rows, fields) {
            if (err) {
                return;
            }
            if (rows) {
                if (!rows[0]) {
                    self.sendSocketData(socket, "ReadSystemMail", 0);
                    return;
                }
                var mail = rows[0][0];
                mail.mailID = mailID;
                mail.attachmentArray = rows[1];
                self.sendSocketData(socket, "ReadSystemMail", mail);
            }
        });
    }

};
//修改玩家系统邮件状态
p.onWriteSystemMail = function (socket, data) {


    var self = this;
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {
        var user = GD.realUserMap[userID];

        var selectID = data.mailID || data.attachmentID || data.id;

        //3表示 标志为读取
        if (data.action == 3) {
            if (data.mailList == null) self.sendSocketData(socket, "WriteSystemMail", {errMsg: "参数错误"});

            for (var i = 0; i < data.mailList.length; ++i) {
                SqlPool.x_query('call ps_user_read_write_system_mail(?, ?, ?);', [userID, data.mailList[i], data.action], function (err, rows, fields) {
                });
            }

        }
        //1表示 标志为删除， 2表示  领取附件
        else if (data.action == 1 || data.action == 2) {//修改玩家系统邮件不可见
            //操作数据库
            SqlPool.x_query('CALL ps_user_read_write_system_mail(?,?,?);', [userID, selectID, data.action], function (err, rows, fields) {
                if (err) {
                    return;
                }
                if (rows) {
                    if (!rows[0] || !rows[0][0]) {
                        return;
                    }
                    if (rows[0][0].cardnum == null) {
                        if (rows[0][0].score != null) {
                            self.sendSocketData(socket, "WriteSystemMail", rows[0][0]);
                            user.setDBInfo(rows[0][0]);
                        }
                        else {
                            self.sendSocketData(socket, "WriteSystemMail", {errMsg: "操作失败"});
                        }
                    }
                    else {

                        mpinfo("已经废弃");
                    }
                }
            });
        }
    }


};


/**
 * 验证二级密码
 * @param socket
 * @param data
 */
p.onVerifyTwoPassword = function (socket, data) {

    var self = this;
    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {
        var user = GD.realUserMap[userID];

        var res = user.verifyTwoPassword(data.twoPassword);
        var msg = {};
        if (res == -2) {
            msg.errMsg = PlazaConst.TWO_PASSWORD_EMPTY;
        }
        else if (res == -1) {
            msg.errMsg = PlazaConst.TWO_PASSWORD_ERROR;
        }
        else {
            msg.success = true;
            verifyMng.setVeirifyBankPassword(userID, true);
        }
        self.sendSocketData(socket, "VerifyTwoPassword", msg);

    }

};

p.onGetRebate = function (socket, data) {
    var self = this;
    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {
        var ip = socket.handshake.address.substr(7);

        var user = GD.realUserMap[userID];

        if (user.roomID != null) {
            self.sendSocketData(socket, "GetRebate", {errMsg: "您正在游戏房间中，无权进行此操作"});
            return;
        }

        SqlPool.x_query('call ps_bank_get_rebate(?, ?)', [userID, ip], function (err, rows, fields) {
            if (err) {
                self.sendSocketData(socket, "GetRebate", {errMsg: "领取返利失败"});
                return;
            }

            if (rows && rows[0]) {
                if (rows[0][0].msgID == 2) {
                    self.sendSocketData(socket, "GetRebate", {errMsg: rows[0][0].errMsg});
                    return;
                }

                if (rows[0][0].msgID == 1) {
                    self.sendSocketData(socket, "GetRebate", {success: true, rebate: rows[1][0].rebate});
                    if (rows[1] && rows[1][0]) {
                        var msg = {
                            bankScore: rows[1][0].bankScore,
                            totalRebate: rows[1][0].totalRebate,
                            unRecvRebate: rows[1][0].unRecvRebate
                        };
                        self.sendSocketData(socket, "UserInfoUpdate", msg);
                        user.setDBInfo(msg);
                    }

                    //写邮件
                    var title = "[个人] -您已经领取返利金额" + rows[1][0].rebate + " 注意查收 ";
                    var content = "如题";

                    GD.plazaServer.writeUserMail(userID, title, content);
                }
            }
        });
    }
};

p.onTransferMoney = function (socket, data) {
    var self = this;
    var userID = socket.userID;

    //如果用户不在线 直接忽略
    if (userID != null && GD.realUserMap[userID]) {
        var toGameID = data.toGameID;
        var toUserID = GD.gameID2UserID[toGameID];

        var user = GD.realUserMap[userID];

        var ip = socket.handshake.address.substr(7);

        if (!verifyMng.isVerifyBankPassword(userID)) {
            self.sendSocketData(socket, "TransferMoney", {errMsg: "保险柜密码未经过验证，无权打赏"});
            return;
        }

        if (user.roomID != null) {
            self.sendSocketData(socket, "TransferMoney", {errMsg: "您正在游戏房间中，禁止打赏，请先退出房间"});
            return;
        }

        if (!data.money || data.money < 0) {
            self.sendSocketData(socket, "TransferMoney", {errMsg: "打赏金额有误"});
            return;
        }

        if (toGameID == null) {
            ttutil.psSendSocketData(socket, "TransferMoney", {errMsg: "打赏用户不能为空"});
            return;
        }

        if (toUserID == userID) {
            ttutil.psSendSocketData(socket, "TransferMoney", {errMsg: "不能自己转给自己"});
            return;
        }

        var toUserInfo = GD.cacheUserInfoMap[toUserID];

        if (toUserInfo == null) {
            ttutil.psSendSocketData(socket, "TransferMoney", {errMsg: "没有打赏用户的信息"});
            return;
        }

        var memberOrder = user.memberOrder || 0;

        var vipConfig = GD.mainServer.getVipConfig(memberOrder);

        if (data.money < vipConfig.transferMoneyCondition) {
            self.sendSocketData(socket, "TransferMoney", {errMsg: "您当前为VIP等级" + memberOrder + "，打赏金额必须大于" + vipConfig.transferMoneyCondition + "，具体规则可以点击大厅头像右边的VIP图标查看。"});
            return;
        }

        if (user.bankScore < data.money) {
            self.sendSocketData(socket, "TransferMoney", {errMsg: "你保险柜的钱不够"});
            return;
        }


        ///////////////////////////////////////////////////////////////////////////////////////

        if (GD.userID2LastBankerOperTimeMap[userID] != null) {

            //两次尝试时间 小于5秒， 做限制
            if (Date.now() - GD.userID2LastBankerOperTimeMap[userID] < 5000) {
                self.sendSocketData(socket, "TransferMoney", {
                    errMsg: "请慢点",
                });
                return;
            }
        }
        GD.userID2LastBankerOperTimeMap[userID] = Date.now();
        ///////////////////////////////////////////////////////////////////////////////////////

        var tax = 0;
        var rebate = 0;

        var toUserInfoVipInfo = GD.mainServer.getVipConfig(toUserInfo.memberOrder || 0);
        //税收是扣被打赏人的， 所以要判断被打赏人的vip等级
        if (toUserInfo.memberOrder != Config.BusinessMan) {     //打赏对象不是商人是就得计算返利和税收了， 普通人返利0

            var totalTax = Math.round(toUserInfoVipInfo.tax * data.money);      //总税收
            rebate = Math.round(toUserInfoVipInfo.rebate * data.money);     //返利
            tax = totalTax - rebate;
            tax = tax > 0 ? tax : 0;
        }

        SqlPool.x_query("call ps_bank_transferMoney(?, ?, ?, ?, ?, ?,?)", [userID, toUserID, data.money, tax, rebate, ip, socket._036_channel], function (err, rows, fields) {
            if (err) {
                self.sendSocketData(socket, "TransferMoney", {errMsg: "打赏失败"});
                return;
            }

            if (rows && rows[0]) {

                if (rows[0][0].msgID == 2) {
                    self.sendSocketData(socket, "TransferMoney", {errMsg: rows[0][0].errMsg});
                    return;
                }

                if (rows[1] && rows[1][0]) {
                    user.setDBInfo(rows[1][0]);
                    rows[1][0].success = true;
                    self.sendSocketData(socket, "TransferMoney", rows[1][0]);

                    var toUser = GD.realUserMap[toUserID];
                    var type = toUser ? 1 : 2;
                    //写邮件
                    var title = "[交互] -[" + user.nickname + "/" + user.gameID + "] 向您打赏 " + data.money + " 游戏币(含税收 " + (tax + rebate) + "),注意查收";
                    var content = "如题";
                    GD.plazaServer.writeUserMail(toUserID, title, content);

                    //更新对方用户
                    if (rows[2] && rows[2][0] && toUser) {
                        toUser.setDBInfo(rows[2][0]);
                        self.sendSocketData(toUser.socket, "UserInfoUpdate", {bankScore: toUser.bankScore});
                    }

                    var toUserInfo = GD.cacheUserInfoMap[toUserID];

                    //写邮件
                    var selfTitle = "[交互] -您向[" + toUserInfo.nickname + "/" + toUserInfo.gameID + "]打赏 " + data.money + "游戏币";

                    if (rebate > 0) {
                        selfTitle += " 返利" + rebate + " 游戏币， 注意查收";
                    }

                    var selfContent = "如题";
                    GD.plazaServer.writeUserMail(userID, selfTitle, selfContent);

                } else {
                    self.sendSocketData(socket, "TransferMoney", {errMsg: "打赏失败"});
                }

                return;
            }
        });
    }
};

p.onQueryBusiness = function (socket, data) {
    var self = this;
    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {
        if (data.type < 100 && verifyMng.isVerifyBankPassword(userID) == false) {
            self.sendSocketData(socket, "QueryBusiness", {errMsg: "您未验证过保险柜密码，无权操作", type: data.type});
            return;
        }

        if (GD.realUserMap[userID].roomID != null) {
            self.sendSocketData(socket, "QueryBusiness", {errMsg: "您正在游戏房间中，无权操作", type: data.type});
            return;
        }

        switch (data.type) {
            //明细查询
            case 1:
                SqlPool.x_query("call ps_bank_money_detail(?, ?, ?)", [userID, data.crtID, Config.QueryBankCount], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "QueryBusiness", {errMsg: "查询失败请重试", type: data.type});
                        return;
                    }

                    if (rows && rows[0]) {
                        for (var i = 0; i < rows[0].length; i++) {
                            rows[0][i].ts = rows[0][i].ts.getTime();
                        }

                        self.sendSocketData(socket, "QueryBusiness", {success: true, data: rows[0], type: data.type});
                    }
                });
                break;

            case 2:
                SqlPool.x_query("call ps_bank_transfer_detail(?, ?, ?, ?)", [userID, Config.TransferDetailDay, data.crtID, Config.QueryBankCount], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "QueryBusiness", {errMsg: "查询失败请重试", type: data.type});
                        return;
                    }

                    if (rows && rows[0]) {
                        var msg = {success: true, type: data.type};

                        var detail = [];

                        for (var i = 0; i < rows[0].length; i++) {
                            var item = {};
                            item.ts = rows[0][i].ts.getTime();
                            item.id = rows[0][i].id;
                            item.detailID = rows[0][i].detailID;
                            item.tax = rows[0][i].tax;

                            var fromUserInfo = GD.cacheUserInfoMap[rows[0][i].fromUserID];
                            var toUserInfo = GD.cacheUserInfoMap[rows[0][i].toUserID];

                            //没用缓存信息
                            if (!fromUserInfo || !toUserInfo) {
                                mpinfo("用户缓存信息为null");
                                continue;
                            }

                            if (rows[0][i].money > 0) {
                                item.fromGameID = toUserInfo.gameID;
                                item.fromNickname = toUserInfo.nickname;
                                item.toGameID = fromUserInfo.gameID;
                                item.toNickname = fromUserInfo.nickname;
                            } else if (rows[0][i].money < 0) {
                                item.fromGameID = fromUserInfo.gameID;
                                item.fromNickname = fromUserInfo.nickname;
                                item.toGameID = toUserInfo.gameID;
                                item.toNickname = toUserInfo.nickname;
                            }

                            item.money = Math.abs(rows[0][i].money);

                            detail.push(item);
                        }

                        msg.data = detail;
                        msg.income = 0;
                        msg.outgo = 0;
                        msg.days = Config.TransferDetailDay;

                        if (rows[1] && rows[1][0]) {
                            msg.income = Math.abs(rows[1][0].income);
                        }

                        if (rows[2] && rows[2][0]) {
                            msg.outgo = Math.abs(rows[2][0].outgo);
                        }

                        self.sendSocketData(socket, "QueryBusiness", msg);
                    }
                });
                break;
            //查询用户信息
            case 3:
                SqlPool.x_query("select * from ps_account where gameID=" + data.gameID, function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "QueryBusiness", {errMsg: "查询失败请重试", type: data.type});
                        return;
                    }
                    if (rows && rows[0]) {
                        self.sendSocketData(socket, "QueryBusiness", {
                            success: true,
                            info: rows[0].nickname,
                            type: data.type
                        });
                    }
                    else {
                        self.sendSocketData(socket, "QueryBusiness", {success: true, info: "用户不存在", type: data.type});
                    }
                });
                break;

            //红包明细查询
            case 4:
                SqlPool.x_query("call ps_lucky_rmb_detail(?, ?, ?)", [userID, data.crtID, Config.QueryBankCount], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "QueryBusiness", {errMsg: "查询失败请重试", type: data.type});
                        return;
                    }

                    if (rows && rows[0]) {
                        for (var i = 0; i < rows[0].length; i++) {
                            rows[0][i].ts = rows[0][i].ts.getTime();
                        }

                        self.sendSocketData(socket, "QueryBusiness", {success: true, data: rows[0], type: data.type});
                    }
                });
                break;

            //实物订单查询
            case 101:
                SqlPool.x_query("call ps_user_goods_order(?, ?, ?)", [data.startIndex, data.limitSize, userID], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "QueryBusiness", {errMsg: "查询失败请重试", type: data.type});
                        return;
                    }

                    if (rows && rows[0]) {
                        for (var i = 0; i < rows[0].length; i++) {
                            rows[0][i].createTime = rows[0][i].createTime.getTime();
                            rows[0][i].updateTime = rows[0][i].updateTime.getTime();
                        }

                        self.sendSocketData(socket, "QueryBusiness", {success: true, data: rows[0], type: data.type});
                    }
                });
                break;

            //摊位明细查询
            case 102:
                SqlPool.x_query("call ps_user_store_trade_detail(?, ?, ?)", [userID, data.index, data.size], (err, rows, fields) => {
                    if (err) {
                        self.sendSocketData(socket, "QueryBusiness", {errMsg: "查询失败请重试", type: data.type});
                        return;
                    }

                    if (rows && rows[0]) {
                        for (var i = 0; i < rows[0].length; i++) {
                            var info = rows[0][i];
                            info.ts = info.ts.getTime();
                            info.nickname = GD.cacheUserInfoMap[info.userID].nickname;
                        }

                        self.sendSocketData(socket, "QueryBusiness", {success: true, data: rows[0], type: data.type});
                    }
                });
                break;

            //身上金币明细查询
            case 103:
                SqlPool.x_query("call ps_user_coin_detail(?, ?, ?)", [userID, data.index, data.size], (err, rows, fields) => {
                    if (err) {
                        self.sendSocketData(socket, "QueryBusiness", {errMsg: "查询失败请重试", type: data.type});
                        return;
                    }

                    if (rows && rows[0]) {
                        for (var i = 0; i < rows[0].length; i++) {
                            var info = rows[0][i];
                            info.ts = info.ts.getTime();
                        }

                        self.sendSocketData(socket, "QueryBusiness", {success: true, data: rows[0], type: data.type});
                    }
                });
                break;

            default :
                self.sendSocketData(socket, "QueryBusiness", {errMsg: "非法参数", type: data.type});
                return;
        }
    }
};

//保险柜业务，玩家存取出钻石兑换
p.onBankBusiness = function (socket, data) {


    var self = this;
    var userID = socket.userID;

    if (userID != null && GD.realUserMap[userID]) {

        var user = GD.realUserMap[userID];

        if (user.roomID != null) {
            self.sendSocketData(socket, "BankBusiness", {errMsg: "您在游戏房间中，禁止存取操作，请先退出房间", type: data.type});
            return;
        }

        var ip = GD.realUserMap[userID].loginIP;

        if (verifyMng.isVerifyBankPassword(userID) == false) {
            self.sendSocketData(socket, "BankBusiness", {errMsg: "您未验证过保险柜密码，无权操作", type: data.type});
            return;
        }


        data.money = Number(data.money);
        if (isNaN(data.money) || data.money <= 0 || data.money % 1 != 0) {
            self.sendSocketData(socket, "BankBusiness", {errMsg: "操作金额不合法", type: data.type});
            return;
        }


        //先判断一下， 有不合法， 直接return

        switch (data.type) {
            //把钱从身上存到保险柜中去
            case 1:
                if (user.score < data.money) {
                    self.sendSocketData(socket, "BankBusiness", {errMsg: "您身上钱不够", type: data.type});
                    return;
                }

                data.description = '存入保险柜';

                break;
            //把钱从保险柜取到身上来
            case 2:
                if (user.bankScore < data.money) {
                    self.sendSocketData(socket, "BankBusiness", {errMsg: "您保险柜中钱不够", type: data.type});
                    return;
                }

                data.description = '从保险柜取出';
                break;
            //钻石兑换金豆
            case 3:
                if (user.acer < data.money) {
                    self.sendSocketData(socket, "BankBusiness", {errMsg: "您身上的钻石不够", type: data.type});
                    return;
                }

                data.description = '钻石兑换';
                break;
            //把钻石从身上存到保险柜中去
            case 4:
                if (user.acer < data.money) {
                    self.sendSocketData(socket, "BankBusiness", {errMsg: "您身上的钻石不够", type: data.type});
                    return;
                }

                data.description = '钻石存入保险柜';
                break;
            //把钻石从保险柜取到身上来
            case 5:
                if (user.bankAcer < data.money) {
                    self.sendSocketData(socket, "BankBusiness", {errMsg: "您保险柜中的钻石不够", type: data.type});
                    return;
                }

                data.description = '从保险柜取出保险柜';
                break;

            //
            case 7: //把红包兑换成红包道具

                if (user.luckyRMB < data.money) {
                    self.sendSocketData(socket, "BankBusiness", {errMsg: "您身上红包额度不足", type: data.type});
                    return;
                }

                data.description = '红包兑换红包道具';
                break;

            case 8://把红包道具兑换成红包

                var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);
                if (!goodsSet || goodsSet.getGoodsCount(GoodsConst.RedPacket) < data.money) {
                    ttutil.psSendSocketData(socket, "BankBusiness", {errMsg: "您红包道具不够， 操作失败", type: data.type});
                    return;
                }
                data.description = '红包道具兑换红包';
                break;
            default :
                self.sendSocketData(socket, "BankBusiness", {errMsg: "非法参数", type: data.type});
                return;
        }

        ///////////////////////////////////////////////////////////////////////////////////////

        if (GD.userID2LastBankerOperTimeMap[userID] != null) {

            //两次尝试时间 小于5秒， 做限制
            if (Date.now() - GD.userID2LastBankerOperTimeMap[userID] < 5000) {
                self.sendSocketData(socket, "BankBusiness", {
                    errMsg: "请慢点",
                });
                return;
            }
        }
        GD.userID2LastBankerOperTimeMap[userID] = Date.now();
        ///////////////////////////////////////////////////////////////////////////////////////

        //这里面会在进行一层判断， 保证不为负数， 因为有可能数据库还没处理完， 请求又过来了， 所以上面的判断有可能会出错。
        //开始操作数据库
        SqlPool.x_query('CALL ps_bank_business(?,?,?,?, ?,?,?);', [userID, data.money, data.type, ip, data.description, socket._036_channel, GoodsConst.RedPacket], function (err, rows, fields) {
            if (err) {
                self.sendSocketData(socket, "BankBusiness", {errMsg: "操作失败", type: data.type});
                return;
            }
            if (rows && rows[0] && rows[0][0]) {

                if (rows[0][0].t_err != 0) {
                    self.sendSocketData(socket, "BankBusiness", {errMsg: "操作失败", type: data.type});
                    return;
                }

                //如果是红包兑换业务
                if (data.type == 7 || data.type == 8) {
                    var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);
                    var redPacketNum = rows[2][0].redPacketNum;
                    //更新身上道具
                    goodsSet.updateGoods(GoodsConst.RedPacket, redPacketNum - goodsSet.getGoodsCount(GoodsConst.RedPacket));
                    ttutil.psSendSocketData(socket, "UpdateGoods", {
                        goodsID: GoodsConst.RedPacket,
                        count: redPacketNum
                    });
                }

                user.setDBInfo(rows[1][0]);
                rows[1][0].type = data.type;
                rows[1][0].success = true;
                self.sendSocketData(socket, "BankBusiness", rows[1][0]);

            } else {
                self.sendSocketData(socket, "BankBusiness", {errMsg: "操作失败", type: data.type});
            }
        });
    }
};

/**
 * 读取vip信息
 * @param socket
 * @param data
 */
p.onReadVipConfig = function (socket, data) {

    var self = this;
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {

        self.sendSocketData(socket, "ReadVipConfig", GD.vipConfigArray);
    }

};


//修改玩家个人信息
p.onModifySetup = function (socket, data) {

    var self = this;
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID] && data) {

        var user = GD.realUserMap[userID];

        switch (data.action) {
            case 1://修改个人信息
                //开始操作数据库
                if (GD.mainServer.isLegitimateNickName(data.nickname)) {
                    data.address = data.address || "";
                    data.underWrite = data.underWrite || "";

                    if (data.faceID == undefined)
                        data.faceID = user.faceID;

                    if (data.gender == undefined)
                        data.gender = user.gender;

                    SqlPool.x_query('call ps_user_modify_basicInfo(?, ?, ?, ?, ?, ?)', [userID, data.nickname, data.address, data.underWrite, data.faceID, data.gender], function (err, rows, fields) {
                        if (err) {
                            self.sendSocketData(socket, 'ModifySetup', {errMsg: "个人信息修改失败", action: data.action});
                            return;
                        }

                        if (rows && rows[0]) {
                            if (rows[0][0].msgID == 2) {
                                self.sendSocketData(socket, 'ModifySetup', {
                                    errMsg: rows[0][0].errMsg,
                                    action: data.action
                                });
                            } else if (rows[0][0].msgID == 1) {

                                if (user.nickname !== data.nickname) {
                                    var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);
                                    goodsSet.updateGoods(GoodsConst.RenameCard, -1);
                                    var count = goodsSet.getGoodsCount(GoodsConst.RenameCard);
                                    self.sendSocketData(socket, "UpdateGoods", {
                                        goodsID: GoodsConst.RenameCard,
                                        count: count
                                    });
                                }

                                self.sendSocketData(socket, 'ModifySetup', {success: true, action: data.action});
                                user.setDBInfo({
                                    nickname: data.nickname,
                                    address: data.address,
                                    faceID: data.faceID,
                                    gender: data.gender,
                                    underWrite: data.underWrite
                                });
                            }

                        } else {
                            self.sendSocketData(socket, "ModifySetup", {errMsg: '个人信息修改失败', action: data.action});
                        }

                    });
                } else {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "昵称不合法。", action: data.action});
                }
                break;

            case 2://修改登陆密码

                //参数错误
                if (!data.oldPassword || !data.newPassword || data.oldPassword.length != 40 || data.newPassword.length != 40) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "参数错误", action: data.action});
                    return;
                }

                if (user.password != data.oldPassword) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "旧密码错误", action: data.action});
                    return;
                }

                if (user.twoPassword == data.newPassword) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "登录密码不能与保险柜密码一样", action: data.action});
                    return;
                }

                if (user.password == data.newPassword) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "与旧密码相同", action: data.action});
                    return;
                }

                //  1 修改密码   2 找回密码 直接设定新密码
                SqlPool.x_query('CALL ps_user_modify_password(?,?,?,?);', [userID, data.oldPassword, data.newPassword, 1], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "ModifySetup", {errMsg: "修改失败", action: data.action});
                        return;
                    }
                    if (rows && rows[0]) {
                        if (rows[0][0].msgID == 1) {
                            self.sendSocketData(socket, "ModifySetup", {success: true, action: data.action});
                            user.setDBInfo({password: data.newPassword});
                            return;
                        } else if (rows[0][0].msgID == 2) {
                            self.sendSocketData(socket, "ModifySetup", {
                                errMsg: rows[0][0].errMsg,
                                action: data.action
                            });
                            return;
                        }
                    }
                });

                break;

            case 3://设定二级密码

                if (!data.twoPassword) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "保险柜密码不能为空", action: data.action});
                    return;
                }

                if (data.twoPassword == user.password) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "保险柜密码不能与登录密码一样", action: data.action});
                    return;
                }

                if (data.twoPassword.length != 40) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "设定保险柜密码参数错误", action: data.action});
                    return;
                }

                //开始操作数据库
                SqlPool.x_query('CALL ps_user_modify_twoPassword(?,?,?,?,?);', [userID, null, data.twoPassword, 2, data.passwordType], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "ModifySetup", {
                            errMsg: "设定保险柜密码失败。",
                            action: data.action
                        });
                        return;
                    }
                    if (rows && rows[0]) {
                        if (rows[0][0].msgID == 1) {
                            self.sendSocketData(socket, "ModifySetup", {success: true, action: data.action});
                            user.setDBInfo({twoPassword: data.twoPassword, twoPasswordType: data.twoPasswordType});
                            //验证通过
                            verifyMng.setVeirifyBankPassword(userID, true);
                            return;
                        } else if (rows[0][0].msgID = 2) {
                            self.sendSocketData(socket, "ModifySetup", {
                                errMsg: rows[0][0].errMsg,
                                action: data.action
                            });
                            return;
                        }
                    }
                });

                break;

            case 6://修改二级密码

                if (!data.oldTwoPassword || !data.newTwoPassword || data.oldTwoPassword.length != 40 || data.newTwoPassword.length != 40) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "参数错误", action: data.action});
                    return;
                }

                if (data.oldTwoPassword != user.twoPassword) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "旧保险柜密码错误", action: data.action});
                    return;
                }

                if (data.newTwoPassword == user.password) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "保险柜密码不能与登录密码一样", action: data.action});
                    return;
                }

                if (user.twoPassword == data.newTwoPassword) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "新保险柜密码与旧的保险柜密码相同", action: data.action});
                }

                SqlPool.x_query('CALL ps_user_modify_twoPassword(?,?,?,?,?);', [userID, data.oldTwoPassword, data.newTwoPassword, 1, data.passwordType], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "ModifySetup", {
                            errMsg: "修改保险柜密码失败",
                            action: data.action
                        });
                        return;
                    }

                    if (rows && rows[0]) {
                        if (rows[0][0].msgID == 1) {
                            self.sendSocketData(socket, "ModifySetup", {success: true, action: data.action});
                            user.setDBInfo({twoPassword: data.newTwoPassword});
                            return;
                        } else if (rows[0][0].msgID == 2) {
                            self.sendSocketData(socket, "ModifySetup", {
                                errMsg: rows[0][0].errMsg,
                                action: data.action
                            });
                            return;
                        }
                    }
                });

                break;

            case 8://设定手机绑定

                if (!data.mobileNum || !ttutil.isMobileNum(data.mobileNum)) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "手机号码错误。", action: data.action});
                    return;
                }


                var code = data.code;
                var socketID = ttutil.md5(socket.id);
                var codeObj = GD.mobileVerifyCodeServer.getCodeObj(socketID);

                if (!codeObj) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "验证码错误", action: data.action});
                    return;
                }
                if (codeObj.mobileNum != data.mobileNum) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "手机号码不正确", action: data.action});
                    return;
                }
                if (codeObj.code != code) {

                    self.sendSocketData(socket, "ModifySetup", {errMsg: "验证码错误", action: data.action});//
                    codeObj.count++;
                    if (codeObj.count > 3) {
                        GD.mobileVerifyCodeServer.deleteCodeObj(socketID);
                    }
                    return;
                }

                if (user.phone) {
                    self.sendSocketData(socket, "ModifySetup", {errMsg: "已经绑定手机号了", action: data.action});//
                    return;
                }

                SqlPool.x_query('CALL ps_user_realname_verification(?,?,?,?,?,?);', [userID, data.mobileNum, data.idcard.toUpperCase(), data.realname, 1, null], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "ModifySetup", {
                            errMsg: "手机绑定失败，请稍后重试。",
                            action: data.action
                        });
                        return;
                    }

                    var reward = rows && rows[0] && rows[0][0] && rows[0][0].reward;

                    if (reward == null) {
                        var verification = null;
                        if (data.idcard)
                            verification = JuheHelper.phoneNameIDCardVerification(data.realname, data.mobileNum, data.idcard);
                        else
                            verification = JuheHelper.phoneNameVerification(data.realname, data.mobileNum);

                        verification.then(obj => {
                            let juhe_data = obj && obj.data && JSON.parse(obj.data);

                            let res = 0;
                            if (juhe_data && juhe_data.result && juhe_data.result.res == 1)
                                res = 1;

                            SqlPool.x_query('CALL ps_user_realname_verification(?,?,?,?,?,?);', [userID, data.mobileNum, data.idcard, data.realname, 0, res], function (err, rows, fields) {
                                let resultInfo = rows && rows[0] && rows[0][0] && rows[0][0];

                                if (err || resultInfo == null) {
                                    self.sendSocketData(socket, "ModifySetup", {
                                        errMsg: "手机绑定失败，请稍后重试。",
                                        action: data.action
                                    });
                                    return;
                                }

                                if (resultInfo.reward < 0) {
                                    var title = `[实名] - 实名认证失败扣除手续费${-resultInfo.reward.toFixed(2)}金币`;
                                    GD.plazaServer.writeUserMail(userID, title);

                                    user.setDBInfo({
                                        luckyRMB: user.luckyRMB + resultInfo.reward
                                    });

                                    self.sendSocketData(socket, "ModifySetup", {
                                        errMsg: title,
                                        action: data.action
                                    });
                                }
                                else {
                                    var title;
                                    if (resultInfo.reward > 0) {
                                        title = `[实名] - 首次实名认证成功奖励${resultInfo.reward.toFixed(2)}元现金红包, 请前往保险柜查收`;
                                        GD.plazaServer.writeUserMail(userID, title);
                                    }
                                    else {
                                        if (data.idcard)
                                            title = `非首次实名认证成功不奖励`;
                                        else
                                            title = `绑定手机成功`;
                                    }

                                    user.setDBInfo({
                                        phone: data.mobileNum,
                                        realname: data.realname,
                                        luckyRMB: user.luckyRMB + resultInfo.reward
                                    });

                                    self.sendSocketData(socket, "ModifySetup", {
                                        successMsg: title,
                                        success: true,
                                        action: data.action,
                                        phone: data.mobileNum,
                                        realname: data.realname
                                    });
                                }

                                self.sendSocketData(socket, "UserInfoUpdate", {luckyRMB: user.luckyRMB});

                                if (resultInfo.recommendUserID && resultInfo.recommendRewardRMB > 0) {
                                    let recommendUserInfo = GD.realUserMap[resultInfo.recommendUserID];

                                    //推荐人在线
                                    if (recommendUserInfo != null) {
                                        recommendUserInfo.setDBInfo({luckyRMB: recommendUserInfo.luckyRMB + resultInfo.recommendRewardRMB});
                                        self.sendSocketData(recommendUserInfo.socket, "UserInfoUpdate", {luckyRMB: recommendUserInfo.luckyRMB});
                                    }

                                    //写邮件
                                    var title = "[活动]-您在有奖推广活动中成功推荐【" + resultInfo.subscribe_nickname + "】获得" + resultInfo.recommendRewardRMB.toFixed(2) + "元红包, 请前往保险柜查收";
                                    GD.plazaServer.writeUserMail(resultInfo.recommendUserID, title);
                                }

                                if (resultInfo.recommend2UserID && resultInfo.recommend2RewardRMB > 0) {
                                    let recommend2UserInfo = GD.realUserMap[resultInfo.recommend2UserID];

                                    //推荐人在线
                                    if (recommend2UserInfo != null) {
                                        recommend2UserInfo.setDBInfo({luckyRMB: recommend2UserInfo.luckyRMB + resultInfo.recommend2RewardRMB});
                                        self.sendSocketData(recommend2UserInfo.socket, "UserInfoUpdate", {luckyRMB: recommend2UserInfo.luckyRMB});
                                    }

                                    //写邮件
                                    var title = "[活动]-您的推荐人在有奖推广活动中成功推荐【" + resultInfo.subscribe_nickname + "】，您获得" + resultInfo.recommend2RewardRMB.toFixed(2) + "元红包, 请前往保险柜查收";
                                    GD.plazaServer.writeUserMail(resultInfo.recommend2UserID, title);
                                }
                            });
                        });
                    }
                    else if (reward >= 0) {
                        user.setDBInfo({phone: data.mobileNum, realname: data.realname});
                        self.sendSocketData(socket, "ModifySetup", {
                            success: true,
                            action: data.action,
                            phone: data.mobileNum,
                            realname: data.realname
                        });
                    }
                    else {
                        self.sendSocketData(socket, "ModifySetup", {
                            errMsg: "实名认证失败，本次不扣除手续费。",
                            action: data.action
                        });
                    }

                    GD.mobileVerifyCodeServer.deleteCodeObj(socketID);
                });
                break;

            case 9:     //解绑手机
                if (user.realname) {
                    self.sendSocketData(socket, "ModifySetup", {
                        errMsg: "现已支持相同手机号码绑定多个账号，实名认证不再提供解绑服务。",
                        action: data.action
                    });
                    return;
                }

                SqlPool.x_query('CALL ps_user_realname_verification(?,?,?,?,?,?);', [userID, null, null, null, null, null], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "ModifySetup", {
                            errMsg: "手机解绑失败，请稍后重试。",
                            action: data.action
                        });
                        return;
                    }

                    user.setDBInfo({phone: null});
                    self.sendSocketData(socket, "ModifySetup", {
                        success: true,
                        action: data.action,
                        phone: null
                    });
                });
                break;
        }
    }
};
/**
 * 获取道具配置
 */
p.getGoodsConfig = function () {
    var goodsConfigArray = [];

    for (var id in GD.goodsMap) {
        goodsConfigArray.push(GD.goodsMap[id]);
    }

    return goodsConfigArray;
};


//接受玩家喇叭道具喊话信息
p.onLabaAnnouncement = function (socket, data) {

    var self = this;
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID] && data != null) {
        //操作数据库
        GD.realUserMap[userID].onUseItem(data);
    }
};

p.onGetScoreRank = function (socket, data) {
    ttutil.psSendSocketData(socket, "GetScoreRank", {rankData: GD.userRank.getTopN(Config.RankCount)});
};

p.onReadPayConfig = function (socket, data) {
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {
        this.sendSocketData(socket, "ReadPayConfig", GD.payConfigArray);
    }
};

p.onBindAccount = function (socket, data) {
    var self = this;
    var userID = socket.userID;

    mpdebug("UserID: " + userID);

    if (userID != null && GD.realUserMap[userID] && data != null) {

        var user = GD.realUserMap[userID];

        //type 1表示绑定账号， 2表示绑定微信，-2 表示解绑微信
        if (data.type == 1) {
            if (data.account == null) {
                self.sendSocketData(socket, "BindAccount", {errMsg: "账号不能为空"});
            }

            if (data.nickname == null) {
                self.sendSocketData(socket, "BindAccount", {errMsg: "昵称不能为空"});
            }

            if (data.password == null) {
                self.sendSocketData(socket, "BindAccount", {errMsg: "密码不能为空"});
            }

            SqlPool.x_query('call ps_user_bind_account(?, ?, ?, ?,?,?,?,?)', [data.type, userID, data.account, data.nickname, data.password, null, null, null], function (err, rows, fields) {
                if (err) {
                    return;
                }

                if (rows && rows[0]) {
                    if (rows[0][0].msgID == 1) {
                        self.sendSocketData(socket, "BindAccount", {success: true, type: data.type});
                        //更新信息
                        user.setDBInfo({account: data.account, nickname: data.nickname, password: data.password});

                    } else if (rows[0][0].msgID == 2) {
                        self.sendSocketData(socket, "BindAccount", {errMsg: rows[0][0].errMsg});
                    }
                }

            });
        }
        //绑定微信
        else if (data.type == 2) {

            WXLoginUtil.onWXLogin(data.plaza, data.code, data.isWeb, function (err, openID, unionID) {

                if (err) {
                    mperror(err);
                    self.sendSocketData(socket, "BindAccount", {errMsg: "绑定微信失败"});
                    return
                }

                SqlPool.x_query('call ps_user_bind_account(?, ?, ?, ?,?, ?,?,?)', [data.type, userID, null, null, null, openID, unionID, !data.isWeb ? openID : ""], function (err, rows, fields) {
                    if (err) {
                        self.sendSocketData(socket, "BindAccount", {errMsg: "绑定微信失败"});
                        return;
                    }

                    if (rows && rows[0]) {
                        if (rows[0][0].msgID == 1) {
                            self.sendSocketData(socket, "BindAccount", {success: true, type: data.type});

                        } else if (rows[0][0].msgID == 2) {
                            self.sendSocketData(socket, "BindAccount", {errMsg: rows[0][0].errMsg});
                        }
                    }
                });

            })
        }
        //解绑微信
        else if (data.type == -2) {

            SqlPool.x_query('call ps_user_bind_account(?, ?, ?, ?,?, ?,?,?)', [data.type, userID, null, null, null, null, null, null], function (err, rows, fields) {
                if (err) {
                    self.sendSocketData(socket, "BindAccount", {errMsg: "解绑微信失败"});
                    return;
                }

                if (rows && rows[0]) {
                    if (rows[0][0].msgID == 1) {
                        self.sendSocketData(socket, "BindAccount", {success: true, type: data.type});

                    } else if (rows[0][0].msgID == 2) {
                        self.sendSocketData(socket, "BindAccount", {errMsg: rows[0][0].errMsg});
                    }
                }
            });
        }


    }
};

p.onGetRandomName = function (socket, data) {
    this.getRandomName(socket, data, 0);
};

p.getRandomName = function (socket, data, times) {
    var name = NameGenerator.generator();

    if (times > 3) {
        ttutil.psSendSocketData(socket, "GetRandomName", {errMsg: "获取随机昵称失败，请手动填写昵称，或者再一次请求昵称"})
    }

    var self = this;

    SqlPool.x_query('call ps_user_reg_verify(?, ?)', [name, 2], function (err, rows, fields) {
        if (err) {
            //如果错误再一次请求
            self.getRandomName(socket, data, times + 1);
            return;
        }
        if (rows) {
            if (rows[0]) {
                if (rows[0][0].useCount == 0) ttutil.psSendSocketData(socket, "GetRandomName", {nickname: name});//可以注册
                if (rows[0][0].useCount >= 1) self.getRandomName(socket, data, times + 1);//不可以注册
            }
        }
    });
};

/**
 * 客户端请求系统公告
 * @param socket
 * @param data
 */
p.onSystemNotice = function (socket, data) {
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {
        SqlPool.x_query('call ps_announcement_read(?)', new Date(), function (err, rows) {
            if (err) {
                return;
            }
            if (rows && rows[0]) {
                var systemNoticeArray = rows[0];
                ttutil.psSendSocketData(socket, "SystemNotice", systemNoticeArray);
            }
        })
    }
};

p.onEnterPlazaMain = function (socket, data) {
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {
        //GD.mainServer.broadcastSystem.broadcastUser(GD.realUserMap[userID].socket);

        //进入大厅时， 把分享配置表也下发下去
        ttutil.psSendSocketData(socket, "ReadShareConfig", GD.shareConfigMap);


    }
};

/**
 * 获取道具配置
 * @param socket
 * @param data
 */
p.onGetGoodsConfig = function (socket, data) {
    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {
        ttutil.psSendSocketData(socket, "GetGoodsConfig", {goodsConfig: this.getGoodsConfig()})
    }
};

p.onGetGoods = function (socket, data) {
    var userID = socket.userID;
    if (GD.realUserMap[userID]) {
        //获取道具数组
        var goodsArray = GD.goodsSystem.getUserGoodsSet(userID).getGoodsArray();
        ttutil.psSendSocketData(socket, "GetGoods", goodsArray);
    }
};

/**
 * 开启每日签到
 * @param socket
 */
p.onOpenDailyAttendance = function (socket, data) {
    var userID = socket.userID;

    if (GD.realUserMap[userID] === null) {
        return;
    }

    SqlPool.x_query("select day(ts), prizeType from ps_user_daily_attendance where userID=? and year(ts)=DATE_FORMAT(now(), '%Y') and month(ts)=DATE_FORMAT(now(), '%m');", userID, function (err, rows) {
        if (err) {
            return;
        }

        var dates = {};
        for (var i = 0; i < rows.length; i++) {
            dates[rows[i]["day(ts)"]] = rows[i].prizeType;
        }

        var isOpen = true;
        if (JSON.stringify(dates) !== "{}") {
            if (ttutil.arrayContains(Object.keys(dates), new Date().getDate()) !== -1) {   // new Date().getDate()
                isOpen = false;
            }
        }

        ttutil.psSendSocketData(socket, "OpenDailyAttendance", {isOpen: isOpen, dates: dates});
        return;
    });
};


/**
 * 购买道具
 * @param socket
 * @param data
 */
p.onBuyGoods = function (socket, data) {

    if (data.goodsID == null || data.count == null) {
        ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "参数错误"});
        return;
    }


    if (data.count <= 0) {
        ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "购买道具数量错误"});
        return;
    }

    if (!Config.canBuyHorn && data.goodsID == 1) {
        ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "对不起,今日已经被抢空"});
        return;
    }

    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {
        var goodsConfig = GD.goodsMap[data.goodsID];

        if (!goodsConfig) {
            ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "没有此物品！"});
            return;
        }

        var user = GD.realUserMap[userID];

        if (user.roomID != null) {
            ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "您正在游戏房间中，不能购买道具！"});
            return;
        }

        if (goodsConfig.price == 0) {
            ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "非卖品不能购买！"});
            return;
        }

        var allPrice = data.count * goodsConfig.price;
        var allCost = data.count * goodsConfig.ticket;

        switch (goodsConfig.type) {
            case GoodPayType.Coin:
                if (user.score < allPrice) {
                    ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "您的" + CURRENCY + "不够"});
                    return;
                }
                break;

            case GoodPayType.Acer:
                if (user.acer < allPrice) {
                    ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "您的钻石不够"});
                    return;
                }
                break;

            case GoodPayType.Ticket:
            case 3:
                var userGoodsSet = GD.goodsSystem.getUserGoodsSet(userID);
                var ticketCount = userGoodsSet.getGoodsCount(GoodsConst.GoodsCertificates);
                if (ticketCount < allCost) {
                    ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "您的物品兑换券不够"});
                    return;
                } else if (user.score < allPrice) {
                    ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "您的" + CURRENCY + "不够"});
                    return;
                }
                break;

            case GoodPayType.LuckyRMB:
                if (user.luckyRMB < allPrice) {
                    ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "您的红包不够"});
                    return;
                }
                break;

            default:
                ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "系统错误， 没有该付款方式"});
                return;
        }

        var allGetMoney = data.count * goodsConfig.giveMoney;

        //预扣
        SqlPool.x_query('call ps_goods_buy(?,?,?)', [userID, data.goodsID, data.count], (err, results) => {
            if (err) {
                ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "购买道具失败"});
                return;
            }

            if (results && results[0]) {
                if (results[0][0].msgID == 2) {
                    ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: results[0][0].errMsg});
                    return;
                }

                var userInfo = GD.realUserMap[userID];

                if (userInfo) {
                    switch (goodsConfig.type) {
                        case GoodPayType.Coin:
                            userInfo.score -= allPrice;
                            break;

                        case GoodPayType.Acer:
                            userInfo.acer -= allPrice;
                            userInfo.bankScore += allGetMoney;
                            break;

                        case GoodPayType.Ticket:
                        case 3:
                            userInfo.score -= allPrice;
                            userGoodsSet.updateGoods(GoodsConst.GoodsCertificates, -allCost);

                            ttutil.psSendSocketData(socket, "UpdateGoods", {
                                goodsID: GoodsConst.GoodsCertificates,
                                count: ticketCount - allCost
                            });
                            break;

                        case GoodPayType.LuckyRMB:
                            userInfo.luckyRMB -= allPrice;
                            break;
                    }

                    //更新分数
                    ttutil.psSendSocketData(socket, "UserInfoUpdate", {
                        acer: userInfo.acer,
                        score: userInfo.score,
                        bankScore: userInfo.bankScore,
                        luckyRMB: userInfo.luckyRMB
                    });

                    this.onBuyGoodsSuccess(userID, data.goodsID, data.count);

                    //更新道具表
                    var count = GD.goodsSystem.getUserGoodsCount(userID, data.goodsID);
                    //更新道具
                    ttutil.psSendSocketData(socket, "BuyGoods", {goodsID: data.goodsID, count: count});
                }

                //写邮件
                var content = "如题";

                var title;
                if (allGetMoney > 0) {
                    title = `[购买]-您购买${goodsConfig.name}数目${data.count}成功,获赠${allGetMoney}金币，请在背包及保险柜查收`;
                }
                else {
                    title = `[购买]-您购买${goodsConfig.name}数目${data.count}成功，请在背包查收`;
                }

                GD.plazaServer.writeUserMail(userID, title, content);

            } else {
                ttutil.psSendSocketData(socket, "BuyGoods", {errMsg: "购买道具失败"});
            }

        });
    }
};

/**
 * 写邮件
 * @param userID
 * @param title
 * @param content
 */
p.writeUserMail = function (userID, title, content) {

    content = content || "如题";

    SqlPool.x_query("CALL ps_user_write_user_mail(?,?,?,?,?,?,?, ?);", [userID, title, content, null, null, null, null, 1], (err, rows) => {
        if (err) {
            return;
        }

        if (rows && rows[0] && GD.realUserMap[userID]) {
            //返回未读邮件
            ttutil.psSendSocketData(GD.realUserMap[userID].socket, "GetMailListTip", {msgCount: rows[0][0].msgCount});
        }
    });
};

/**
 * 在线玩家封号检测
 */
p.onOnlineUserBanCheck = function () {

    SqlPool.x_query("CALL ps_online_user_ban_check();", (err, rows) => {
        if (err) {
            return;
        }

        if (rows && rows[0]) {
            for (var i = 0; i < rows[0].length; i++) {
                var userID = rows[0][i].userID;

                var user = GD.realUserMap[userID];
                if (user == null)
                    continue;

                //更新缓存
                GD.mainServer.updateCacheUserInfoMap(userID, {enable: false});

                //处理踢出游戏，数据库封号不写邮件
                user.kickOutBySystem("您的账号已经被管理员禁用，如有疑问，请联系客服");
            }
        }
    });
};

/**
 * 购买成功
 * @param goodsID 道具ID
 * @param count 数目
 */
p.onBuyGoodsSuccess = function (userID, goodsID, count) {

    //更新道具表
    GD.goodsSystem.updateUserGoods(userID, goodsID, count);
};

/**
 * 上架物品
 * @param socket
 * @param data
 */

p.onStoreAdd = function (socket, data) {
    var userID = socket.userID;
    if (data.count == null || data.goodsID == null || data.price == null || data.payType == null) {
        ttutil.psSendSocketData(socket, "StoreAdd", {errMsg: "参数错误"});
        return;
    }

    var userInfo = GD.realUserMap[userID];
    if (userInfo == null)
        return;

    var storeSet = GD.storeSystem.getUserStoreSet(userID);
    var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);
    var goods = goodsSet.findGoods(data.goodsID);

    if (goods == null) {
        ttutil.psSendSocketData(socket, "StoreAdd", {errMsg: "道具不存在"});
        return;
    }

    if (data.price <= 0) {
        ttutil.psSendSocketData(socket, "StoreAdd", {errMsg: "物品定价错误"});
        return;
    }

    if (data.count <= 0) {
        ttutil.psSendSocketData(socket, "StoreAdd", {errMsg: "物品数量错误"});
        return;
    }
    if (Number(data.count) > goods.count) {
        ttutil.psSendSocketData(socket, "StoreAdd", {errMsg: "背包物品数量不足"});
        return;
    }
    var payType = data.payType;

    SqlPool.x_query('call ps_store_add(?, ?, ?, ?, ?)', [userID, data.goodsID, data.price, Number(data.count), payType], (err, results) => {
        if (err) {
            ttutil.psSendSocketData(socket, "StoreAdd", {errMsg: "道具上架失败"});
            return;
        }
        if (results && results[0]) {
            if (results[0][0].msgID == 2) {
                ttutil.psSendSocketData(socket, "StoreAdd", {errMsg: results[0][0].errMsg});
                return;
            }
            ttutil.psSendSocketData(socket, "StoreAdd", {msg: "出售成功"});
            data.id = results[0][0]["@@identity"];
            data.userID = userID;
            data.nickname = userInfo.nickname;
            data.faceID = userInfo.faceID;
            storeSet.addGoods(data);
            goodsSet.updateGoods(data.goodsID, Number(-data.count));
            ttutil.psSendSocketData(socket, "UpdateGoods", {
                goodsID: data.goodsID,
                count: goodsSet.findGoods(data.goodsID).count
            });
        }
    });
};

/**
 * 随机上架物品
 */

p.onStoreAutoAdd = function () {

    var androidUserIDArray = [];

    for (var userID in GD.androidUserMap) {
        var memberOrder = GD.androidUserMap[userID].memberOrder;
        if (memberOrder >= 2)
            androidUserIDArray.push(userID);
    }

    if (androidUserIDArray.length < 1)
        return;

    var index = Math.floor(androidUserIDArray.length * Math.random());
    var userID = androidUserIDArray[index];

    var goodsID = 10;
    var payType = 0;//金币
    var price = 10300 + Math.floor(100 * Math.random());

    var count = 0;
    var value = Math.random();
    if (value < 0.8)
        count = 10 + Math.floor(90 * Math.random());
    else if (value < 0.9)
        count = 100 + Math.floor(900 * Math.random());
    else if (value < 0.95)
        count = 200 + Math.floor(300 * Math.random());
    else
        count = 500 + Math.floor(300 * Math.random());

    var userInfo = GD.androidUserMap[userID];
    if (userInfo == null)
        return;

    var storeSet = GD.storeSystem.getUserStoreSet(userID);
    var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);

    SqlPool.x_query('call ps_store_add(?, ?, ?, ?, ?)', [userID, goodsID, price, count, payType], (err, results) => {
        if (err) {
            mperror(err);
            return;
        }

        if (results && results[0]) {
            if (results[0][0].msgID == 2) {
                mperror(results[0][0].errMsg);
                return;
            }

            var data = {};
            data.id = results[0][0]["@@identity"];
            data.goodsID = goodsID;
            data.payType = payType;
            data.price = price;
            data.count = count;
            data.userID = userID;
            data.nickname = userInfo.nickname;
            data.faceID = userInfo.faceID;
            storeSet.addGoods(data);
            goodsSet.updateGoods(goodsID, -count);
        }
    });
};

/**
 * 下架物品
 * @param socket
 * @param data
 */
p.onStoreDec = function (socket, data) {
    var userID = socket.userID;
    if (data.id == null) {
        return;
    }
    if (GD.realUserMap[userID]) {
        var storeSet = GD.storeSystem.getUserStoreSet(userID);
        var goods = storeSet.findGoods(data.id);
        var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);

        SqlPool.x_query('call ps_store_dec(?)', [data.id], (err, results) => {
            if (err) {
                ttutil.psSendSocketData(socket, "StoreDec", {errMsg: "道具下架失败"});
                return;
            }
            if (results && results[0] && results[0][0].msgID == 1) {
                goodsSet.updateGoods(goods.goodsID, goods.count);
                //同步客户端
                ttutil.psSendSocketData(socket, "UpdateGoods", {
                    goodsID: goods.goodsID,
                    count: goodsSet.findGoods(goods.goodsID).count
                });
                storeSet.decGoods(data.id);

                var storeMap = GD.storeSystem.getGoodsStoreSet(goods.goodsID);
                ttutil.psSendSocketData(socket, "StoreGet", storeMap);
            }
        });
    }
};

p.onStoreGoods = function (socket, data) {
    var goods = GD.storeSystem.getGoods(socket.userID);

    ttutil.psSendSocketData(socket, "StoreGoods", goods);
};

/**
 * 获取卖家所有摊位
 * @param socket
 * @param data
 */
p.onUserStoreGet = function (socket, data) {
    var userID = socket.userID;
    if (GD.realUserMap[userID]) {

        var storeMap = GD.storeSystem.getUserStoreSet(data.sellerID).getStoreMap();
        ttutil.psSendSocketData(socket, "UserStoreGet", {data: storeMap});
    }
};

/**
 * 获取物品所有摊位信息
 * @param socket
 * @param data
 */
p.onStoreGet = function (socket, data) {

    if (data.goodsID == null) {
        ttutil.psSendSocketData(socket, "StoreGet", {errMsg: "参数错误"});
        return;
    }

    var storeMap = GD.storeSystem.getGoodsStoreSet(data.goodsID);
    ttutil.psSendSocketData(socket, "StoreGet", storeMap);
};

/**
 * 摊位购买物品
 * @param socket
 * @param data
 */
p.onStoreBuy = function (socket, data) {
    //sellerID 懒得查询
    if (data.count == null || data.sellerID == null || data.id == null) {
        ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "参数错误"});
        return;
    }

    if (data.count <= 0) {
        ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "购买道具数量错误"});
        return;
    }

    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {
        var storeSet = GD.storeSystem.getUserStoreSet(data.sellerID);
        var goods = storeSet.findGoods(data.id);

        if (!goods) {
            ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "玩家摊位没有该物品"});
            return;
        }

        var goodsConfig = GD.goodsMap[goods.goodsID];

        if (!goodsConfig) {
            ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "系统没有此物品"});
            return;
        }

        var user = GD.realUserMap[userID];

        if (user.roomID != null) {
            ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "您正在游戏房间中，不能购买道具"});
            return;
        }

        if (data.count > goods.count) {
            ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "购买的数量过多"});
            return;
        }

        var allPrice = data.count * goods.price;

        if (goods.payType == GoodPayType.Acer) {
            if (user.acer < allPrice) {
                ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "您的钻石不够"});
                return;
            }
        }
        else if (goods.payType == GoodPayType.Coin) {
            if (user.score < allPrice) {
                ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "您的" + CURRENCY + "不够"});
                return;
            }
        } else if (goods.payType == GoodPayType.LuckyRMB) {
            if (user.LuckyRMB < allPrice) {
                ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "您的红包不够"});
                return;
            }
        }
        else {
            ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "系统错误， 没有该付款方式"});
            return;
        }

        SqlPool.x_query('call ps_store_buy(?, ?, ?, @result)', [userID, data.id, data.count], (err, results) => {
            if (err) {
                ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "购买道具失败"});
                return;
            }
            if (results && results[0]) {
                if (results[0][0].msgID == 2) {
                    ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: results[0][0].errMsg});
                    return;
                }
                //买家在线　更新
                if (GD.realUserMap[userID]) {
                    if (goods.payType == GoodPayType.Acer) {
                        GD.realUserMap[userID].acer -= allPrice;
                    }
                    else if (goods.payType == GoodPayType.LuckyRMB) {
                        GD.realUserMap[userID].luckyRMB -= allPrice;
                    }
                    else {
                        GD.realUserMap[userID].score -= allPrice;
                    }

                    //更新分数
                    ttutil.psSendSocketData(socket, "UserInfoUpdate", {
                        acer: GD.realUserMap[userID].acer,
                        score: GD.realUserMap[userID].score,
                        bankScore: GD.realUserMap[userID].bankScore,
                        luckyRMB: GD.realUserMap[userID].luckyRMB,
                    });
                }

                //卖家在线更新
                if (GD.realUserMap[data.sellerID]) {
                    if (goods.payType == GoodPayType.Acer) {
                        GD.realUserMap[data.sellerID].acer += allPrice;
                    } else if (goods.payType == GoodPayType.LuckyRMB) {
                        GD.realUserMap[data.sellerID].luckyRMB += allPrice;
                    }
                    else {
                        GD.realUserMap[data.sellerID].bankScore += allPrice;
                    }

                    //更新分数
                    ttutil.psSendSocketData(GD.realUserMap[data.sellerID].socket, "UserInfoUpdate", {
                        acer: GD.realUserMap[data.sellerID].acer,
                        score: GD.realUserMap[data.sellerID].score,
                        bankScore: GD.realUserMap[data.sellerID].bankScore,
                        luckyRMB: GD.realUserMap[data.sellerID].luckyRMB,
                    });
                }

                //更新买家背包
                this.onBuyGoodsSuccess(userID, goods.goodsID, data.count);

                var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);
                var count = goodsSet.getGoodsCount(goods.goodsID);
                ttutil.psSendSocketData(socket, "UpdateGoods", {goodsID: goods.goodsID, count: count});

                //通知买家购买成功
                ttutil.psSendSocketData(socket, "StoreBuy", {name: goods.name, count: data.count});

                //刷新列表
                var storeMap = GD.storeSystem.getGoodsStoreSet(goods.goodsID);

                //处理store 该条目数目为0的时候删除
                goods.count -= data.count;
                if (goods.count == 0) {
                    delete storeSet.storeMap[goods.goodsID];
                    delete storeMap[data.id];
                }

                ttutil.psSendSocketData(socket, "StoreGet", storeMap);

                //写邮件
                var content = "如题";
                var title = `[购买]-您购买用户${results[0][0].s_nickname}的${goodsConfig.name}数目${data.count}成功，请在背包查收`;

                GD.plazaServer.writeUserMail(userID, title, content);

                //写邮件
                content = "如题";
                title = `[出售]-用户${user.nickname}购买了您出售的${goodsConfig.name}数目${data.count}成功`;

                GD.plazaServer.writeUserMail(data.sellerID, title, content);
            } else {
                ttutil.psSendSocketData(socket, "StoreBuy", {errMsg: "购买道具失败"});
            }
        })
    }
};

/**
 * 摊位批量购买物品
 * @param socket
 * @param data
 */
p.onStoreBatchBuy = function (socket, data) {

    if (data.goodsID == null || data.payType == null || data.price == null || data.price <= 0 || data.count == null || data.count <= 0) {
        ttutil.psSendSocketData(socket, "StoreBatchBuy", {errMsg: "参数错误"});
        return;
    }

    var goodsID = data.goodsID;

    var userID = socket.userID;
    if (userID != null && GD.realUserMap[userID]) {

        var user = GD.realUserMap[userID];

        if (user.roomID != null) {
            ttutil.psSendSocketData(socket, "StoreBatchBuy", {errMsg: "您正在游戏房间中，不能购买道具"});
            return;
        }

        var goodsConfig = GD.goodsMap[data.goodsID];

        if (!goodsConfig) {
            ttutil.psSendSocketData(socket, "StoreBatchBuy", {errMsg: "系统没有此物品"});
            return;
        }

        SqlPool.x_query('call ps_store_batch_buy(?, ?, ?, ?, ?)', [userID, data.goodsID, data.payType, data.price, data.count], (err, results) => {
            if (err) {
                ttutil.psSendSocketData(socket, "StoreBatchBuy", {errMsg: "购买道具失败"});
                return;
            }

            if (results == null || results.length <= 2) {
                ttutil.psSendSocketData(socket, "StoreBatchBuy", {errMsg: "购买道具失败"});
                return;
            }

            var errMsg = "";

            for (var i = 0; i < results.length - 2; i++) {
                var result = results[i][0];

                //购买失败
                if (result.msgID == 2) {
                    errMsg = result.errMsg;
                    break;
                }

                var sellerInfo = GD.realUserMap[result.sellerID];

                //卖家在线更新
                if (sellerInfo) {
                    if (data.payType == GoodPayType.Acer) {
                        sellerInfo.acer += result.totalPay;
                    } else if (data.payType == GoodPayType.LuckyRMB) {
                        sellerInfo.luckyRMB += result.totalPay;
                    }
                    else {
                        sellerInfo.bankScore += result.totalPay;
                    }

                    //更新分数
                    ttutil.psSendSocketData(sellerInfo.socket, "UserInfoUpdate", {
                        acer: sellerInfo.acer,
                        score: sellerInfo.score,
                        bankScore: sellerInfo.bankScore,
                        luckyRMB: sellerInfo.luckyRMB,
                    });
                }

                var sellerID = result.sellerID;

                var storeSet = GD.storeSystem.getUserStoreSet(sellerID);
                var storeMap = GD.storeSystem.getGoodsStoreSet(goodsID);
                var goods = storeSet.findGoods(result.storeID);

                //摊位物品数目为0的时候删除
                goods.count -= result.count;
                if (goods.count == 0) {
                    delete storeSet.storeMap[goodsID];
                    delete storeMap[result.storeID];
                }

                //写邮件
                content = "如题";
                title = `[出售]-用户${user.nickname}购买了您出售的${goodsConfig.name}数目${result.count}成功`;

                GD.plazaServer.writeUserMail(result.sellerID, title, content);
            }

            var result = results[results.length - 2][0];
            var totalPrice = result.totalPrice;
            var count = result.count;

            if (count == 0) {
                ttutil.psSendSocketData(socket, "StoreBatchBuy", {errMsg: errMsg});
                return;
            }

            var buyerInfo = GD.realUserMap[userID];

            var message = `您花费${totalPrice}${GoodPayTypeDesc[data.payType]}购买${goodsConfig.name}数目${count}成功，请在背包查收`;

            //买家在线　更新
            if (buyerInfo) {
                //更新买家背包
                this.onBuyGoodsSuccess(userID, goodsID, count);
                var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);
                ttutil.psSendSocketData(socket, "UpdateGoods", {
                    goodsID: goodsID,
                    count: goodsSet.getGoodsCount(goodsID)
                });

                var storeMap = GD.storeSystem.getGoodsStoreSet(goodsID);
                ttutil.psSendSocketData(socket, "StoreGet", storeMap);

                if (data.payType == GoodPayType.Acer) {
                    buyerInfo.acer -= totalPrice;
                }
                else if (data.payType == GoodPayType.LuckyRMB) {
                    buyerInfo.luckyRMB -= totalPrice;
                }
                else {
                    buyerInfo.score -= totalPrice;
                }

                //更新分数
                ttutil.psSendSocketData(socket, "UserInfoUpdate", {
                    acer: buyerInfo.acer,
                    score: buyerInfo.score,
                    bankScore: buyerInfo.bankScore,
                    luckyRMB: buyerInfo.luckyRMB,
                });

                ttutil.psSendSocketData(socket, "StoreBatchBuy", {successMsg: message});
            }

            //写邮件
            var title = `[购买]-${message}`;

            GD.plazaServer.writeUserMail(userID, title);
        });
    }
};

//监听连接断开事件
p.onDisconnect = function (socket, data) {
    //验证码清除
    this.onCodeDel(socket);
    delete this.socketID2socketMap[socket.id];
    delete GD.deviceID2SocketMap[socket.deviceID];
    delete GD.userID2PrizeCodeTimeMap[socket.userID];

    //非服务端主动断开的.
    if (!socket.activeDisConnect) {
        var userID = socket.userID;
        if (userID != null && GD.realUserMap[userID]) {
            GD.realUserMap[userID].onUserSocketDisconnect();
        }
    }

};


/**
 * 玩家领取充值回扣
 * @param socket
 * @param data
 */
p.onTakeActivityRechargeRebate = function (socket, data) {

    var userID = socket.userID;
    if (GD.realUserMap[userID]) {

        if (GD.realUserMap[userID].paySum <= 0) {

            ttutil.psSendSocketData(socket, "TakeActivityRechargeRebate", {errMsg: "您未充值， 没有奖励可领取"});
            return;
        }


        SqlPool.x_query("call ps_activity_take_recharge_rebate(?);", [userID], (err, results) => {

            if (err) {
                ttutil.psSendSocketData(socket, "TakeActivityRechargeRebate", {errMsg: "系统错误"});
                return;
            }
            if (results && results[0]) {

                if (results[0][0].errMsg) {
                    ttutil.psSendSocketData(socket, "TakeActivityRechargeRebate", {errMsg: results[0][0].errMsg});
                    return;
                }
                else {

                    var money = results[0][0].money;

                    if (GD.realUserMap[userID]) {
                        GD.realUserMap[userID].bankScore += money;
                        ttutil.psSendSocketData(socket, "TakeActivityRechargeRebate", {money: money});
                        ttutil.psSendSocketData(GD.realUserMap[userID].socket, "UserInfoUpdate", {bankScore: GD.realUserMap[userID].bankScore});
                    }

                    //写邮件
                    var title = "[活动] -您已经领取充值天天送金额" + money + " 注意查收 ";
                    var content = "如题";
                    GD.plazaServer.writeUserMail(userID, title, content);

                    return;
                }
            }


        });
    }
};

// p.onUseMobQuery = function (socket, data) {
//     var userID = socket.userID;
//     if (GD.realUserMap[userID] == null) {
//         ttutil.psSendSocketData(socket, "UseMobQuery", {errMsg: "用户不存在，使用道具失败"});
//         return;
//     }
//
//     var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);
//
//     goodsSet.useGoods(data.goodsID, (err) => {
//         if (err == null) {
//             var user = GD.realUserMap[userID];
//             var count = goodsSet.getGoodsCount(data.goodsID);
//             ttutil.psSendSocketData(user.socket, "UpdateGoods", {goodsID: data.goodsID, count: count});
//             return;
//         }
//     });
// };


///////////////////////////////////////////////////////////////////////////
p.onUseGoods = function (socket, data) {
    var userID = socket.userID;
    var userInfo = GD.realUserMap[userID];
    if (userInfo == null) {
        ttutil.psSendSocketData(socket, "UseGoods", {errMsg: "用户不存在，使用道具失败"});
        return;
    }

    var goodsID = Number(data.goodsID);
    var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);

    if (!goodsSet || goodsSet.getGoodsCount(goodsID) <= 0) {
        ttutil.psSendSocketData(socket, "UseGoods", {errMsg: "您没有该道具， 使用失败"});
        return;
    }

    var goodInfo = GD.goodsMap[goodsID];
    if (goodInfo.type == 3)//实物类
    {
        SqlPool.x_query("insert into ps_goods_order(userID, goodsID, phone, address, choice, notes) value(?,?,?,?,?,?)", [userID, goodsID, data.phone, data.address, data.choice, data.notes], (err, rows) => {
            if (err) {
                ttutil.psSendSocketData(socket, "UseGoods", {errMsg: err});
                return;
            }

            goodsSet.useGoods(data.goodsID, (err) => {
                if (err == null) {
                    var user = GD.realUserMap[userID];
                    var count = goodsSet.getGoodsCount(data.goodsID);
                    ttutil.psSendSocketData(user.socket, "UpdateGoods", {goodsID: data.goodsID, count: count});
                    ttutil.psSendSocketData(user.socket, "UseGoods", {successMsg: "道具使用成功"});
                }
                else {
                    ttutil.psSendSocketData(socket, "UseGoods", {errMsg: err});
                }
            });
        });

        return;
    }

    switch (goodsID) {
        //这些直接扣
        case GoodsConst.Weather:
        case GoodsConst.Mobile:
        case GoodsConst.Postcode:
        case GoodsConst.Calendar:
        case GoodsConst.CoinPackage1:
        case GoodsConst.CoinPackage10:
        case GoodsConst.CoinPackage100:
        case GoodsConst.CoinPackage1000:
            goodsSet.useGoods(data.goodsID, (err) => {
                if (err == null) {
                    var count = goodsSet.getGoodsCount(data.goodsID);
                    ttutil.psSendSocketData(socket, "UpdateGoods", {goodsID: data.goodsID, count: count});

                    if (goodsID == GoodsConst.CoinPackage1 || goodsID == GoodsConst.CoinPackage10 || goodsID == GoodsConst.CoinPackage100 || goodsID == GoodsConst.CoinPackage1000) {
                        switch (goodsID) {
                            case GoodsConst.CoinPackage1:
                                userInfo.bankScore += 10000;
                                break;

                            case GoodsConst.CoinPackage10:
                                userInfo.bankScore += 100000;
                                break;

                            case GoodsConst.CoinPackage100:
                                userInfo.bankScore += 1000000;
                                break;

                            case GoodsConst.CoinPackage1000:
                                userInfo.bankScore += 10000000;
                                break;
                        }


                        ttutil.psSendSocketData(socket, "UseGoods", {successMsg: "道具使用成功，" + CURRENCY + "已放在您的保险柜，请注意查收。"});
                        ttutil.psSendSocketData(socket, "UserInfoUpdate", {
                            acer: userInfo.acer,
                            score: userInfo.score,
                            bankScore: userInfo.bankScore,
                            luckyRMB: userInfo.luckyRMB
                        });
                    }
                    else {
                        ttutil.psSendSocketData(socket, "UseGoods", {successMsg: "道具使用成功"});
                    }
                }
                else {
                    ttutil.psSendSocketData(socket, "UseGoods", {errMsg: err});
                }
            });
            break;

        case GoodsConst.MobileFare1:
        case GoodsConst.MobileFare5:
        case GoodsConst.MobileFare10:
        case GoodsConst.MobileFare50:
        case GoodsConst.MobileFare100:
            var phoneNo = data.phoneNo;

            if (!phoneNo || phoneNo.length != 11) {
                ttutil.psSendSocketData(socket, "UseGoods", {errMsg: "手机号码错误"});
                return;
            }


            var cardNum = 0;

            if (goodsID === GoodsConst.MobileFare1) {
                cardNum = 1;
            }
            else if (goodsID === GoodsConst.MobileFare5) {
                cardNum = 5;
            }
            else if (goodsID === GoodsConst.MobileFare10) {
                cardNum = 10;
            }
            else if (goodsID === GoodsConst.MobileFare50) {
                cardNum = 50;
            }
            else if (goodsID === GoodsConst.MobileFare100) {
                cardNum = 100;
            }


            JuheHelper.checkCanRechargeMobile(phoneNo, cardNum).then(obj => {
                var data = JSON.parse(obj.data);

                if (data.error_code == 0 && data.reason == "允许充值的手机号码及金额") {

                    var orderID = ttutil.generateOrderID();

                    SqlPool.x_query("insert into ps_mobile_fare(phone, num, myOrderID,userID, status, goodsID) value(?,?,?,?,?,?)", [phoneNo, cardNum, orderID, userID, -1, goodsID], (err, rows) => {
                        if (err) {
                            ttutil.psSendSocketData(socket, "UseGoods", {errMsg: "充值失败"});
                            return;
                        }

                        this.doRechargeMobileFare(userID, goodsID, orderID, phoneNo, cardNum);
                    });


                }
                else {
                    ttutil.psSendSocketData(socket, "UseGoods", {errMsg: data.reason});
                }

            }).catch(e => {
                console.log(e.toString());
                ttutil.psSendSocketData(socket, "UseGoods", {errMsg: "充值失败"});
            });
            break;

        default:
            ttutil.psSendSocketData(socket, "UseGoods", {errMsg: "使用道具失败，没有使用方式！"});
            break;
    }
};

p.doRechargeMobileFare = function (userID, goodsID, orderID, phoneNo, cardNum) {
    var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);
    var user = GD.realUserMap[userID];

    var socket = user && user.socket;

    goodsSet.useGoods(goodsID, (errMsg) => {
        if (errMsg == null) {

            JuheHelper.rechargeMobileFare(orderID, phoneNo, cardNum).then(obj => {
                var data2 = JSON.parse(obj.data);

                if (data2.error_code == 0) {

                    var result = data2.result;

                    //存储数据库
                    SqlPool.x_query("update ps_mobile_fare set orderCash = ?,outOrderID = ?, status = ?, cardID = ?, cardName = ? where myOrderID = ?", [result.ordercash, result.sporder_id, result.game_state, result.cardid, result.cardname, result.uorderid], (err, rows) => {
                        if (err) {
                            console.log(err.toString());
                            return;
                        }
                    });

                    ttutil.psSendSocketData(socket, "UseGoods", {
                        successMsg: "正在充值中，请留意",
                        cardName: result.cardname,
                        phoneNo: result.game_userid,
                        cardNum: result.cardnum,
                    });

                    var count = goodsSet.getGoodsCount(goodsID);
                    ttutil.psSendSocketData(socket, "UpdateGoods", {goodsID: goodsID, count: count});

                }
                else {
                    //[***如果充值过程中，遇到http网络状态异常或错误码返回系统异常10014，请务必通过订单查询接口检测订单或联系客服，不要直接做失败处理，避免造成不必要的损失！！！]

                    mperror("充值失败", data2.reason);
                    //系统错误, 状态为-1， 另一边会定时查询处理
                    ttutil.psSendSocketData(socket, "UseGoods", {errMsg: "充值失败, 道具稍后会退还"});

                }


            }).catch(e => {
                winston.log(e.toString());
                //系统错误, 状态为-1， 另一边会定时查询处理
                ttutil.psSendSocketData(socket, "UseGoods", {errMsg: "充值失败"});
            });

            return;
        }
        else {
            ttutil.psSendSocketData(socket, "UseGoods", {errMsg: errMsg});
        }
    });
};

/**
 * 玩家红包兑换
 * @param socket
 * @param data
 */
p.onPayCash = function (socket, data) {
    var userID = socket.userID;
    var userInfo = GD.realUserMap[userID];
    if (userInfo == null) {
        ttutil.psSendSocketData(socket, "PayCash", {errMsg: "用户不存在，转出失败"});
        return;
    }

    data.type = Number(data.type);
    if (data.type != 3 && data.type != 4) {
        ttutil.psSendSocketData(socket, "PayCash", {errMsg: "转出类型错误"});
        return;
    }

    if (verifyMng.isVerifyBankPassword(userID) == false) {
        ttutil.psSendSocketData(socket, "PayCash", {errMsg: "您未验证过保险柜密码，无权操作"});
        return;
    }

    data.payRMB = Number(data.payRMB);

    if (data.payRMB < 10) {
        ttutil.psSendSocketData(socket, "PayCash", {errMsg: "至少要10元才能转出!"});
        return;
    }

    if (isNaN(data.payRMB) || data.payRMB % 10 != 0) {
        ttutil.psSendSocketData(socket, "PayCash", {errMsg: "转出额度错误, 只能是10的倍数"});
        return;
    }

    if (userInfo.luckyRMB < data.payRMB) {
        ttutil.psSendSocketData(socket, "PayCash", {errMsg: "您的红包余额不足。"});
        return;
    }

    if (userInfo.realname == null) {
        if (userInfo.phone)
            ttutil.psSendSocketData(socket, "PayCash", {errMsg: "您还未实名认证，请先解绑手机再重新绑定手机实名验证。"});
        else if (userInfo.account)
            ttutil.psSendSocketData(socket, "PayCash", {errMsg: "您还未实名认证，请点击大厅左上角头像进入【账户安全】绑定手机实名验证。"});
        else
            ttutil.psSendSocketData(socket, "PayCash", {errMsg: "您还未实名认证，请点击大厅左上角头像进入【个人中心】绑定正式账号再绑定手机实名验证。"});
        return;
    }

    if (data.outRealName != userInfo.realname) {
        //写邮件
        var title = "[系统] - 您因实名信息作弊被封号";
        GD.plazaServer.writeUserMail(userID, title);

        //更新缓存
        GD.mainServer.updateCacheUserInfoMap(userID, {enable: false});

        //处理踢出游戏
        user.kickOutBySystem("您因实名信息作弊被封号");
        return;
    }

    if (data.outAccount.includes("****")) {
        data.outAccount = userInfo.phone;
    }

    switch (data.type) {
        case 3: //支付宝现金

            //支付宝账号，
            if (!data.outAccount) {
                ttutil.psSendSocketData(socket, "PayCash", {errMsg: "支付宝账号不能为空。"});
                return;
            }
            break;

        case 4://微信现金
            if (!GD.realUserMap[userID].openID) {
                if (GD.realUserMap[userID].wxUnionID) {
                    ttutil.psSendSocketData(socket, "PayCash", {errMsg: "请先用手机客户端微信登录"});
                    return;
                }
                else {
                    ttutil.psSendSocketData(socket, "PayCash", {errMsg: "请先用手机客户端绑定微信"});
                    return;
                }
            }
            break;
    }

    var ip = GD.realUserMap[userID].loginIP;
    var openID = GD.realUserMap[userID].openID;
    var orderID = ttutil.generateOrderID();
    var outAccount;
    if (data.type == 3) {
        outAccount = data.outAccount;
    }
    else if (data.type == 4) {
        outAccount = openID;
    }

    SqlPool.x_query("call ps_check_anomaly_userID(?, ?, ?)", [userID, outAccount, data.outRealName], (err, rows) => {

        if (err || !rows) {
            ttutil.psSendSocketData(socket, "PayCash", {errMsg: "系统错误"});
            return;
        }

        var anomaly = false;

        for (var i = 0; i < rows.length - 1; i++) {
            var result = rows[i][0];
            if (result.errMsg == null)//账号异常，封号
                continue;

            //写邮件
            var content = "如题";
            var title = "[系统]-管理员对您的账号进行了 冻结账号 的操作";

            GD.plazaServer.writeUserMail(result.userID, title, content);

            var user = GD.realUserMap[result.userID];
            if (user == null)
                continue;

            //更新缓存
            GD.mainServer.updateCacheUserInfoMap(result.userID, {enable: false});

            //处理踢出游戏
            user.kickOutBySystem("您的账号已经被管理员禁用，如有疑问，请联系客服");

            if (userID == result.userID)
                anomaly = true;
        }

        if (anomaly) {
            ttutil.psSendSocketData(socket, "PayCash", {errMsg: ""});
            return;
        }

        SqlPool.x_query("call ps_insert_pay_cash_order(?,?,?,?,?,?)", [userID, data.payRMB, orderID, data.type, outAccount, data.outRealName], (err, rows) => {

            if (err || !rows || !rows[0] || !rows[0][0]) {
                ttutil.psSendSocketData(socket, "PayCash", {errMsg: "系统错误"});
                return;
            }

            var info = rows[0][0];

            if (info.res != 0) {
                if (info.res == 1) {
                    ttutil.psSendSocketData(socket, "PayCash", {errMsg: "系统错误2"});
                    mperror("ps_insert_pay_cash_order 事务出错");
                }
                else {
                    ttutil.psSendSocketData(socket, "PayCash", {errMsg: info.msg});

                    if (info.res == 3) {

                        var user = GD.realUserMap[userID];
                        if (user == null)
                            return;

                        //更新缓存
                        GD.mainServer.updateCacheUserInfoMap(userID, {enable: false});

                        //处理踢出游戏，数据库封号不写邮件
                        user.kickOutBySystem("您的账号已经被管理员禁用，如有疑问，请联系客服");
                    }
                }
            }
            else {

                //在内存里扣除掉
                if (GD.realUserMap[userID]) {

                    GD.realUserMap[userID].setDBInfo({luckyRMB: GD.realUserMap[userID].luckyRMB - data.payRMB});
                    ttutil.psSendSocketData(GD.realUserMap[userID].socket, "UserInfoUpdate", {luckyRMB: GD.realUserMap[userID].luckyRMB});
                }

                //支付宝转账
                if (data.type == 3) {
                    //发起付款
                    AlipayHelper.transfer(orderID, data.outAccount, data.payRMB, data.outRealName).then(obj => {

                        var result = 0;
                        if (obj.code == "10000" && obj.msg == "Success") {

                            ttutil.psSendSocketData(socket, "PayCash", {successMsg: `提现${data.payRMB}元成功, 请往支付宝${data.outAccount}查收`});
                            result = 1;
                        }
                        else {
                            //失败
                            ttutil.psSendSocketData(socket, "PayCash", {errMsg: obj.sub_msg || obj.msg});
                            //要回滚了
                            // ...
                            result = -Math.abs(Number(obj.code));
                            if (result == 0 || isNaN(result)) {
                                result = -99999;
                            }
                        }

                        //失败回退， 成功标志成功
                        SqlPool.x_query("call ps_handle_pay_cash_order(?,?,?,?)", [obj.out_biz_no, obj.order_id, result, obj.sub_msg || obj.msg], (error, rows) => {
                            if (err || !rows || !rows[0] || !rows[0][0]) {
                                ttutil.psSendSocketData(socket, "PayCash", {errMsg: "系统错误"});
                                return;
                            }

                            if (rows[0][0].msg == null) {
                                //即提现失败的， 已经退回给他现金了
                                if (result != 1) {
                                    if (GD.realUserMap[userID]) {
                                        GD.realUserMap[userID].setDBInfo({luckyRMB: GD.realUserMap[userID].luckyRMB + data.payRMB});
                                        ttutil.psSendSocketData(GD.realUserMap[userID].socket, "UserInfoUpdate", {luckyRMB: GD.realUserMap[userID].luckyRMB});
                                    }
                                }
                                else {
                                    //成功的
                                    var title = `[提现]-您已经通过支付宝提现${data.payRMB}元成功！`;
                                    GD.plazaServer.writeUserMail(userID, title);
                                }
                            }
                            else if (rows[0][0].msg == "您因实名信息作弊被封号") {
                                //写邮件
                                var title = "[系统] - " + rows[0][0].msg;
                                GD.plazaServer.writeUserMail(userID, title);

                                //更新缓存
                                GD.mainServer.updateCacheUserInfoMap(userID, {enable: false});

                                //处理踢出游戏
                                GD.realUserMap[userID].kickOutBySystem(rows[0][0].msg);
                            }
                        });

                    }).catch(e => {
                        mperror("AlipayHelper.transfer", e);
                        ttutil.psSendSocketData(socket, "PayCash", {errMsg: e.errMsg});
                    });
                }
                ////微信现金
                else if (data.type == 4) {
                    WXPayHelper.transfers(orderID, openID, data.payRMB, data.outRealName, ip).then(obj => {

                        var result = 0;
                        if (obj.return_code == "SUCCESS" && obj.result_code == "SUCCESS") {
                            ttutil.psSendSocketData(socket, "PayCash", {successMsg: `提现${data.payRMB}元成功, 请往微信查收`});
                            result = 1;
                        }
                        else {
                            //失败
                            ttutil.psSendSocketData(socket, "PayCash", {errMsg: obj.err_code_des || obj.return_msg});
                            //要回滚了
                            // ...
                            result = -1;
                        }

                        //失败回退， 成功标志成功
                        SqlPool.x_query("call ps_handle_pay_cash_order(?,?,?,?)", [orderID, obj.payment_no, result, obj.err_code_des || obj.return_msg], (error, rows) => {
                            if (err || !rows || !rows[0] || !rows[0][0]) {
                                ttutil.psSendSocketData(socket, "PayCash", {errMsg: "系统错误"});
                                return;
                            }

                            if (rows[0][0].msg == null) {
                                //即提现失败的， 已经退回给他现金了
                                if (result != 1) {
                                    if (GD.realUserMap[userID]) {
                                        GD.realUserMap[userID].setDBInfo({luckyRMB: GD.realUserMap[userID].luckyRMB + data.payRMB});
                                        ttutil.psSendSocketData(GD.realUserMap[userID].socket, "UserInfoUpdate", {luckyRMB: GD.realUserMap[userID].luckyRMB});
                                    }
                                }
                                else {
                                    //成功的
                                    var title = `[提现]-您已经通过微信提现${data.payRMB}元成功！`;
                                    GD.plazaServer.writeUserMail(userID, title);
                                }
                            }
                            else if (rows[0][0].msg == "您因实名信息作弊被封号") {
                                //写邮件
                                var title = "[系统] - " + rows[0][0].msg;
                                GD.plazaServer.writeUserMail(userID, title);

                                //更新缓存
                                GD.mainServer.updateCacheUserInfoMap(userID, {enable: false});

                                //处理踢出游戏
                                GD.realUserMap[userID].kickOutBySystem(rows[0][0].msg);
                            }
                        }).catch(e => {
                            mperror("WXPayHelper.transfer", e);
                            ttutil.psSendSocketData(socket, "PayCash", {errMsg: e.errMsg});
                        });
                    });
                }
            }
        });
    });
};

/**
 * 点击领取微信分享集赞奖励
 * @param socket
 * @param data
 */
p.onTakeActivityWXFXJZ = function (socket, data) {
    var userID = socket.userID;
    if (GD.realUserMap[userID] == null) {
        ttutil.psSendSocketData(socket, "TakeActivityWXFXJZ", {errMsg: "用户不存在，领取失败"});
        return;
    }

    var userInfo = GD.realUserMap[userID];

    if (!userInfo.wxUnionID) {
        ttutil.psSendSocketData(socket, "TakeActivityWXFXJZ", {errMsg: "请先绑定微信"});
        return;
    }
    if (!userInfo.phone) {
        ttutil.psSendSocketData(socket, "TakeActivityWXFXJZ", {errMsg: "请先绑定手机"});
        return;
    }


    SqlPool.x_query('call ps_take_activityWXFXJZ(?)', [userInfo.wxUnionID], (err, results) => {
        if (err || !results || !results[0] || !results[0][0]) {
            ttutil.psSendSocketData(socket, "TakeActivityWXFXJZ", {errMsg: "领取奖励失败，请稍后重试！"});
            return;
        }

        var result = results[0][0];
        if (result.errMsg) {
            ttutil.psSendSocketData(socket, "TakeActivityWXFXJZ", {errID: result.errID, errMsg: result.errMsg});
            return;
        }

        userInfo.bankScore += result.money;

        ttutil.psSendSocketData(socket, "UserInfoUpdate", {
            bankScore: userInfo.bankScore,
        });

        //写邮件
        var content = "如题";
        var title = `[活动]-您领取微信分享集赞${result.money}金币成功， 请往保险柜查收`;
        GD.plazaServer.writeUserMail(userID, title, content);

        ttutil.psSendSocketData(socket, "TakeActivityWXFXJZ", {successMsg: `您领取微信分享集赞${result.money}金币成功， 请往保险柜查收`});
    });

};

p.onDailyAttendance = function (socket, data) {
    var userID = socket.userID;
    if (GD.realUserMap[userID] == null) {
        ttutil.psSendSocketData(socket, "DailyAttendance", {errMsg: "用户不存在，签到失败"});
        return;
    }

    if (data.type === DailyAttendanceType.Retroactive) {
        var goodsSet = GD.goodsSystem.getUserGoodsSet(userID);
        if (!goodsSet || goodsSet.getGoodsCount(GoodsConst.RetroactiveCard) <= 0) {
            ttutil.psSendSocketData(GD.realUserMap[userID].socket, "DailyAttendance", {errMsg: "您没有补签卡，不能补签。"});
            return;
        }
    }

    SqlPool.x_query('CALL ps_user_daily_attendance_add(?,?,?);', [userID, data.type, data.date], (err, rows) => {
        if (err) {
            ttutil.psSendSocketData(GD.realUserMap[userID].socket, "DailyAttendance", {errMsg: "系统错误！"});
            return;
        }

        if (rows[0][0].msgID === 1) {
            ttutil.psSendSocketData(GD.realUserMap[userID].socket, "DailyAttendance", {succeed: true});

            if (data.type === 1) {

                SqlPool.x_query('select count from ps_user_goods where userID=? and goodsID=?;', [userID, GoodsConst.RetroactiveCard], (err, rows) => {
                    if (err) {
                        ttutil.psSendSocketData(GD.realUserMap[userID].socket, "DailyAttendance", {errMsg: "系统错误！"});
                        return;
                    }

                    ttutil.psSendSocketData(GD.realUserMap[userID].socket, "UpdateGoods", {
                        goodsID: GoodsConst.RetroactiveCard,
                        count: rows[0].count
                    });
                    GD.goodsSystem.updateUserGoods(userID, GoodsConst.RetroactiveCard, -1);

                });
            }
        } else {
            ttutil.psSendSocketData(GD.realUserMap[userID].socket, "DailyAttendance", {errMsg: rows[0][0].errMsg});
        }
    });
};

p.onJqueryrotate = function (socket, data) {
    var userID = socket.userID;
    var userInfo = GD.realUserMap[userID];
    if (userInfo === null) {
        ttutil.psSendSocketData(socket, "Jqueryrotate", {errMsg: "用户不存在，抽奖失败！"});
        return;
    }

    // 奖励品概率表 【兑换券X2，金币X188，兑换券X5，金币X1888，金币X8888，房卡X10，改名卡X2，推广红包X3】
    var prizeTypeCountArray = [2, 188, 5, 1888, 8888, 10, 2, 3];  // 奖励品数量表
    var prizeTypeProbArray = [20, 20, 15, 15, 10, 10, 8, 2];        // 奖励品概率表
    var userOriginWeight = 100;

    SqlPool.x_query('select sum(payRMB) from ps_pay where status = 300 and userID=? and ts > DATE_SUB(now(), INTERVAL 3 day);', userID, function (err, rows) {
        if (err) {
            ttutil.psSendSocketData(socket, "Jqueryrotate", {errMsg: "系统错误！"});
            return;
        }

        // 最近三天充值的RMB
        var paySum = rows[0]['sum(payRMB)'] || 0;
        if (paySum < 10) {
            userOriginWeight = userOriginWeight - 45;
        }
        else if (paySum < 100) {
            userOriginWeight = userOriginWeight - 10;
        }
        else if (paySum > 1000) {
            userOriginWeight *= 10;
        }

        var userRealWeight = Math.floor(Math.random() * userOriginWeight);

        for (let i = 0; i < prizeTypeProbArray.length; i++) {
            userRealWeight = userRealWeight - prizeTypeProbArray[i];
            if (userRealWeight <= 0) {

                SqlPool.x_query('CALL ps_user_daily_attendance_lotto(?,?,?,?);', [userID, i, prizeTypeCountArray[i], data.date], (err, rows) => {
                    if (err) {
                        ttutil.psSendSocketData(socket, "Jqueryrotate", {errMsg: "系统错误！"});
                        return;
                    }

                    var info = rows[0][0];

                    if (info.errMsg) {
                        ttutil.psSendSocketData(socket, "Jqueryrotate", {errMsg: info.errMsg});
                        return;
                    }

                    // 兑奖券奖励
                    if (i === 0 || i === 2) {
                        ttutil.psSendSocketData(socket, "UpdateGoods", {
                            goodsID: GoodsConst.GoodsCertificates,
                            count: info.prizeCount
                        });
                        GD.goodsSystem.updateUserGoods(userID, GoodsConst.GoodsCertificates, prizeTypeCountArray[i]);
                        // 金币奖励
                    } else if (i === 1 || i === 3 || i === 4) {
                        userInfo.setDBInfo({bankScore: info.prizeCount});
                        ttutil.psSendSocketData(socket, "UserInfoUpdate", {bankScore: userInfo.bankScore});
                        // 房卡奖励
                    } else if (i === 5) {
                        ttutil.psSendSocketData(socket, "UpdateGoods", {
                            goodsID: GoodsConst.RoomCard,
                            count: info.prizeCount
                        });
                        GD.goodsSystem.updateUserGoods(userID, GoodsConst.RoomCard, prizeTypeCountArray[i]);
                        // 改名卡奖励
                    } else if (i === 6) {
                        ttutil.psSendSocketData(socket, "UpdateGoods", {
                            goodsID: GoodsConst.RenameCard,
                            count: info.prizeCount
                        });
                        GD.goodsSystem.updateUserGoods(userID, GoodsConst.RenameCard, prizeTypeCountArray[i]);
                        // 推广红包奖励
                    } else if (i === 7) {
                        ttutil.psSendSocketData(socket, "UpdateGoods", {
                            goodsID: GoodsConst.RedPacket,
                            count: info.prizeCount
                        });
                        GD.goodsSystem.updateUserGoods(userID, GoodsConst.RedPacket, prizeTypeCountArray[i]);
                    }

                    ttutil.psSendSocketData(socket, "Jqueryrotate", {
                        prizeType: i,
                        prizeCount: prizeTypeCountArray[i]
                    });

                    var title = `[每日抽奖]-您已完成${data.date}签到，并在“幸运转盘”中抽得“${info.prizeDesp}”奖项。`;
                    GD.plazaServer.writeUserMail(userID, title, "如题");
                });

                return;
            }
        }
    });
};

/**
 * 获取红包雨记录
 * @param socket
 * @param data
 */
p.onTakeActivityRedPackRainLog = function (socket, data) {
    if (GD.realUserMap[socket.userID]) {
        ttutil.psSendSocketData(socket, "TakeActivityRedPackRainLog", {list: GD.redPackRainLogArray});
    }
};


p.onHistoricRecord = function (socket, data) {

    if (data == null) {
        ttutil.psSendSocketData(socket, "HistoricRecord", {errMsg: "查询数据错误！"});
        return;
    }

    var user = GD.realUserMap[socket.userID];

    if (!user) {
        mpinfo("找不到用户信息，无法查询游戏记录！", "userID:", socket.userID, "roomID:", data.roomID);
        return;
    }

    var userID = socket.userID;
    if (data.queryType === 1) {
        userID = null;
    }

    SqlPool.x_query("call ps_game_query_record(?,?,?,?,?,?)", [data.moduleID, data.roomID, data.tableID, userID, data.startIndex, data.queryCount], (err, rows) => {
        if (err) {
            ttutil.psSendSocketData(socket, "HistoricRecord", {errMsg: "游戏记录查询失败！"});
            return;
        }

        ttutil.psSendSocketData(socket, "HistoricRecord", {data: rows[0]});
    });
};

/**
 * 兑换码
 * @param socket
 * @param data
 */
p.onTakePrizeCodeReward = function (socket, data) {

    var userID = socket.userID;
    if (!GD.realUserMap[userID]) {
        ttutil.psSendSocketData(socket, "TakePrizeCodeReward", {errMsg: "玩家未登录！"});
        return;
    }

    if (!data || !data.prizeCode || data.prizeCode.length != 20) {
        ttutil.psSendSocketData(socket, "TakePrizeCodeReward", {errMsg: "兑奖码错误！"});
        return;
    }

    if (GD.userID2PrizeCodeTimeMap[userID] && GD.userID2PrizeCodeTimeMap[userID] + 1000 > Date.now()) {
        ttutil.psSendSocketData(socket, "TakePrizeCodeReward", {errMsg: "请慢点！"});
        return;
    }
    GD.userID2PrizeCodeTimeMap[socket.userID] = Date.now();


    SqlPool.x_query("call ps_take_prize_code_reward(?,?)", [userID, data.prizeCode], (err, rows) => {
        if (err || !rows || !rows[0] || !rows[0][0]) {
            ttutil.psSendSocketData(socket, "TakePrizeCodeReward", {errMsg: "系统错误"});
            return;
        }

        if (rows[0][0].errMsg) {
            ttutil.psSendSocketData(socket, "TakePrizeCodeReward", {errMsg: rows[0][0].errMsg});
            return;
        }
        var money = rows[0][0].money;

        var user = GD.realUserMap[socket.userID];
        if (user) {
            user.setDBInfo({bankScore: user.bankScore + money});
            ttutil.psSendSocketData(socket, "TakePrizeCodeReward", {successMsg: `您领取了${money}金币,请前往保险柜查收!`});
            ttutil.psSendSocketData(user.socket, "UserInfoUpdate", {bankScore: user.bankScore});
        }

        GD.plazaServer.writeUserMail(userID, `[活动]-您通过邀请码领取了${money}金币, 请前往保险柜查收`);

    });
};

//对外开放接口
module.exports = PlazaServer;